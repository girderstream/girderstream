# Girderstream

Girderstream is a meta-build project that builds and integrates software from a
set of declarative build instructions a bit like buildroot, yocto or buildstream.

A introduction can be found in our docs as [source](./docs) or as [html](https://girderstream.gitlab.io/girderstream/book/main/about.html)

If you use girderstream as a lib or as a dev the doc strings are also rendered as
[html](https://girderstream.gitlab.io/girderstream/doc/girderstream/index.html)

## Users of Girderstream

Some projects providing tool chains for you to use to build your girderstream projects:

* [Girderstream SDK](https://gitlab.com/pointswaves/girderstream-sdk/)
  This imports Freedesktop-sdk into girderstream elements (not maintained)
* [GrdTools](https://gitlab.com/pointswaves/grdtools/)
  This builds up a tool chain including native and cross-compilers and has some
  example target elements.

Some projects that build target systems that you can install your programs into
and provide BSPs so you can build full disk images for varius targets.

* [Girderstream Example Project](https://gitlab.com/pointswaves/girderstream-test-project)
  Provides base x86 and aarch64 userlands including busybox, gnu with optional systemd.
  Provides board support for qemu, pi3 pi4 and pi zero 2.

Example Projects using girderstream as there intergation tool.

* [Big Yellow Camera](https://gitlab.com/pointswaves/pi-camera)
  A open source point and shoot camera project running a girderstream-example-project
  userland and BSP that it adds its camera app into.

If you are using girderstream please create a MR adding your project(s)

## Test

To run the test you should have [buildbox](https://gitlab.com/BuildGrid/buildbox/buildbox) installed. Both `buildbox-casd`, `casupload` and `buildbox-run` should both
be on your PATH and work correctly.

Don't forget to create a symlink for buildbox-run eg `ln -s buildbox-run-bubblewrap buildbox-run`

Check that they can run with `buildbox-run --help` and `buildbox-casd --help`

You can then run the tests with

```bash
cargo test
```

## Verify buildbox-casd

You can check your buildbox-casd is functioning with the girderstream-cas-check tool.

First create a .remotes file eg:
```bash
cat > .remotes <<EOF
remotes:
- url: "http://0.0.0.0:50040"
  instance: ""
  push: true
EOF
```

Then start your casd

```bash
buildbox-casd  --bind localhost:50040 /tmp/cas/cas &

```
Then run the test tool

```bash
cargo run --bin girderstream-cas-check
```

This should finish with `{"Result":"Success","Status":"Finished"}` and its exit code should be 0.

### Install locally

You can install girderstream to your local rust folder, `~/.cargo/bin/`, with cargo. If you have installed rust with `rustup` then this folder should be on your path, if not then add it and you will be able to run.

```bash
cargo install --path crates/girderstream --locked
```

Then you can run girderstream

```bash
girderstream --help
```

As a basic smoke test you can build some of the testing elements.

To check the cas you can `build` a import element from the griderstream repo.

```bash
tar xf crates/girderstream/tests/build_tests/files/busybox.tar.xz
buildbox-casd  --bind localhost:50040 /tmp/cas/cas &
casupload --remote=http://localhost:50040 busybox2
girderstream --project-root crates/girderstream/tests/build_tests/ build --name stage0
```

Note that the busybox.tar.xz file is a git-lfs object if the tar file is very small then please ensure that git-lfs is installed and you have pulled the real tar file with it.

And to check that the build sandbox is working you can do a basic build

```bash
girderstream --project-root crates/girderstream/tests/build_tests/ build --name basic_build
```

### Using docker

First pull down the latest build from the git lab registries

``` bash
docker pull registry.gitlab.com/girderstream/docker-images/girderstream-ci-base:latest
```

Currently this docker image is mostly used in ci so is a little ruff round the edges
but should work effectively.

``` bash
$ docker run -it -v `pwd`:/project:Z --privileged --workdir /project/ -v ~/.cache/girderstream/docker:/tmp/cas:Z registry.gitlab.com/girderstream/docker-images/girderstream-ci-base:latest
# buildbox-casd  --bind localhost:50040 /tmp/cas &
# girderstream --help
# girderstream -v show --name busybox
# girderstream build --name busybox
```
