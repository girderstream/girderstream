// Copyright 2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Provide entry points for running commands in a sanbox
//!
//! Both running sandboxed commands at a given mount point and running commands on a fuse files
//! sytem with recorded changes to the file system.
use std::{
    fmt,
    io::Read,
    thread::spawn,
    {fs, path::PathBuf},
};

use crossbeam_channel::{select, unbounded, Receiver, RecvError};
use fuser::MountOption;
use hakoniwa::{Container, ExitStatus, Namespace, Runctl, Stdio};
use libc::pid_t;
use nix::{
    sys::wait::{waitpid, WaitStatus},
    unistd::Pid,
};
use reapi::build::bazel::remote::execution::v2::{
    command::EnvironmentVariable,
    {Digest, OutputDirectory},
};
use tempdir::TempDir;
use thiserror::Error;
use tokio::{sync::broadcast, task::JoinError};
use tracing::{info, warn};

use crate::{
    digest_from_tree, directory_blob_to_tree_block, directory_from_digest_block,
    fuse::ReapiFilesystem,
};

#[derive(Error, Debug)]
pub enum ExecutionError {
    #[error(transparent)]
    Hakoniwa(#[from] hakoniwa::Error),
    #[error("Pipe issues")]
    RecvError(#[source] RecvError),
    #[error("failed to read the key file")]
    JoinError(#[source] JoinError),
    #[error("canceled while building")]
    CancelationError,
    #[error("canceled while building")]
    CancelationErrorData(YabaExectionResult),
    #[error("canceled while building")]
    CancelationErrorResult(YabaBuildResult),
}

pub struct SandboxMountSettings {
    pub arguments: Vec<String>,
    pub root_digest: Digest,
    pub with_network: bool,
    pub environment_variables: Vec<EnvironmentVariable>,
    pub interactive: bool,
    pub cwd: Option<String>,
}

impl From<SandboxMountSettings> for SandboxSettings {
    fn from(sandbox_mount_options: SandboxMountSettings) -> Self {
        SandboxSettings {
            arguments: sandbox_mount_options.arguments,
            with_network: sandbox_mount_options.with_network,
            environment_variables: sandbox_mount_options.environment_variables,
            interactive: sandbox_mount_options.interactive,
            cwd: sandbox_mount_options.cwd,
        }
    }
}

pub struct SandboxSettings {
    pub arguments: Vec<String>,
    pub with_network: bool,
    pub environment_variables: Vec<EnvironmentVariable>,
    pub interactive: bool,
    pub cwd: Option<String>,
}

pub struct YabaExectionResult {
    pub status: Option<ExitStatus>,
    pub stdout: Vec<u8>,
    pub stderr: Vec<u8>,
}

impl fmt::Debug for YabaExectionResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("YabaBuildResult")
            .field("exit status", &self.status)
            .field("stdout", &String::from_utf8_lossy(&self.stdout))
            .field("stderr", &String::from_utf8_lossy(&self.stderr))
            .finish()
    }
}

pub struct YabaBuildResult {
    pub status: Option<ExitStatus>,
    pub stdout: Vec<u8>,
    pub stderr: Vec<u8>,
    pub output_directories: Vec<OutputDirectory>,
}

impl fmt::Debug for YabaBuildResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("YabaBuildResult")
            .field("exit status", &self.status)
            .field("stdout", &String::from_utf8_lossy(&self.stdout))
            .field("stderr", &String::from_utf8_lossy(&self.stderr))
            .field("output directories", &self.output_directories)
            .finish()
    }
}

/// Mount a Yaba fuse file system and run a command with that root
pub async fn mount_and_execute(
    root: &PathBuf,
    sandbox_options: SandboxMountSettings,
    cancelation: broadcast::Sender<()>,
) -> Result<YabaBuildResult, ExecutionError> {
    let build_dir = root.join("tmp/");
    fs::create_dir_all(&build_dir).unwrap();
    let mountpoint_owner = TempDir::new_in(build_dir, "yaba-exe").unwrap();
    let mountpoint = mountpoint_owner.path().to_path_buf();
    let options = vec![MountOption::RW, MountOption::FSName("hello".to_string())];

    let (sender, receiver) = std::sync::mpsc::channel();

    let cas_fs = ReapiFilesystem::new(root.to_owned(), sandbox_options.root_digest.clone(), sender);

    let filesystem = fuser::spawn_mount2(cas_fs, &mountpoint, &options).unwrap();
    let mut cancelation = cancelation.subscribe();

    let (canceler, canceled) = unbounded();
    let (signal_finished, recive_finished) = unbounded();
    let result = {
        let build_future = tokio::task::spawn_blocking(move || {
            let returned_action = execute_action(mountpoint, sandbox_options.into(), canceled);
            let _ = signal_finished.send(());
            returned_action
        });
        let drop_able_future = tokio::task::spawn_blocking(move || {
            let _ = recive_finished.recv();
        });
        let _bob = tokio::select! {
          val = drop_able_future => {
                val
                    .map_err(ExecutionError::JoinError)
            }
          _val = cancelation.recv() => {
                let _ = canceler.send(());
                Err(ExecutionError::CancelationError)
            }
        };
        let build_future = build_future.await;
        build_future.map_err(ExecutionError::JoinError)
    };

    info!("Drop fuse filesystem to trigger capture");
    drop(filesystem);
    let root_digest = receiver.recv();

    drop(mountpoint_owner);
    let output_paths = vec![
        "output_dir".to_string(),
        "sources".to_string(),
        ".".to_string(),
    ];
    let mut output_directories = vec![];

    if let Ok(root_digest) = root_digest {
        for path in output_paths {
            if path == "." {
                output_directories.push(OutputDirectory {
                    path: path.clone(),
                    tree_digest: Some(digest_from_tree(
                        root,
                        &directory_blob_to_tree_block(root, root_digest.clone()),
                    )),
                })
            } else {
                let root_dir = directory_from_digest_block(root, root_digest.clone());
                for subdir in &root_dir.directories {
                    if subdir.name == path {
                        output_directories.push(OutputDirectory {
                            path: subdir.name.clone(),
                            tree_digest: Some(digest_from_tree(
                                root,
                                &directory_blob_to_tree_block(root, subdir.digest.clone().unwrap()),
                            )),
                        })
                    }
                }
            }
        }
    };

    info!("Build Complete {result:?}");

    match result? {
        Ok(result) => Ok(YabaBuildResult {
            output_directories,
            status: result.status,
            stdout: result.stdout,
            stderr: result.stderr,
        }),
        Err(ExecutionError::CancelationErrorData(result)) => Ok(YabaBuildResult {
            output_directories,
            status: result.status,
            stdout: result.stdout,
            stderr: result.stderr,
        }),
        Err(_e) => Ok(YabaBuildResult {
            status: None,
            stdout: vec![],
            stderr: vec![],
            output_directories,
        }),
    }
}

#[allow(clippy::result_large_err)]
/// Code to launch a command inside a Hakoniwa sandbox
pub fn execute_action(
    mount_point: PathBuf,
    sandbox_options: SandboxSettings,
    canceler: Receiver<()>,
) -> Result<YabaExectionResult, ExecutionError> {
    let mut sandbox = Container::new();
    sandbox.bindmount_rw("/dev/null", "/dev/null");
    sandbox.bindmount_rw("/dev/random", "/dev/random");
    sandbox.bindmount_rw("/dev/urandom", "/dev/urandom");
    sandbox.bindmount_rw("/dev/zero", "/dev/zero");
    sandbox.tmpfsmount("/tmp/");
    sandbox.uidmap(0);
    sandbox.gidmap(0);
    sandbox.runctl(Runctl::RootdirRW);

    if !sandbox_options.with_network {
        sandbox.unshare(Namespace::Network);
    };

    sandbox.rootdir(mount_point);

    let prog = sandbox_options.arguments.first().unwrap();
    let mut argv = sandbox_options.arguments.clone();
    argv.remove(0);
    let mut executor = sandbox.command(prog);
    executor.args(argv);
    for env in sandbox_options.environment_variables {
        executor.env(&env.name, &env.value);
    }

    if sandbox_options.interactive {
        executor.stdout(Stdio::inherit());
        executor.stderr(Stdio::inherit());
        executor.stdin(Stdio::inherit());
    } else {
        executor.stdout(Stdio::piped());
        executor.stderr(Stdio::piped());
        executor.stdin(Stdio::piped());
    }

    let mut child = executor.spawn().map_err(ExecutionError::Hakoniwa)?;
    drop(child.stdin.take());
    let mut stdout = child.stdout.take().unwrap();
    let mut stderr = child.stderr.take().unwrap();

    let child_pid: pid_t = child.id() as i32;
    let child_pid = Pid::from_raw(child_pid);

    let (working, working_stop) = unbounded();

    let stdout_data = spawn(move || {
        let mut data = vec![];
        let _ = stdout.read_to_end(&mut data);
        data
    });
    let stderr_data = spawn(move || {
        let mut data = vec![];
        let _ = stderr.read_to_end(&mut data);
        data
    });

    let task = spawn(move || {
        let good = match waitpid(Some(child_pid), None) {
            Ok(WaitStatus::Exited(pid, status)) => {
                if pid == child_pid {
                    if status == 0 {
                        true
                    } else {
                        info!("Child process exited with error");
                        false
                    }
                } else {
                    warn!("Unknown process ID");
                    false
                }
            }
            Ok(info) => {
                warn!("Child process did not exit normally: {info:?}");
                false
            }
            Err(_) => {
                warn!("Error waiting for child process");
                false
            }
        };
        let _ = working.send(good);
    });

    let wait_result = select! {
        recv(canceler) -> cancel_msg => {
            info!("Killer: {cancel_msg:?}");
            child.kill().unwrap();
            Err(ExecutionError::CancelationError)
        },
        recv(working_stop) -> exit_type => {
            info!("Child exited: {exit_type:?}");
            let _ = exit_type.map_err(ExecutionError::RecvError);
            Ok(())
        },
    };
    drop(task);

    match child.wait() {
        Ok(build_result) => {
            let build_result = YabaExectionResult {
                status: Some(build_result),
                stdout: stdout_data.join().unwrap(),
                stderr: stderr_data.join().unwrap(),
            };

            if let Err(ExecutionError::CancelationError) = wait_result {
                Err(ExecutionError::CancelationErrorData(build_result))
            } else {
                Ok(build_result)
            }
        }
        Err(_err) => {
            let build_result = YabaExectionResult {
                status: None,
                stdout: stdout_data.join().unwrap(),
                stderr: stderr_data.join().unwrap(),
            };

            Err(ExecutionError::CancelationErrorData(build_result))
        }
    }
}
