// Copyright 2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is the main libary containing common help fuctions used accross yaba
use sha2::{Digest as _, Sha256};
use std::{
    collections::VecDeque,
    fs,
    io::{Read, Write},
    path::{Path, PathBuf},
};

use tokio::io::AsyncReadExt;

use tokio::fs::File;

use prost::Message;
use reapi::build::bazel::remote::execution::v2::{Digest, Directory, Tree};

pub mod execution;
pub mod fuse;
pub mod server;

pub fn digest_to_path(root: &Path, digest: &Digest) -> PathBuf {
    let root = root.join("cas/objects");
    root.join(&digest.hash[..2]).join(&digest.hash[2..])
}

pub fn digest_to_tmp_path(root: &Path, digest: &Digest) -> PathBuf {
    let root = root.join("cas/tmp/");
    root.join(&digest.hash)
}

pub async fn read_blob(root: &Path, blob_digest: &Digest) -> Vec<u8> {
    if blob_digest.size_bytes == 0 {
        return vec![];
    }
    let new_path = digest_to_path(root, blob_digest);

    if !new_path.exists() {
        panic!("Blob not in cas")
    }

    let mut blob = vec![];
    let mut blob_file = File::open(&new_path).await.unwrap();
    blob_file.read_to_end(&mut blob).await.unwrap();
    blob
}

pub fn read_blob_block(path: &Path, blob_digest: &Digest) -> Vec<u8> {
    if blob_digest.size_bytes == 0 {
        return vec![];
    }
    let new_path = digest_to_path(path, blob_digest);

    if !new_path.exists() {
        panic!("Blob not in cas, {new_path:?}")
    }

    let mut blob = vec![];
    let mut blob_file = std::fs::File::open(&new_path).unwrap();
    blob_file.read_to_end(&mut blob).unwrap();
    blob
}

/// Fetch and decode a directory from the cas
///
/// Fetch a REAPI directory from the cas and decode
/// it. Returns the `Directory` structure represented
/// by the given digest.
pub async fn directory_from_digest(root: &Path, digest: Digest) -> Directory {
    let dir_blob = read_blob(root, &digest).await;

    Directory::decode(&*dir_blob).expect("could not decode directory")
}

/// Fetch and decode a directory from the cas
///
/// Fetch a REAPI directory from the cas and decode
/// it. Returns the `Directory` structure represented
/// by the given digest.
pub fn directory_from_digest_block(root: &Path, digest: Digest) -> Directory {
    let dir_blob = read_blob_block(root, &digest);

    Directory::decode(&*dir_blob).expect("could not decode directory")
}

pub fn digest_from_directory(root: &Path, directory: Directory) -> Digest {
    let mut buf: Vec<u8> = vec![];
    directory.encode(&mut buf).unwrap();
    upload_blob_block(root, buf)
}

pub fn digest_from_tree(root: &Path, tree: &Tree) -> Digest {
    let mut buf: Vec<u8> = Vec::new();
    tree.encode(&mut buf).unwrap();
    upload_blob_block(root, buf)
}

pub fn upload_blob_block(root: &Path, blob: Vec<u8>) -> Digest {
    let mut dig = Sha256::new();
    dig.update(&blob);
    let sha = format!("{:x}", dig.finalize());
    let blob_digest = Digest {
        size_bytes: blob.len() as i64,
        hash: sha,
    };

    let new_path = digest_to_path(root, &blob_digest);
    if !new_path.exists() {
        fs::create_dir_all(new_path.parent().unwrap()).unwrap();

        let mut fl = fs::File::create(new_path).unwrap();
        fl.write_all(&blob).unwrap();
    };

    blob_digest
}

pub fn directory_blob_to_tree_block(root: &Path, root_digest: Digest) -> Tree {
    let root_dir = directory_from_digest_block(root, root_digest.clone());
    let mut tree = Tree {
        root: Some(root_dir.clone()),
        children: Vec::new(),
    };

    let mut queue = VecDeque::new();
    queue.push_back(root_dir);
    while let Some(dir) = queue.pop_front() {
        // Iterate dir
        for subdir in &dir.directories {
            let subdigest = subdir.digest.clone().unwrap();
            let subdir = directory_from_digest_block(root, subdigest);
            queue.push_back(subdir);
        }
        tree.children.push(dir);
    }

    tree
}
