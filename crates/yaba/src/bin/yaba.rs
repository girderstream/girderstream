// Copyright 2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is a entry point for using yaba as a cas server
use std::path::PathBuf;

use clap::Parser;

use yaba::server::serve;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Cli {
    /// Specify the url of the local cas endpoint
    #[clap(long)]
    bind: Option<String>,
    /// Specify the instance of the local cas
    location: Option<PathBuf>,
    #[arg(short, long)]
    protect_session_blobs: bool,
    #[clap(long)]
    writeable: Option<bool>,
}

#[tokio::main]
async fn main() {
    println!("Hello, world!");
    tracing_subscriber::fmt().init();
    println!("Serve, world!");
    let cli = Cli::parse();
    println!("cli: {cli:?}");

    println!("{:?}", cli.bind);
    let port = if let Some(dir) = cli.bind {
        dir.split(':').last().unwrap().parse::<u16>().unwrap()
    } else {
        50040
    };

    serve(
        ([0, 0, 0, 0], port).into(),
        cli.location,
        cli.writeable.unwrap_or(true),
    )
    .await;
}
