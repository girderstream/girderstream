// Copyright 2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is a entry point for using yaba to mount a fuse filesystem
use clap::Parser;
use fuser::MountOption;
use reapi::build::bazel::remote::execution::v2::Digest;
use std::io::{stdin, stdout, Write};
use std::path::PathBuf;
use std::sync::mpsc::channel;
use yaba::fuse::ReapiFilesystem;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[clap(long)]
    mount_point: String,
    #[clap(long)]
    auto_unmount: Option<bool>,
    #[clap(long)]
    allow_root: Option<bool>,
    #[clap(long)]
    cas_location: Option<PathBuf>,
}

fn main() {
    let cli = Cli::parse();

    tracing_subscriber::fmt().init();
    let mountpoint = cli.mount_point;
    let mut options = vec![
        MountOption::RW,
        MountOption::FSName("yaba_fuse".to_string()),
    ];
    if cli.auto_unmount.unwrap_or(true) {
        options.push(MountOption::AutoUnmount);
    }
    if cli.allow_root.unwrap_or(false) {
        options.push(MountOption::AllowRoot);
    }

    let (sender, receiver) = channel();

    let cas_fs = ReapiFilesystem::new(
        cli.cas_location.unwrap_or(
            PathBuf::from(std::env::var("HOME").unwrap()).join(".cache/girderstream/cas/"),
        ),
        Digest {
            hash: "215282d240f0edde8cfe72f5d1a0908b874db227433a185e6fbb04b6d980f516".to_string(),
            size_bytes: 1183,
        },
        sender,
    );

    println!("cas_fs: {cas_fs:?}");

    let filesystem = fuser::spawn_mount2(cas_fs, mountpoint, &options).unwrap();

    let mut s = String::new();
    print!("Please enter some text: ");
    let _ = stdout().flush();
    stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");
    println!("You typed: {}", s);

    drop(filesystem);

    let reply = receiver.recv().unwrap();
    println!("reply: {reply:?}");
}
