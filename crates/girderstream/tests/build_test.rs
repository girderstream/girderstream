// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{collections::HashMap, path::PathBuf, sync::Arc};

use common::RemoteConfigOptions;
use girderstream::{misc::copy_dir_all, project, scheduler, Crapshoot};
use reapi_tools::{
    fs_io::upload_tar, test_infra::YabaTestSetup, virtual_fs::VirtualDir, FullConfig, LocalConfig,
    RemoteConfig,
};
use tokio::{fs::File, io::AsyncReadExt, process::Command, sync::broadcast};

mod common;

/// This uses the public api of the girderstream lib to build a project
///
/// This is less complete of a end to end test as the similar one that
/// uses girderstream its self. But it lest us make a number of assertions
/// as we build the project which will give us a lot more options to catch
/// issues and hopefully give more helpful diagnostics  if a change brakes
/// something.
#[tokio::test]
async fn test_build_by_parts() -> Crapshoot {
    println!("start the test");
    let yts = YabaTestSetup::setup_server().await?;
    let running_yts = yts.run_cas_thread().await;
    let tmp_dir_test_path = running_yts.get_temp_root();
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let full_config = FullConfig::new(config.clone(), vec![]);
    let mut connection = config.into();
    println!("upload");
    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("basic_build")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    scheduler::build(
        &full_config,
        &build_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await?;
    println!("built build");

    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;
    println!("Checked out");

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .expect("Could not open file 'test_source_second.output'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'test_source_second.output'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    let build_result = build_element
        .get_build_artifact(&mut full_config.get_local().to_owned().into())
        .await?;
    let stdout = build_result
        .stdout(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stdout
        .expect("Build did not have a stdout")
        .contains("message to stdout"));

    let stderr = build_result
        .stderr(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stderr
        .expect("Build did not have a stderr")
        .contains("message to stderr"));

    println!("finished just drop casd");
    drop(running_yts);
    Ok(())
}

/// This test local sources plugin works for a bootstrap import
///
/// This does not check a build sandbox interacts correctly
#[tokio::test]
async fn test_source_bootstrap() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let running_yts = yts.run_cas_thread().await;
    let tmp_dir_test_path = running_yts.get_temp_root();
    let address = running_yts.get_address().to_owned();
    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    println!("about to setup project");

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let full_config = FullConfig::new(config.clone(), vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        tmp_dir_project.to_path_buf().into(),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let local_element = project.get_element(&Arc::from("local_config")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &local_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    scheduler::build(
        &full_config,
        &local_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await?;
    println!("built build");

    local_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;
    println!("Checked out");
    let mut connection = config.into();
    let build = local_element.get_build_artifact(&mut connection).await?;
    println!("build {:?}", build);

    //    println!("{:?}", tmp_dir_checkout);
    //  sleep(Duration::from_secs(6));
    //println!("{:?}", tmp_dir_checkout);
    //sleep(Duration::from_secs(30));

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("readme.md"))
        .await
        .expect("Could not open file 'readme.md'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'readme.md'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("test the local file source"));

    let build_result = local_element
        .get_build_artifact(&mut full_config.get_local().to_owned().into())
        .await?;
    let stdout = build_result
        .stdout(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stdout.is_none());

    let stderr = build_result
        .stderr(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stderr.is_none());

    println!("finished just drop casd");
    drop(running_yts);

    Ok(())
}

/// This test local sources plugin works for a full build
///
/// This extends the test_source_bootstrap test to also
/// interact with the build sandbox
#[tokio::test]
async fn test_source_build() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let running_yts = yts.run_cas_thread().await;
    let tmp_dir_test_path = running_yts.get_temp_root();
    let address = running_yts.get_address().to_owned();
    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let full_config = FullConfig::new(config.clone(), vec![]);
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);
    println!("about to setup project");

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        tmp_dir_project.to_path_buf().into(),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let local_element = project.get_element(&Arc::from("local_build")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &local_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await?;
    println!("built build");

    local_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;
    println!("Checked out");
    let build = local_element.get_build_artifact(&mut connection).await?;
    println!("build {:?}", build);

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("readme.md"))
        .await
        .expect("Could not open file 'readme.md'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'readme.md'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("test the local file source"));

    let build_result = local_element
        .get_build_artifact(&mut full_config.get_local().to_owned().into())
        .await?;
    let stdout = build_result
        .stdout(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stdout.is_some());

    let stderr = build_result
        .stderr(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stderr.is_some());

    println!("finished just drop casd");
    drop(running_yts);

    Ok(())
}
/// This test checks that Girderstream its self can build a project
///
/// This is a more complete integration test as it uses Girderstream
/// as it is but it does not give as much information about why things
/// might have gone wrong and does not let use make assertions about
/// internal state at any points throughout the build.
#[tokio::test]
async fn test_build_by_tool() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");
    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    drop(running_yts);
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = common::get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();
    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));
    assert!(girderstream_stage0.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    println!("Girderstream stage0: {:?}", girderstream_build);
    println!("{}", String::from_utf8_lossy(&girderstream_build.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_build.stderr));
    assert!(girderstream_build.status.success());

    println!("Girderstream basic_build: {:?}", girderstream_build);
    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();

    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    let _ = tmp_dir_test_path;
    Ok(())
}

/// This test checks that Girderstream can pull a cas import source
#[tokio::test]
async fn test_build_with_pull_cas() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let yts_remote = YabaTestSetup::setup_server().await?;
    let running_yts_remote = yts_remote.run_cas_thread().await;
    let address_remote = running_yts.get_address().to_owned();

    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");
    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    common::create_remotes_config(
        tmp_dir_project.join(".remotes"),
        &running_yts_remote,
        RemoteConfigOptions { push: false },
    )
    .await?;

    let remote = RemoteConfig::new(address_remote, "".to_string(), true, None, false);
    let mut remote_connection = remote.into();

    let upload = upload_tar(
        &mut remote_connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = common::get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();

    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));
    assert!(girderstream_stage0.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    assert!(girderstream_build.status.success());

    println!("Girderstream basic_build: {:?}", girderstream_build);
    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();

    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    drop(running_yts);
    drop(running_yts_remote);
    Ok(())
}

/// This test checks that Girderstream can push and then pull down a cached
/// successful build.
#[tokio::test]
async fn test_build_with_push_pull() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let yts_remote = YabaTestSetup::setup_server().await?;
    let running_yts_remote = yts_remote.run_cas_thread().await;

    let yts_second = YabaTestSetup::setup_server().await?;
    let running_yts_second = yts_second.run_cas_thread().await;
    let address_second = running_yts_second.get_address();
    let tmp_dir_test_path_second = yts_second.get_temp_root();
    let tmp_dir_checkout = tmp_dir_test_path_second.join("checkout");

    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    common::create_remotes_config(
        tmp_dir_project.join(".remotes"),
        &running_yts_remote,
        RemoteConfigOptions { push: true },
    )
    .await?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = common::get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();

    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));
    assert!(girderstream_stage0.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    println!("Girderstream basic_build: {:?}", girderstream_build);
    println!("{}", String::from_utf8_lossy(&girderstream_build.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_build.stderr));
    assert!(girderstream_build.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(address_second)
        .arg("--cas-location")
        .arg(tmp_dir_test_path_second)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        //.arg("stage0")
        .output()
        .await
        .unwrap();
    println!("Girderstream pull_build: {:?}", girderstream_build);
    println!("{}", String::from_utf8_lossy(&girderstream_build.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_build.stderr));
    assert!(girderstream_build.status.success());

    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(address_second)
        .arg("--cas-location")
        .arg(tmp_dir_test_path_second)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();
    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    drop(running_yts);
    drop(running_yts_remote);
    drop(running_yts_second);
    Ok(())
}

/// This test checks that environment variables are passed on
///
/// This has some variables going in and others excluded by the configuration
#[tokio::test]
async fn test_build_environment_vars() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        Some(project_config),
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("env_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    scheduler::build(
        &full_config,
        &build_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built build");

    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("var_files"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("first env var value1"));
    assert!(content.contains("second env var value2"));

    drop(running_yts);
    Ok(())
}

/// This test checks that failing source dont success fully build
///
/// This should fail in a way that helps let the user know why
#[tokio::test]
async fn test_build_failing_source() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new_default(address, "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        Some(project_config),
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let bad_source_element = project.get_element(&Arc::from("bad_source")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    assert!(scheduler::build(
        &full_config,
        &bad_source_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await
    .is_err());
    println!("built build");

    let build_result = bad_source_element
        .get_build_success(&mut full_config.get_local().to_owned().into())
        .await;
    println!("result: {build_result:?}");
    assert!(!build_result.unwrap());

    drop(running_yts);
    Ok(())
}

/// This test checks that empty sources dont success fully build
///
/// This should fail in a way that helps let the user know why
#[tokio::test]
async fn test_build_empty_source() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new_default(address, "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        Some(project_config),
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let bad_source_element = project.get_element(&Arc::from("bad_source")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    assert!(scheduler::build(
        &full_config,
        &bad_source_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await
    .is_err());
    println!("built build");

    let build_result = bad_source_element
        .get_build_success(&mut full_config.get_local().to_owned().into())
        .await;
    println!("result: {build_result:?}");
    assert!(!build_result.unwrap());

    drop(running_yts);
    Ok(())
}

/// This test demonstrates variables in a context passing into a element
///
/// This test element moves the vars into the artifact and then checks
/// that the correct variables are present. It also makes sure the the
/// when works in the context.
#[tokio::test]
async fn test_build_variables() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.clone().into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let full_config = FullConfig::new(config, vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("var_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    scheduler::build(
        &full_config,
        &build_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await?;
    println!("built build");

    //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("plugin_vars.yaml"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("shell: /bin/sh"));
    assert!(content.contains("bob: bob_value"));
    assert!(content.contains("sam: tom"));

    drop(running_yts);
    Ok(())
}

/// This test demonstrates variables in a context passing into a element
///
/// This has some variables going in and others excluded by the configuration
#[tokio::test]
async fn test_build_variables_config_second() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.clone().into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        Some(project_config),
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("var_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    scheduler::build(
        &full_config,
        &build_element,
        locked_project.clone(),
        false,
        cancelation_issuer,
    )
    .await?;
    println!("built build");

    //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("plugin_vars.yaml"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("shell: /bin/sh"));
    assert!(content.contains("bob: bob_value"));
    assert!(!content.contains("sam: tom"));

    drop(running_yts);
    Ok(())
}

/// Check that the build artifact of a failed build creates logs
///
/// In order to debug failed builds we need to be able to export the
/// stdout and stderr of the build and a prerequisite is that after a failed
/// build the stdout and stderr is correctly saved in the failed build artifact.
#[tokio::test]
async fn test_build_failed_logs() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");
    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to build project");
    let girderstream_binary = common::get_test_bin_dir().join("girderstream");
    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("failed_build")
        .output()
        .await
        .unwrap();

    println!("Girderstream failed_build: {:?}", girderstream_build);
    println!("{}", String::from_utf8_lossy(&girderstream_build.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_build.stderr));
    assert!(!girderstream_build.status.success());

    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(&address)
        .arg("--cas-location")
        .arg(tmp_dir_test_path)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("export-logs")
        .arg("--name")
        .arg("failed_build")
        .arg("--location")
        .arg(format!("{}/logs", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();
    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("logs").join("failed_build.stdout"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("Message to stdout"));

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("logs").join("failed_build.stderr"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("Message to stderr"));

    drop(running_yts);
    Ok(())
}

/// Check that cached failed builds dont trigger a build
#[tokio::test]
async fn test_build_after_failure() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.into();

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new_default(address, "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let failed_element = project.get_element(&Arc::from("failed_build")).await?;
    let dep_failed_element = project.get_element(&Arc::from("dep_failure")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    // Once we do the work of having proper errors we can check for failed build
    assert!(scheduler::build(
        &full_config,
        &failed_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone()
    )
    .await
    .is_err());
    let mut connection = full_config.get_local().to_owned().into();
    assert!(!failed_element.get_build_success(&mut connection).await?);

    // Once we do the work of having proper errors we can check for did not start build due to failed dep
    assert!(scheduler::build(
        &full_config,
        &dep_failed_element,
        locked_project.clone(),
        false,
        cancelation_issuer
    )
    .await
    .is_err());

    drop(running_yts);
    Ok(())
}
