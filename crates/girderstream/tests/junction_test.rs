// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{path::PathBuf, sync::Arc};

use girderstream::{
    misc::copy_dir_all,
    project::{self},
    scheduler, Crapshoot,
};
use reapi_tools::{
    fs_io::{upload_dir, upload_tar},
    test_infra::YabaTestSetup,
    virtual_fs::VirtualDir,
    FullConfig, LocalConfig,
};
use tokio::sync::broadcast;

// test that we can load a element over a junction
#[tokio::test]
async fn test_load_by_source() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.clone().into();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/junction_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("Upload: {:?}", upload);
    let upload = upload_dir(
        &mut connection,
        PathBuf::from(format!(
            "{}",
            tmp_dir_project.join("files").join("junction1").display()
        ))
        .as_path(),
    )
    .await?;

    println!("Upload junc1: {:?}", upload);

    let path = PathBuf::from(format!(
        "{}",
        tmp_dir_project.join("files").join("junction2").display()
    ));
    let upload = upload_dir(&mut connection, path.as_path()).await?;

    println!("Upload junc2: {:?}", upload);

    println!("About to setup project");

    let full_config = FullConfig::new(config, vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        true,
        true,
        cancelation_issuer.clone(),
    )
    .await?;

    println!("Fully loaded project");
    let build_element = project.get_element(&Arc::from("cross_junc")).await?;
    {
        let locked_project = Arc::new(project);
        scheduler::build(
            &full_config,
            &build_element,
            locked_project.clone(),
            true,
            cancelation_issuer,
        )
        .await?;
        println!("Built build");

        //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
        build_element
            .checkout(
                &mut full_config.get_local().to_owned().into(),
                &format!("{}", tmp_dir_checkout.display()),
                false,
                false,
            )
            .await?;
    }

    drop(running_yts);
    Ok(())
}

// Todo check that a souce plugin can be loaded across junction

// Todo check that a build plugin can be loaded across junction

// Check that two jucntions can be loaded and there element dep on each other
#[tokio::test]
async fn test_load_over_two_juncs() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.clone().into();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/junction_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("Upload: {:?}", upload);

    let upload = upload_dir(
        &mut connection,
        PathBuf::from(format!(
            "{}",
            tmp_dir_project.join("files").join("junction1").display()
        ))
        .as_path(),
    )
    .await?;

    println!("Upload junc1: {:?}", upload);

    let upload = upload_dir(
        &mut connection,
        PathBuf::from(format!(
            "{}",
            tmp_dir_project.join("files").join("junction2").display()
        ))
        .as_path(),
    )
    .await?;

    println!("Upload junc2: {:?}", upload);

    println!("About to setup project");

    let full_config = FullConfig::new(config, vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        true,
        true,
        cancelation_issuer.clone(),
    )
    .await?;

    println!("Loaded project");

    let build_element = project.get_element(&Arc::from("built_with_junc")).await?;
    dbg!(&project);
    {
        let locked_project = Arc::new(project);
        scheduler::build(
            &full_config,
            &build_element,
            locked_project.clone(),
            true,
            cancelation_issuer,
        )
        .await?;
        println!("Built build");

        build_element
            .checkout(
                &mut full_config.get_local().to_owned().into(),
                &format!("{}", tmp_dir_checkout.display()),
                false,
                false,
            )
            .await?;
    }

    drop(running_yts);
    Ok(())
}
