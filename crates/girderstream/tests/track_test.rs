// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{path::PathBuf, sync::Arc};

use tokio::{fs::File, io::AsyncReadExt, sync::broadcast};

use girderstream::{misc::copy_dir_all, project, scheduler, Crapshoot};
use reapi_tools::{
    fs_io::upload_tar, test_infra::YabaTestSetup, virtual_fs::VirtualDir, FullConfig, LocalConfig,
};

/// This uses the public api of the girderstream lib to build a project
///
/// This is less complete of a end to end test as the similar one that
/// uses girderstream its self. But it lest us make a number of assertions
/// as we build the project which will give us a lot more options to catch
/// issues and hopefully give more helpful diagnostics  if a change brakes
/// something.
#[tokio::test]
async fn test_tack() -> Crapshoot {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let running_yts = yts.run_cas_thread().await;
    let address = running_yts.get_address().to_owned();

    let config = LocalConfig::new_yaba(address.clone(), "".to_string(), tmp_dir_test_path.into());
    let mut connection = config.clone().into();

    let tmp_dir_project = tmp_dir_test_path.join("project");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    let upload = upload_tar(
        &mut connection,
        &PathBuf::from("tests/build_tests/files/busybox.tar.xz"),
    )
    .await?;
    println!("upload: {:?}", upload);

    println!("about to setup project");

    let full_config = FullConfig::new(config, vec![]);

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let mut project = project::project::Project::new(
        &full_config,
        VirtualDir::from(tmp_dir_project.to_path_buf()),
        None,
        false,
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let track_element = project.get_element(&Arc::from("track_element")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(
        &full_config,
        &stage0_element,
        locked_project.clone(),
        false,
        cancelation_issuer.clone(),
    )
    .await?;
    println!("built stage0");

    track_element
        .track(&locked_project, &full_config, cancelation_issuer)
        .await?;

    println!("tracked");
    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_project.join("elements").join("track_element.grd"))
        .await
        .expect("Could not open file 'test_source_second.output'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'test_source_second.output'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("sha: \"new_value\""));

    println!("finished just drop casd");
    drop(running_yts);
    Ok(())
}
