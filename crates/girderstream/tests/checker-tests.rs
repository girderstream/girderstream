// Copyright 2022
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use std::process::Output;

use anyhow::Context;
use common::RemoteConfigOptions;
use reapi_tools::test_infra::YabaTestSetup;
use serde_json::Value;
use tokio::{
    fs::{self},
    process::Command,
};

mod common;

fn validate_output(output: &Output) {
    println!("exit code: {:?}", output.status);
    println!("==stdout===\n{}", String::from_utf8_lossy(&output.stdout));
    println!("==stderr===\n{}", String::from_utf8_lossy(&output.stderr));

    assert!(output.status.success());
    assert!(output.stderr.is_empty());

    let lines_out = output
        .stdout
        .split(|&b| b == b'\n')
        .filter(|line| !line.iter().all(u8::is_ascii_whitespace))
        .map(|l| {
            serde_json::from_slice(l)
                .with_context(|| format!("deserialising: {}", String::from_utf8_lossy(l)))
        })
        .collect::<anyhow::Result<Vec<Value>>>()
        .expect("Must parse stdout as newline delimited json");

    let get_str_field = |line: &Value, key: &str| -> Option<String> {
        let Value::String(status) = line.get(key)? else {
            panic!("Line {line:?} invalid field {key}");
        };
        Some(status.clone())
    };

    let mut seen_ok_status = false;

    for line in &lines_out {
        let Some(status) = get_str_field(line, "Status") else {
            continue;
        };
        match status.to_ascii_lowercase().as_str() {
            "ok" => {
                seen_ok_status = true;
            }

            "start" | "begin" => {}
            "finished" => {
                assert_eq!(
                    get_str_field(line, "Result")
                        .expect("Line with finished status must have Result field"),
                    "Success",
                    "line was {line:?}"
                );
            }
            _ => {
                panic!("Bad line {line:?} had status {status}");
            }
        }
    }

    assert!(seen_ok_status, "atleast one line must have Ok status");
}

/// This test checks the girderstream-cas-cas-check cli
#[tokio::test]
async fn test_cas_check() -> anyhow::Result<()> {
    let yts = YabaTestSetup::setup_server().await?;
    let tmp_dir_test_path = yts.get_temp_root();
    let tmp_dir_project = tmp_dir_test_path.join("project");
    let running_yts = yts.run_cas_thread().await;

    fs::create_dir(&tmp_dir_project).await?;

    common::create_remotes_config(
        tmp_dir_project.join(".remotes"),
        &running_yts,
        RemoteConfigOptions { push: true },
    )
    .await?;

    let binary = common::get_test_bin_dir().join("girderstream-cas-check");
    let output = Command::new(&binary)
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .output()
        .await
        .unwrap();

    validate_output(&output);

    drop(running_yts);
    Ok(())
}

/// This test checks the girderstream-remote-asset-check cli
#[tokio::test]
async fn test_remote_asset_check() -> anyhow::Result<()> {
    let yts = YabaTestSetup::setup_server().await?;
    let running_yts = yts.run_cas_thread().await;

    let tmp_file = tempfile::NamedTempFile::with_prefix("remote-config")?;

    common::create_remotes_config(&tmp_file, &running_yts, RemoteConfigOptions { push: true })
        .await?;

    let binary = common::get_test_bin_dir().join("girderstream-remote-asset-check");
    let output = Command::new(&binary)
        .arg("--config")
        .arg(tmp_file.path())
        .arg("--log-level")
        .arg("info")
        .arg("--json-logging")
        .output()
        .await
        .unwrap();

    validate_output(&output);

    drop(running_yts);
    Ok(())
}
