use anyhow::Context;
use reapi_tools::test_infra::YabaTestServer;
use std::path::Path;
use tokio::{
    fs::{self, File},
    io::AsyncWriteExt,
};

pub fn get_test_bin_dir() -> std::path::PathBuf {
    // Cargo puts the integration test binary in target/debug/deps
    let current_exe =
        std::env::current_exe().expect("Failed to get the path of the integration test binary");
    let current_dir = current_exe
        .parent()
        .expect("Failed to get the directory of the integration test binary");

    let test_bin_dir = current_dir
        .parent()
        .expect("Failed to get the binary folder");
    test_bin_dir.to_owned()
}

pub struct RemoteConfigOptions {
    pub push: bool,
}

pub async fn create_remotes_config(
    path: impl AsRef<Path>,
    yaba: &YabaTestServer<'_>,
    opts: RemoteConfigOptions,
) -> anyhow::Result<()> {
    File::create(&path)
        .await
        .context("creating remotes file")?
        .write_all(
            format!(
                r#"remotes:
    - url: "{}"
      instance: ""
      push: {}
    "#,
                yaba.get_address(),
                opts.push,
            )
            .as_bytes(),
        )
        .await
        .context("writing content to .remotes file")?;

    println!(
        "Created .remotes file with content: {:?}",
        fs::read_to_string(path).await?
    );

    Ok(())
}
