// Copyright 2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Extra yaml handling functions
//!
//! Girderstream tries to use serde yaml as much as possible but some of the things
//! that we need to handle tasks like tracking are not possible with serde.
//!
//! Ideally all of these functions would be upstream-ed to some kind of yaml handling
//! library as they are not specific to a meta-build-tool.

use std::collections::HashMap;

use itertools::Itertools;

/// Takes a string containing yaml and returns a new string with updated values
///
/// Updates the value of the given key
pub fn update_values_in_source(raw: String, updates: HashMap<String, String>) -> String {
    let mut new_strings = vec![];

    for line in raw.split('\n') {
        // Todo:
        // * only look for key after finding "source"
        // * stop looking for key after source stops
        // * after finding source look for tab level
        let mut change = false;
        for (key, value) in &updates {
            if line.trim().contains(key) {
                change = true;
                // Todo: use existing tab level
                let start: String = line.chars().take_while(|ch| ch.is_whitespace()).collect();
                let value = if value.chars().all(char::is_alphanumeric) {
                    value.to_owned()
                } else {
                    format!("\"{value}\"")
                };
                new_strings.push(format!("{start}{key}: {value}"));
                break;
            }
        }

        if !change {
            new_strings.push(line.to_string());
        }
    }

    // Todo:
    // * fail if key not found, or add key to end of source?
    new_strings.join("\n")
}

/// Takes a string containing yaml and returns a new string with updated values
///
/// Updates the value of the given key
pub fn chunked_grd_file(raw: String) -> anyhow::Result<(String, Vec<String>, String)> {
    println!("raw {:?}", raw);

    let mut begining = String::new();
    let mut start_source = false;
    let mut lines = raw.split('\n').peekable();
    for line in lines.by_ref() {
        begining.push_str(line);
        begining.push('\n');
        if line.starts_with("sources:") {
            start_source = true;
            break;
        }
    }
    if !start_source {
        anyhow::bail!("No source block detected")
    }
    let mut split_sources = vec![];
    let mut this_source = String::new();
    while let Some(next) = lines.peek() {
        if let Some(first_char) = next.chars().next() {
            if !" -".contains(first_char) {
                break;
            } else {
                let next = lines.next().unwrap();
                if !this_source.is_empty() && next.trim().starts_with('-') {
                    split_sources.push(this_source);
                    this_source = String::new();
                }
                this_source.push_str(next);
                this_source.push('\n');
            }
        } else {
            break;
        }
    }
    split_sources.push(this_source);
    let end = lines.join("\n");

    Ok((begining, split_sources, end))
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::{chunked_grd_file, update_values_in_source};

    #[test]
    fn basic_split() {
        let raw = "key: value\nsources:\n  - bob: 5\n sam: 3\n- bob: 6\nother: stuff\n".to_string();

        let (beginning, sources, end) = chunked_grd_file(raw).unwrap();

        println!("{} {:?} {}", beginning, sources, end);
        assert_eq!(beginning, "key: value\nsources:\n");
        assert_eq!(sources.len(), 2);
        assert_eq!(end, "other: stuff\n");
    }

    #[test]
    fn basic_split_gap() {
        let raw =
            "key: value\nsources:\n  - bob: 5\n sam: 3\n- bob: 6\n\nother: stuff\n".to_string();

        let (beginning, sources, end) = chunked_grd_file(raw).unwrap();

        println!("{} {:?} {}", beginning, sources, end);
        assert_eq!(beginning, "key: value\nsources:\n");
        assert_eq!(sources.len(), 2);
        assert_eq!(end, "\nother: stuff\n");
    }

    #[test]
    fn basic_replace() {
        let input = "- bob: 5\n  sam: 3\n".to_string();
        let updates = HashMap::from([("sam".to_string(), "5".to_string())]);
        println!("{} {:?}", input, updates);
        let new = update_values_in_source(input, updates);
        assert!(new.contains("sam: 5"));

        println!("{}", new);
    }

    #[test]
    fn more_spaces_replace() {
        let input = "  - bob: 5\n    sam: 3\n".to_string();
        let updates = HashMap::from([("sam".to_string(), "5".to_string())]);
        println!("{} {:?}", input, updates);
        let new = update_values_in_source(input, updates);
        assert!(new.contains("\n    sam: 5\n"));

        println!("{}", new);
    }

    #[test]
    fn space_replace() {
        let input = "  - bob: 5\n    sam: 3\n".to_string();
        let updates = HashMap::from([("sam".to_string(), "5 7".to_string())]);
        println!("{} {:?}", input, updates);
        let new = update_values_in_source(input, updates);
        assert!(new.contains("\n    sam: \"5 7\"\n"));

        println!("{}", new);
    }
}
