// Copyright 2023 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// Perform a build locally
//! Translation layer between the Girderstream specific build functions and REAPI
//!
//! Currently this wraps buildbox-run but could be expended with minimal reworking
//! to also support remote execution over REAPI.
use crate::build::{GrdActionResult, ReapiResult};
use reapi_tools::{
    grpc_io::upload_blob,
    remote::{from_tree_to_list, pull_cached_cas, push_blobs},
    LocalConfig, ReapiConnection, RemoteConfig,
};

use anyhow::bail;
use prost::Message;
use reapi::{
    build::bazel::remote::execution::v2::{
        execution_stage, Action, ExecuteOperationMetadata, ExecuteRequest, ExecuteResponse,
        WaitExecutionRequest,
    },
    google::{
        bytestream::ReadRequest,
        longrunning::operation::Result::{Error, Response},
        rpc::Code,
    },
};

use super::CommandResult;

/// This takes a REAPI action and submits it to a remote execution service
///
/// This is still very basic, but does work as a proof of concept.
pub(crate) async fn run_action(
    connection_local: &mut ReapiConnection,
    remote_executer: &RemoteConfig,
    action: Action,
) -> anyhow::Result<CommandResult> {
    let mut buf: Vec<u8> = vec![];
    action.encode(&mut buf).unwrap();
    let mut connection_remote = ReapiConnection::from(LocalConfig::new(
        remote_executer.get_endpoint(),
        "".to_string(),
        "false".to_string(),
    ));

    let mut connection_executor = ReapiConnection::from(remote_executer.clone());
    let mut execution_cache_client = connection_executor
        .get_execution_client()
        .await?
        .ok_or(anyhow::anyhow!("Could not create execution client"))?
        .clone();

    let bsc = connection_executor
        .get_logstream_client()
        .await?
        .ok_or(anyhow::anyhow!("Could not create execution client"))?;

    let build_root_digest = action.input_root_digest.unwrap().clone();

    // the list tree and the push blobs should be combined, so if parents are present then there tree is not checked.
    let mut dirs = from_tree_to_list(connection_local, &build_root_digest).await?;
    dirs.push(action.command_digest.unwrap().clone());
    dirs.push(build_root_digest.clone());
    println!("Got the dirs to push");
    push_blobs(connection_local, &mut connection_remote, dirs).await?;

    println!("Uploaded ready for action");

    let action_digest = upload_blob(&mut connection_remote, buf).await?;

    let execution_request = ExecuteRequest {
        instance_name: remote_executer.get_instance_name().to_owned(),
        skip_cache_lookup: true,
        action_digest: Some(action_digest),
        execution_policy: None,
        results_cache_policy: None,
    };

    let streaming_operation = execution_cache_client.execute(execution_request).await?;
    let (_, mut streaming, _) = streaming_operation.into_parts();

    while let Some(message) = streaming.message().await.expect("loopy") {
        if let Some(raw_meta) = message.metadata {
            if raw_meta.type_url
                == "type.googleapis.com/build.bazel.remote.execution.v2.ExecuteOperationMetadata"
            {
                let eom = ExecuteOperationMetadata::decode(raw_meta.value.as_slice())?;
                println!("ExecuteOperationMetadata: {eom:?}");
            } else {
                bail!("Un expected message metadata type")
            }
        }
        if let Some(Response(raw_res)) = message.result {
            if raw_res.type_url
                == "type.googleapis.com/build.bazel.remote.execution.v2.ExecuteResponse"
            {
                let execution_response = ExecuteResponse::decode(raw_res.value.as_slice())?;
                println!("ExecuteResponse: {execution_response:?}");
            } else {
                bail!("Un expected message result type")
            }
        }

        println!("Waiting for remote execution request");
        let wait_execution_request = WaitExecutionRequest { name: message.name };
        let thing = execution_cache_client
            .wait_execution(wait_execution_request)
            .await?;
        let (_, mut streaming, _) = thing.into_parts();

        while let Some(message) = streaming.message().await.expect("loopy") {
            if let Some(raw_meta) = message.metadata {
                // This arm handles Meta data that might provide streaming stdout or stderr while the build continues
                if raw_meta.type_url
                    == "type.googleapis.com/build.bazel.remote.execution.v2.ExecuteOperationMetadata"
                {

                    let execution_meta: ExecuteOperationMetadata = ExecuteOperationMetadata::decode(raw_meta.value.as_slice()).unwrap();
                    if !execution_meta.stderr_stream_name.is_empty() {
                        let read_request = ReadRequest{ resource_name: execution_meta.stderr_stream_name.clone(), read_offset: 0, read_limit: 0 };
                        let streaming_read_request = bsc.read(read_request).await.unwrap();
                        let (_ , mut streaming, _) = streaming_read_request.into_parts();
                        loop {
                            match  streaming.message().await {
                                Ok(message) => {
                                    if let Some(message) = message {
                                        println!("Message stderr: {message:?}");
                                        if message.data.is_empty(){
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                },
                                Err(er) => println!("Error: {er:?}")
                            }
                        }

                        let read_request = ReadRequest{ resource_name: execution_meta.stdout_stream_name.clone(), read_offset: 0, read_limit: 0 };
                        let streaming_read_request = bsc.read(read_request).await.unwrap();
                        let (_ , mut streaming, _) = streaming_read_request.into_parts();
                        loop {
                            match  streaming.message().await {
                                Ok(message) => {
                                    if let Some(message) = message {
                                        println!("Message stderr: {message:?}");
                                        if message.data.is_empty(){
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                },
                                Err(er) => println!("Error: {er:?}")
                            }
                        }
                    }

                    if execution_meta.stage() == execution_stage::Value::Completed {
                    }

                }
            }

            // This arm handles completed builds and turns them into a `CommandResult`
            if let Some(raw_res) = message.result {
                match raw_res {
                    Response(raw_res) => {
                        if raw_res.type_url
                            == "type.googleapis.com/build.bazel.remote.execution.v2.ExecuteResponse"
                        {
                            let execution_response =
                                ExecuteResponse::decode(raw_res.value.as_slice()).unwrap();
                            if let Some(result) = execution_response.result {
                                for directory in &result.output_directories {
                                    pull_cached_cas(
                                        connection_local,
                                        &mut connection_remote,
                                        directory.tree_digest.as_ref().unwrap(),
                                    )
                                    .await?;
                                }

                                return Ok(CommandResult {
                                    run_result: crate::build::RunnerResults::Reapi(ReapiResult {
                                        code: 200,
                                        meta_data: "".to_string(),
                                    }),
                                    action: Some(
                                        GrdActionResult::from_action_results(
                                            &mut connection_remote,
                                            result,
                                        )
                                        .await?,
                                    ),
                                });
                            }
                        }
                    }
                    Error(grpc_status) => {
                        println!("Oh dear: {grpc_status:?}");
                        let failure_enum = Code::try_from(grpc_status.code)?;
                        println!("Failed build: {failure_enum:?}");
                        return Ok(CommandResult {
                            run_result: crate::build::RunnerResults::Reapi(ReapiResult {
                                code: grpc_status.code,
                                meta_data: "".to_string(),
                            }),
                            action: None,
                        });
                    }
                }
            }
        }
    }

    bail!("Final message streaming response was incorrect or not handled properly")
}
