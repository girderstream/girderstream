// Copyright 2022-2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// Perform a build locally
//! Translation layer between the Girderstream specific build functions and REAPI
//!
//! Currently this wraps buildbox-run but could be expended with minimal reworking
//! to also support remote execution over REAPI.
use std::process::Stdio;

use prost::Message;
use reapi::build::bazel::remote::execution::v2::{Action, ActionResult};
use tempfile::tempdir;
use tokio::{
    fs,
    io::{AsyncReadExt, AsyncWriteExt},
    process::Command,
};

use crate::build::GrdActionResult;
use reapi_tools::{grpc_io::upload_blob, ReapiConnection};

use super::{CommandResult, RunnerResults, SubprocessResult};

/// This takes a REAPI action and invokes buildbox-run
///
/// This function is buildbox specific and requires that buildbox-run be on the
/// path. It uses a tokio task and awaits for it to complete.
///
/// In the future we should investigate if this function could return something
/// or write to something to report progress especially for plugins like cmake
/// were a present complete output can be generated.
///
/// # Arguments
///
/// - connection: &mut [`ReapiConnection`] - Connection to the cas
/// - action: [`Action`]` - The REAPI action to run
/// - interactive: [`bool`], - Weather or not to create a interactive or background run of the sandbox
pub(crate) async fn run_action(
    connection: &mut ReapiConnection,
    action: Action,
    interactive: bool,
) -> anyhow::Result<CommandResult> {
    let mut buf: Vec<u8> = vec![];
    action.encode(&mut buf).unwrap();

    let dir = tempdir()?;

    let action_path = dir.path().join("action.buf");
    let result_path = dir.path().join("result.di");
    let mut fl = fs::File::create(&action_path).await?;
    fl.write_all(&buf).await?;
    fl.flush().await?;

    // Todo:  handle if connection.get_instance() is not blank
    let full_build = connection.get_local_exec().unwrap();

    match full_build {
        reapi_tools::Executor::Buildbox(full_build) => {
            let mut split_command = full_build.split(' ');
            let mut command = Command::new(split_command.next().unwrap());

            for extra in split_command {
                command.arg(extra);
            }

            command
                .arg(format!("--remote={}", connection.get_endpoint()))
                .arg(format!("--action={}", action_path.display()))
                .arg(format!("--action-result={}", result_path.display()))
                .kill_on_drop(true);

            if interactive {
                command.arg("--no-logs-capture");
                let _ = command.spawn()?.wait().await?;
                anyhow::bail!("Always stuff after interactive shell")
            } else {
                command.stdin(Stdio::piped());

                let result = command.output();

                let buildbox_result = result.await.map_err(|e| {
            anyhow::format_err!("Could not run `buildbox-run`. Error: '{:?}' Please check buildbox is installed correctly", e)
        })?;

                let stdout_di = upload_blob(connection, buildbox_result.stdout).await?;
                let stderr_di = upload_blob(connection, buildbox_result.stderr).await?;

                if let Ok(mut fl) = fs::File::open(&result_path).await {
                    let mut buf = vec![];
                    fl.read_to_end(&mut buf).await?;
                    let result = ActionResult::decode(buf.as_ref())
                        .map_err(|err| anyhow::anyhow!("Could not decode action result {err:?}"))?;
                    Ok(CommandResult {
                        action: Some(
                            GrdActionResult::from_action_results(connection, result).await?,
                        ),
                        run_result: RunnerResults::BuildboxRun(SubprocessResult {
                            exit_status: buildbox_result.status.code().unwrap_or(99),
                            stdout: stdout_di,
                            stderr: stderr_di,
                        }),
                    })
                } else {
                    Ok(CommandResult {
                        action: None,
                        run_result: RunnerResults::BuildboxRun(SubprocessResult {
                            exit_status: buildbox_result.status.code().unwrap_or(99),
                            stdout: stdout_di,
                            stderr: stderr_di,
                        }),
                    })
                }
            }
        }
        reapi_tools::Executor::Yaba(_) => {
            panic!("Logic failure! Only buildbox builds should get here!");
        }
    }
}
