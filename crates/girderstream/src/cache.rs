// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Push and fetch artifacts from the cache
// This is currently very simple as we just consider the local cache but we will
// need to expand this once we support multiple cache servers and remote builds.

use anyhow::bail;
use reapi::build::bazel::remote::execution::v2::{Digest, FindMissingBlobsRequest};
use sha2::{Digest as _, Sha256};

use crate::{
    project::{
        element::{ElementRef, SourceDetails},
        plugin::SourcePlugin,
        project::Project,
        BuildResult, FetchResult,
    },
    Crapshoot,
};
use reapi_tools::{
    grpc_io::{fetch_asset, fetch_blob, push_asset, upload_blob},
    ReapiConnection,
};

const BUILD_ASSET_KEY: &str = "0.0.6";

/// Calculate the assets key used to identify a cached build
///
/// This key is used when adding or retrieving cached builds from the
/// REAPI asset store.
pub(crate) fn build_asset_key_by_parts(_project: &Project, kind: &str, cache_key: &str) -> String {
    format!("{}::{}::{}", BUILD_ASSET_KEY, kind, cache_key)
}

const FETCH_ASSET_KEY: &str = "0.0.5";

/// Calculate the assets key used to identify a cached fetch
///
/// This key is used when adding or retrieving cached sources from the
/// REAPI asset store.
pub(crate) fn fetch_asset_key(
    root_digest: &Digest,
    source_plugin: &SourcePlugin,
    source_map: &SourceDetails,
) -> String {
    let mut dig = Sha256::new();
    dig.update(root_digest.hash.as_bytes());
    dig.update(root_digest.size_bytes.to_ne_bytes());
    // get_unique_bytes includes things like cli and is similar to `element_ref.cache_key`
    dig.update(source_plugin.get_unique_bytes(source_map));
    format!(
        "{}:{}:{}",
        FETCH_ASSET_KEY,
        source_plugin.get_kind(),
        hex::encode(dig.finalize()),
    )
}

/// Add a build result for a element to the cache
pub(crate) async fn cache_build(
    connection: &mut ReapiConnection,
    _project: &Project,
    element_ref: &ElementRef,
    build_result: &BuildResult,
) -> Crapshoot {
    let asset_key = element_ref.build_asset_key();

    let encoded_result = bincode::serialize::<BuildResult>(build_result).unwrap();
    let encoded_result_digest = upload_blob(connection, encoded_result).await?;
    let reference_directories = if let Some(result) = build_result.get_result_digest() {
        vec![result.to_owned()]
    } else {
        vec![]
    };

    push_asset(
        connection,
        &asset_key.to_string(),
        encoded_result_digest,
        reference_directories,
    )
    .await?;

    Ok(())
}

/// Add a fetch result for a source to the cache
pub(crate) async fn cache_fetch(
    connection: &mut ReapiConnection,
    asset_key: &String,
    fetch_result: &FetchResult,
) -> Crapshoot {
    let encoded_result = bincode::serialize::<FetchResult>(fetch_result).unwrap();
    let encoded_result_digest = upload_blob(connection, encoded_result).await?;
    let reference_directories = if let Some(result) = fetch_result.get_result_digest() {
        vec![result.to_owned()]
    } else {
        vec![]
    };

    push_asset(
        connection,
        asset_key,
        encoded_result_digest,
        reference_directories,
    )
    .await?;

    Ok(())
}

/// Retrieve a cached builds result from the cache
pub(crate) async fn get_cached_build(
    connection: &mut ReapiConnection,
    element_ref: &ElementRef,
) -> anyhow::Result<BuildResult> {
    let asset_key = element_ref.build_asset_key();

    let build_asset_digest = fetch_asset(connection, asset_key.to_string()).await?;
    let build_asset_blob = fetch_blob(connection, &build_asset_digest).await?;

    let build_result = bincode::deserialize::<BuildResult>(&build_asset_blob)?;

    let instance_name = connection.get_instance_name().to_string();
    let casc = connection.get_cas_client().await?;

    for blobs in build_result.get_ref_blob().chunks(10_000) {
        let fmbr = FindMissingBlobsRequest {
            instance_name: instance_name.to_string(),
            blob_digests: blobs.to_vec(),
        };
        let fmbres = casc.find_missing_blobs(fmbr).await?;
        let (meta_map, res, _) = fmbres.into_parts();
        assert_eq!(meta_map.into_headers().get("grpc-status").unwrap(), "0");
        if !res.missing_blob_digests.is_empty() {
            bail!("Cas is missing some blobs")
        }
    }

    Ok(build_result)
}

/// Retrieve a cached sources result from the cache
pub(crate) async fn get_cached_fetch(
    connection: &mut ReapiConnection,
    asset_key: String,
) -> anyhow::Result<FetchResult> {
    let build_asset_digest = fetch_asset(connection, asset_key).await?;
    let build_asset_blob = fetch_blob(connection, &build_asset_digest).await?;

    let encoded_result = bincode::deserialize::<FetchResult>(&build_asset_blob).unwrap();

    let instance_name = connection.get_instance_name().to_string();
    let casc = connection.get_cas_client().await?;

    for blobs in encoded_result.get_ref_blob().chunks(10_000) {
        let fmbr = FindMissingBlobsRequest {
            instance_name: instance_name.to_string(),
            blob_digests: blobs.to_vec(),
        };
        let fmbres = casc.find_missing_blobs(fmbr).await?;
        let (meta_map, res, _) = fmbres.into_parts();
        assert_eq!(meta_map.into_headers().get("grpc-status").unwrap(), "0");
        if !res.missing_blob_digests.is_empty() {
            bail!("Cas is missing some blobs")
        }
    }

    Ok(encoded_result)
}
