// Copyright 2024
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is the main girderstream cli entry point
//!
//! Test fetching from and pushing to a remote-asset server specified by a
//! girderstream config file.

use anyhow::bail;
use anyhow::ensure;
use anyhow::Context;
use clap::Parser;
use prost::Message;
use rand::RngCore;
use rand::{
    distr::{Alphanumeric, StandardUniform},
    rng,
    rngs::StdRng,
    Rng, SeedableRng,
};
use reapi::build::bazel::remote::asset::v1::Qualifier;
use reapi::build::bazel::remote::{asset::v1::FetchDirectoryRequest, execution::v2::Directory};
use reapi::build::bazel::remote::{
    asset::v1::{FetchBlobRequest, PushBlobRequest, PushDirectoryRequest},
    execution::v2::Digest,
    execution::v2::DirectoryNode,
    execution::v2::FileNode,
};
use reapi_tools::grpc_io::fetch_blob;
use reapi_tools::{grpc_io::upload_blob, ReapiConnection, RemoteConfig, RemotesConfig};
use sha2::{Digest as _, Sha256};
use std::future::Future;
use std::{
    borrow::Cow,
    ops::Deref,
    path::{Path, PathBuf},
    time::{Duration, SystemTime},
};
use tonic::Code;
use tracing::Instrument;
use tracing::Level;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Cli {
    /// Specify the config file to use.
    #[clap(long, short)]
    config: PathBuf,

    /// Filter tests containing this string.
    #[clap(long)]
    filter: Option<String>,

    /// Seed -- running twice with the same seed won't work as assets will
    /// already be in cache.
    #[clap(long)]
    seed: Option<u64>,

    /// Run tests that check behaviour when uploading bad assets.
    #[clap(long, default_value = "false")]
    run_fail_tests: bool,

    /// Max log level to print
    #[clap(long)]
    log_level: Option<tracing_subscriber::filter::Directive>,

    /// Print logs in json format
    #[clap(long)]
    json_logging: bool,

    /// Add an extra test that will always fail. Useful for checking error
    /// reporting works.
    #[clap(long)]
    add_failing_test: bool,
}

async fn remote_config(config_file: &Path) -> anyhow::Result<RemoteConfig> {
    let remotes = RemotesConfig::open_from_file(config_file)
        .await
        .context("Could not read remotes file")?;

    anyhow::ensure!(
        remotes.get_remotes().len() == 1,
        "remote config file must specify exactly one remote"
    );

    remotes.get_remotes()[0]
        .clone()
        .try_into()
        .context("building root")
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    setup_log(&cli);

    let remote_config = remote_config(&cli.config)
        .await
        .context("getting remote config")?;

    tracing::debug!("Got config: {remote_config:?}");

    ensure!(
        remote_config.can_push(),
        "to check the remote asset we must be able to push to it"
    );

    let seed: u64 = cli.seed.unwrap_or_else(|| rng().next_u64());

    tracing::info!("seed is {seed}");

    let mut r = StdRng::seed_from_u64(seed);

    let tests = vec![
        (
            Asset::blob_rand(StdRng::from_rng(&mut r), AssetType::Blob),
            "blob test",
        ),
        (
            Asset::empty_blob(StdRng::from_rng(&mut r), AssetType::Blob),
            "empty blob test",
        ),
        (
            {
                let mut rng = StdRng::from_rng(&mut r);

                let mut asset = Asset::blob_rand(&mut rng, AssetType::Blob);

                asset.add_qualifier(rand_string(&mut rng), rand_string(&mut rng));
                asset.add_qualifier(rand_string(&mut rng), rand_string(&mut rng));

                asset
            },
            "blob test with random qualifiers",
        ),
        (
            {
                let mut asset = Asset::blob_rand(StdRng::from_rng(&mut r), AssetType::Blob);

                asset.add_qualifier("resource_type", "application/x-git");

                asset
            },
            "blob test with special qualifiers",
        ),
        (
            Asset::bad_directory(StdRng::from_rng(&mut r), AssetType::Blob),
            "bad directory test as blob",
        ),
        (
            Asset::good_directory(StdRng::from_rng(&mut r), AssetType::Blob),
            "good directory test as blob",
        ),
        (
            Asset::good_directory(StdRng::from_rng(&mut r), AssetType::Directory),
            "good directory test as dir",
        ),
    ];

    let fail_tests = vec![
        (
            Asset::blob_rand(StdRng::from_rng(&mut r), AssetType::Directory),
            // TODO(harrysarson): this is probably testing implementation details.
            Code::InvalidArgument,
            "digest in directory asset does not reference a Directory",
            "blob as directory test",
        ),
        (
            Asset::bad_directory(StdRng::from_rng(&mut r), AssetType::Directory),
            // TODO(harrysarson): this is probably testing implementation details.
            Code::InvalidArgument,
            "unknown digest function",
            "bad directory test as directory",
        ),
    ];

    let mut program_res = Ok(());

    for (asset, name) in tests {
        let test_fut = test(&remote_config, asset);
        run_test(&cli, test_fut, name, &mut program_res)
            .instrument(tracing::span!(
                Level::INFO,
                "test",
                Test = name,
                Endpoint = remote_config.get_endpoint()
            ))
            .await;
    }

    if cli.run_fail_tests {
        for (asset, code, message, name) in fail_tests {
            let test_fut = test_push_fails(&remote_config, asset, code, message);
            run_test(&cli, test_fut, name, &mut program_res)
                .instrument(tracing::span!(
                    Level::INFO,
                    "fail_test",
                    Test = name,
                    Endpoint = remote_config.get_endpoint()
                ))
                .await;
        }
    }

    if cli.add_failing_test {
        let test_fut = async {
            bail!("This test fails, use for testing");
        };
        let name = "Dummy test, that will fail";
        run_test(&cli, test_fut, name, &mut program_res)
            .instrument(tracing::span!(
                Level::INFO,
                "fail_test",
                Test = name,
                Endpoint = remote_config.get_endpoint()
            ))
            .await;
    }

    tracing::info!("Finishing test");

    program_res.map_err(|_| anyhow::format_err!("One or more tests failed, see logs above"))
}

async fn push_asset(asset: &Asset, conf: &RemoteConfig) -> anyhow::Result<Digest> {
    let mut remote_connection = ReapiConnection::from(conf.clone());

    // First add data to CAS

    let mut blob_digest = None;

    for blob in asset.payloads() {
        let tmp_digest = upload_blob(&mut remote_connection, blob.to_vec())
            .await
            .context("uploading blob to CAS")?;
        tracing::info!("Uploaded blob and got back digest {tmp_digest:?}");

        let fetched_blob = fetch_blob(&mut remote_connection, &tmp_digest)
            .await
            .with_context(|| format!("fetching {tmp_digest:?} from CAS (checking roundtrips)"))?;

        assert_eq!(fetched_blob, blob, "blob must roundtrip");

        blob_digest.get_or_insert(tmp_digest);
    }

    let Some(blob_digest) = blob_digest else {
        bail!("Atleast one blob needed for push");
    };

    tracing::info!("Uploaded asset with digest {blob_digest:?}");

    // Then push asset

    let cap = remote_connection
        .get_asset_push_client()
        .await
        .context("get_asset_push_client")?;

    match asset.type_ {
        AssetType::Blob => {
            cap.push_blob(PushBlobRequest {
                instance_name: conf.get_instance_name().clone(),
                qualifiers: qualifiers(asset),
                uris: vec![format!(
                    "urn:fdc:girderstream.build:2020:artifact:{}",
                    asset.key()
                )],
                expire_at: Some((SystemTime::now() + Duration::from_secs(60 * 60)).into()),
                blob_digest: Some(blob_digest.clone()),
                references_blobs: vec![],
                references_directories: vec![],
            })
            .await
            .context("push_blob")?;
        }
        AssetType::Directory => {
            cap.push_directory(PushDirectoryRequest {
                instance_name: conf.get_instance_name().clone(),
                qualifiers: qualifiers(asset),
                uris: vec![format!(
                    "urn:fdc:girderstream.build:2020:artifact:{}",
                    asset.key()
                )],
                expire_at: Some((SystemTime::now() + Duration::from_secs(60 * 60)).into()),
                root_directory_digest: Some(blob_digest.clone()),
                references_blobs: vec![],
                references_directories: vec![],
            })
            .await
            .context("push_directory")?;
        }
    };

    Ok(blob_digest)
}

async fn fetch_asset(asset: &Asset, conf: &RemoteConfig) -> anyhow::Result<Digest> {
    let mut remote_connection = ReapiConnection::from(conf.clone());

    let cap = remote_connection
        .get_asset_fetch_client()
        .await
        .context("get_asset_fetch_client")?;

    let uris = vec![format!(
        "urn:fdc:girderstream.build:2020:artifact:{}",
        asset.key()
    )];
    let qualifiers = qualifiers(asset);
    let resulting_digest = match asset.type_ {
        AssetType::Blob => {
            let request = FetchBlobRequest {
                instance_name: conf.get_instance_name().clone(),
                qualifiers,
                uris,
                oldest_content_accepted: None,
                timeout: None,
            };

            let response = cap.fetch_blob(request).await.context("fetch_blob")?;
            response.into_inner().blob_digest.unwrap()
        }
        AssetType::Directory => {
            let request = FetchDirectoryRequest {
                instance_name: conf.get_instance_name().clone(),
                qualifiers,
                uris,
                oldest_content_accepted: None,
                timeout: None,
            };

            let response = cap
                .fetch_directory(request)
                .await
                .context("fetch_directory")?;
            response.into_inner().root_directory_digest.unwrap()
        }
    };

    Ok(resulting_digest)
}

fn qualifiers(asset: &Asset) -> Vec<Qualifier> {
    asset
        .qualifiers
        .iter()
        .cloned()
        .map(|(name, value)| Qualifier { name, value })
        .collect()
}

async fn test_push_fails(
    remote_config: &RemoteConfig,
    asset: Asset,
    code: tonic::Code,
    message: &str,
) -> anyhow::Result<()> {
    tracing::debug!("Trying to push");

    match push_asset(&asset, remote_config)
        .await
        .context("pushing asset")
    {
        Ok(_) => {
            tracing::warn!("Pushing asset succeeded when it should have failed");

            bail!("Should not be able to push asset");
        }
        Err(e) => {
            let Some(grpc_error) = e.downcast_ref::<tonic::Status>() else {
                tracing::warn!("pushing gave wrong error type {e:?}");
                return Err(e.context("could not convert error to grpc error"));
            };

            if grpc_error.code() != code {
                tracing::warn!("pushing had wrong GRPC status code {grpc_error} (wanted {code})");
                return Err(e.context("wrong GRPC status code"));
            }
            let msg = grpc_error.message();
            if !msg.to_lowercase().contains(&message.to_lowercase()) {
                tracing::warn!(
                    "pushing had wrong GRPC message '{msg}' (need to contain '{message}')"
                );
                return Err(e.context("wrong GRPC message"));
            }

            tracing::info!("pushing asset failed (as expected) with error {e:?}");

            Ok(())
        }
    }
}

async fn test(remote_config: &RemoteConfig, asset: Asset) -> anyhow::Result<()> {
    tracing::info!("Created asset {asset:?}");

    tracing::debug!("Trying to fetch");

    match fetch_asset(&asset, remote_config).await {
        Ok(a) => {
            tracing::warn!("fetching asset returned {a:?}");
            bail!("Should not be able to fetch asset");
        }
        Err(e) => {
            let Some(grpc_error) = e.downcast_ref::<tonic::Status>() else {
                tracing::warn!("fetching gave wrong error type {e:?}");
                return Err(e.context("could not convert error to grpc error"));
            };

            if !matches!(grpc_error.code(), tonic::Code::NotFound) {
                tracing::warn!("fetching had wrong GRPC status code {grpc_error}");
                return Err(e.context("wrong GRPC status code"));
            }

            tracing::info!("fetching asset failed (as expected) with error {e:?}");
        }
    }

    tracing::debug!("Trying to push");

    let pushed_digest = push_asset(&asset, remote_config)
        .await
        .context("pushing asset")?;

    tracing::info!("Pushing asset succeeded");

    tracing::debug!("Trying to fetch");

    let fetched_digest = fetch_asset(&asset, remote_config).await?;

    tracing::info!("Fetching asset succeeded (output was {fetched_digest:?})");

    assert_eq!(
        pushed_digest, fetched_digest,
        "Pushed and fetched digests must match"
    );

    Ok(())
}

async fn run_test(
    cli: &Cli,
    test_fut: impl Future<Output = anyhow::Result<()>>,
    name: &str,
    program_res: &mut anyhow::Result<()>,
) {
    if let Some(filter) = &cli.filter {
        if !name.contains(filter) {
            return;
        }
    }
    let test_res = test_fut.await;

    if let Err(e) = &test_res {
        tracing::error!(Status = "Error", Error = ?e, "Test {name} failed with error: {e:?}");
        *program_res = test_res;
    } else {
        tracing::info!(Status = "Ok", "Test {name} passed");
    }
}
#[derive(Debug)]
struct Asset {
    key: String,
    payload: Vec<B64Debug>,
    qualifiers: Vec<(String, String)>,
    type_: AssetType,
}

/// Helps avoid printing long byte sequences in logs.
struct B64Debug(Box<[u8]>);

impl Deref for B64Debug {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::fmt::Debug for B64Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "skipped")
    }
}

impl Asset {
    fn empty_blob(mut r: impl Rng, type_: AssetType) -> Self {
        Self {
            key: rand_string(&mut r),
            payload: vec![B64Debug(Box::new([]))],
            qualifiers: vec![],
            type_,
        }
    }
    fn blob_rand(mut r: impl Rng, type_: AssetType) -> Self {
        let len = r.random_range(10..=100);
        Self {
            key: rand_string(&mut r),
            payload: vec![B64Debug(
                (&mut r).sample_iter(StandardUniform).take(len).collect(),
            )],
            qualifiers: vec![],
            type_,
        }
    }

    fn bad_directory(mut r: impl Rng, type_: AssetType) -> Self {
        let dir = Directory {
            files: vec![FileNode {
                name: "this file is missing".to_string(),
                digest: None,
                is_executable: true,
                node_properties: None,
            }],
            directories: vec![DirectoryNode {
                name: "this dir is missing".to_string(),
                digest: None,
            }],
            symlinks: vec![],
            node_properties: None,
        };

        let bytes = dir.encode_to_vec();

        Self {
            key: rand_string(&mut r),
            qualifiers: vec![],
            payload: vec![B64Debug(bytes.into_boxed_slice())],
            type_,
        }
    }

    fn good_directory(mut r: impl Rng, type_: AssetType) -> Self {
        let file1 = [
            b"This is a file\nisn't this fun\nyes it is\n",
            rand_string(&mut r).as_bytes(),
        ]
        .concat();
        let file2 = [&vec![b'o'; 700], rand_string(&mut r).as_bytes()].concat();
        let subdir1 = Directory {
            files: vec![FileNode {
                name: "file2".to_string(),
                digest: Some(digest(&file2)),
                is_executable: true,
                node_properties: None,
            }],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let subdir2 = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let dir = Directory {
            files: vec![FileNode {
                name: "file1".to_string(),
                digest: Some(digest(&file1)),
                is_executable: true,
                node_properties: None,
            }],
            directories: vec![
                DirectoryNode {
                    name: "subdir1".to_string(),
                    digest: Some(digest(&subdir1.encode_to_vec())),
                },
                DirectoryNode {
                    name: "subdir2".to_string(),
                    digest: Some(digest(&subdir2.encode_to_vec())),
                },
            ],
            symlinks: vec![],
            node_properties: None,
        };

        Self {
            key: rand_string(&mut r),
            qualifiers: vec![],
            payload: vec![
                B64Debug(dir.encode_to_vec().into_boxed_slice()),
                B64Debug(subdir1.encode_to_vec().into_boxed_slice()),
                B64Debug(subdir2.encode_to_vec().into_boxed_slice()),
                B64Debug(file1.iter().copied().collect()),
                B64Debug(file2.iter().copied().collect()),
            ],
            type_,
        }
    }

    fn add_qualifier(&mut self, k: impl ToString, v: impl ToString) {
        self.qualifiers.push((k.to_string(), v.to_string()));
    }
}

fn rand_string(r: &mut impl Rng) -> String {
    r.sample_iter(Alphanumeric)
        .take(7)
        .map(char::from)
        .collect()
}

fn digest(buf: &[u8]) -> Digest {
    let mut dig = Sha256::new();
    dig.update(buf);
    let sha = format!("{:x}", dig.finalize());
    let flen = buf.len();
    Digest {
        hash: sha,
        size_bytes: flen.try_into().unwrap(),
    }
}

impl Asset {
    fn key(&self) -> Cow<'_, str> {
        (&self.key).into()
    }

    fn payloads(&self) -> impl Iterator<Item = &'_ [u8]> {
        self.payload.iter().map(|b| &b[..])
    }
}

#[derive(Debug)]
enum AssetType {
    Blob,
    Directory,
}

fn setup_log(cli: &Cli) {
    use tracing_subscriber::layer::SubscriberExt;
    use tracing_subscriber::util::SubscriberInitExt;
    use tracing_subscriber::EnvFilter;
    use tracing_subscriber::Layer;

    let filter = if let Some(level) = &cli.log_level {
        EnvFilter::default().add_directive(level.clone())
    } else {
        EnvFilter::from_default_env()
    };

    let formatter = if cli.json_logging {
        // TODO: use flatten_current_span when/if
        //       https://github.com/tokio-rs/tracing/pull/2705 lands
        tracing_subscriber::fmt::layer()
            .json()
            .flatten_event(true)
            .boxed()
    } else {
        tracing_subscriber::fmt::layer().boxed()
    };

    tracing_subscriber::registry()
        .with(filter)
        .with(formatter)
        .init();
}
