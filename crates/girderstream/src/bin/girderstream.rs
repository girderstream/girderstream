// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is the main girderstream cli entry point
//!
//! This is used to run girderstream project commands.
//! A project command is one that operates on the project
//! ie build something in the project. show the cache state
//! of a project or element.
//!
//! Other entry points may contain utility functions
//! ie. add a directory or tar to the cas that a project
//! may then consume as a source.
use std::{
    env, fs::create_dir_all, io::stdout, net::ToSocketAddrs, ops::Deref, path::Path, process::exit,
    sync::Arc, time::Duration,
};

use itertools::enumerate;
use reapi_tools::virtual_fs::VirtualDir;
use tokio::{fs, io::AsyncWriteExt, sync::broadcast};

use girderstream::{
    configure::{get_cli, Cli, Command, UserConfig},
    project::{self, element::ElementRef, project::Project},
    scheduler, show, show_list, Crapshoot,
};
use reapi_tools::{FullConfig, LocalConfig, ReapiConnection, RemotesConfig};
use yaba::server::serve;

async fn show_for_element(
    connection: &mut ReapiConnection,
    element_ref: &ElementRef,
    project: &Project,
    cli: &Cli,
) -> Crapshoot {
    let mut stdout = stdout();
    if cli.get_verbose() {
        show::<project::element::ElementDisplayLong>(&mut stdout, connection, project, element_ref)
            .await
    } else {
        show::<project::element::ElementDisplayStd>(&mut stdout, connection, project, element_ref)
            .await
    }
}

//#[tokio::main]
async fn main_async() -> Crapshoot {
    let cli = get_cli();

    let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
    let cancel = cancelation_issuer.clone();
    tokio::spawn(async move {
        tokio::signal::ctrl_c().await.unwrap();
        // Your handler here
        cancel.send(()).unwrap();
    });
    // todo: use cli to turn this on or off
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    let project_root = cli.get_root()?;
    let config_options = cli.get_options();

    let mut remote_configs = vec![];
    let remotes = RemotesConfig::open_from_file(&project_root.join(".remotes")).await;
    if let Ok(remotes) = remotes {
        for remote in remotes.get_remotes() {
            remote_configs.push(remote.clone().try_into()?);
        }
    } else {
        println!("Could not read remotes file")
    }

    let config_path = match env::var("XDG_CONFIG_HOME") {
        Ok(path) => Path::new(&path).to_owned(),
        Err(_) => Path::new(&env::var("HOME").unwrap()).join(".config"),
    };
    let config_path = config_path.join("girderstream");
    let user_config = UserConfig::open_from_file(&config_path).await;
    remote_configs.extend(
        user_config
            .get_remotes()
            .map(|remote| remote.clone().try_into().unwrap()),
    );

    let cas_endpoint: String = if let Some(local_loc) = cli.get_endpoint() {
        local_loc.to_owned()
    } else {
        user_config
            .get_local_cas()
            .unwrap_or(&"localhost:50040".to_string())
            .to_owned()
    };
    let cas_endpoint = cas_endpoint
        .strip_prefix("http://")
        .unwrap_or(&cas_endpoint)
        .to_string();

    let local_server = if let Some(cas_location) = cli.get_cas_location() {
        println!("cas_endpoint {cas_endpoint:?}");

        let server_addr: Vec<_> = cas_endpoint
            .to_socket_addrs()
            .expect("Unable to resolve domain")
            .collect();
        let server = serve(
            server_addr.first().unwrap().to_owned(),
            Some(cas_location.into()),
            true,
        );
        let server = tokio::spawn(server);
        Some(server)
    } else {
        None
    };

    let local_config = if let Some(local_loc) = cli.get_cas_location() {
        LocalConfig::new_yaba(
            cli.get_endpoint()
                .unwrap_or(
                    user_config
                        .get_local_cas()
                        .unwrap_or(&"http://localhost:50040".to_string()),
                )
                .to_owned(),
            "".to_string(),
            local_loc.into(),
        )
    } else {
        LocalConfig::new(
            cli.get_endpoint()
                .unwrap_or(
                    user_config
                        .get_local_cas()
                        .unwrap_or(&"http://localhost:50040".to_string()),
                )
                .to_owned(),
            "".to_string(),
            cli.get_local_exec()
                .unwrap_or(
                    user_config
                        .get_local_exec()
                        .unwrap_or(&"buildbox-run".to_string()),
                )
                .to_owned(),
        )
    };

    let project_config = FullConfig::new(local_config, remote_configs);
    let mut project = project::project::Project::new(
        &project_config,
        VirtualDir::from(project_root),
        Some(config_options),
        true,
        true,
        cancelation_issuer.clone(),
    )
    .await?;

    let mut result = Ok(());

    match cli.get_command() {
        Command::Build(element_name) => {
            let target_element = project.get_element(&element_name.get_name_arc()).await?;
            show_for_element(
                &mut project_config.get_local().to_owned().into(),
                &target_element,
                &project,
                &cli,
            )
            .await?;
            let locked_project = Arc::new(project);
            let force = if let Some(force) = element_name.force() {
                force
            } else {
                user_config.get_force().unwrap_or(false)
            };
            println!("Force: {}", force);

            result = scheduler::build(
                &project_config,
                &target_element,
                locked_project.clone(),
                force,
                cancelation_issuer,
            )
            .await;
            if result.is_ok() {
                println!("Build succeeded");
            } else {
                println!("Build failed");
            };
            let mut stdout = stdout();
            show_list::<project::element::ElementDisplayStd>(
                &mut stdout,
                &mut project_config.get_local().to_owned().into(),
                &locked_project,
                &target_element,
            )
            .await?;
        }
        Command::Shell(element_name) => {
            let element_to_shell = project.get_element(&element_name.get_name_arc()).await?;
            let mut connection = project_config.get_local().to_owned().into();
            let artifact = element_to_shell.get_build_artifact(&mut connection).await?;
            if artifact.successfully_build() {
                element_to_shell
                    .shell(&mut connection, &project, cancelation_issuer)
                    .await?;
            } else {
                element_to_shell
                    .build_shell(&mut connection, &project, cancelation_issuer)
                    .await?;
            }
        }
        Command::Show(element_name) => {
            let element_to_show = project.get_element(&element_name.get_name_arc()).await?;
            show_for_element(
                &mut project_config.get_local().to_owned().into(),
                &element_to_show,
                &project,
                &cli,
            )
            .await?;
        }
        Command::Checkout(checkout_details) => {
            let target_element = if let Ok(target_element) =
                project.get_element(&checkout_details.get_name_arc()).await
            {
                target_element
            } else {
                println!(
                    "Could not find element \"{}\" in the project",
                    checkout_details.get_name()
                );
                exit(110);
            };
            match target_element.get_build_success_from_cache().await {
                Ok(result) => {
                    if result {
                        println!(
                            "Element \"{}\" not already built",
                            checkout_details.get_name()
                        );
                        exit(112);
                    }
                }
                Err(err) => {
                    println!(
                        "Could not get element \"{}\" cache result: {err}",
                        checkout_details.get_name()
                    );
                    exit(112);
                }
            }
            if let Err(err) = target_element
                .checkout(
                    &mut project_config.get_local().to_owned().into(),
                    checkout_details.get_location(),
                    checkout_details.get_fix_links(),
                    checkout_details.checkout_run_deps(),
                )
                .await
            {
                println!(
                    "Could not checkout \"{}\" due to error: \"{err:?}\"",
                    checkout_details.get_name()
                );
                exit(111);
            };
        }
        Command::Push(element_name) => {
            let target_element = project.get_element(&element_name.get_name_arc()).await?;
            for remote in project_config.get_remote() {
                if remote.can_push() {
                    target_element
                        .push(
                            &mut project_config.get_local().to_owned().into(),
                            &mut remote.to_owned().into(),
                            &project,
                        )
                        .await
                        .expect("Could not push element to remote cache")
                }
            }
        }
        Command::ExportLogs(checkout_details) => {
            let mut connection = project_config.get_local().to_owned().into();
            let target_element = project
                .get_element(&Arc::from(checkout_details.get_name().deref()))
                .await?;
            let (elements, _, _) =
                scheduler::get_all_deps(&mut connection, &project, &target_element).await?;
            let root_location = checkout_details.get_location();
            create_dir_all(root_location)?;

            show_for_element(
                &mut project_config.get_local().to_owned().into(),
                &target_element,
                &project,
                &cli,
            )
            .await?;

            for element in elements {
                if let Ok(build_result) = element.get_build_artifact(&mut connection).await {
                    if let Ok(Some(stdout)) = build_result.stdout(&mut connection).await {
                        let mut file = fs::File::create(format!(
                            "{}/{}.stdout",
                            &root_location,
                            element.get_name(),
                        ))
                        .await?;
                        file.write_all(stdout.as_bytes()).await?;
                    };
                    if let Ok(Some(stdout)) = build_result.stdout_runner(&mut connection).await {
                        let mut file = fs::File::create(format!(
                            "{}/{}.stdout_runner",
                            &root_location,
                            element.get_name(),
                        ))
                        .await?;
                        file.write_all(stdout.as_bytes()).await?;
                    };

                    if let Ok(Some(stderr)) = build_result.stderr(&mut connection).await {
                        let mut file = fs::File::create(format!(
                            "{}/{}.stderr",
                            &root_location,
                            element.get_name(),
                        ))
                        .await?;
                        file.write_all(stderr.as_bytes()).await?;
                    };
                    if let Ok(Some(stderr)) = build_result.stderr_runner(&mut connection).await {
                        let mut file = fs::File::create(format!(
                            "{}/{}.stderr_runner",
                            &root_location,
                            element.get_name(),
                        ))
                        .await?;
                        file.write_all(stderr.as_bytes()).await?;
                    };
                };
                if let Ok(sources) = element.get_source_artifacts(&mut connection).await {
                    for (count, source) in enumerate(sources) {
                        if let Ok(Some(stdout)) = source.stdout(&mut connection).await {
                            let mut file = fs::File::create(format!(
                                "{}/{}.source_{}.stdout",
                                &root_location,
                                element.get_name(),
                                count,
                            ))
                            .await?;
                            file.write_all(stdout.as_bytes()).await?;
                        };
                        if let Ok(Some(stderr)) = source.stderr(&mut connection).await {
                            let mut file = fs::File::create(format!(
                                "{}/{}.source_{}.stderr",
                                &root_location,
                                element.get_name(),
                                count,
                            ))
                            .await?;
                            file.write_all(stderr.as_bytes()).await?;
                        };
                    }
                }
            }
        }
        Command::Track(element) => {
            let target_element = project.get_element(&element.get_name_arc()).await?;
            // elements with multiple sources might want some more options at this point..
            target_element
                .track(&project, &project_config, cancelation_issuer)
                .await?;
        }
    };
    drop(local_server);

    result
}

fn main() -> Crapshoot {
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    let result = runtime.block_on(async { main_async().await });

    runtime.shutdown_timeout(Duration::from_millis(500));
    println!("{result:#?}");
    result
}
