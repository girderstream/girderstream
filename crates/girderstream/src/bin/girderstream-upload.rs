use std::{
    net::ToSocketAddrs,
    path::{Path, PathBuf},
};

use clap::Parser;
use reapi_tools::{fs_io::upload_dir, LocalConfig};
use yaba::server::serve;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Cli {
    /// Specify the url of the local cas endpoint
    #[clap(long)]
    address: Option<String>,
    /// Specify the instance of the local cas
    #[clap(long)]
    location: Option<PathBuf>,
    upload_location: PathBuf,
}

impl Cli {
    fn get_cas_location(&self) -> Option<&Path> {
        self.location.as_deref()
    }
    fn get_endpoint(&self) -> Option<&str> {
        self.address.as_deref()
    }
}

#[tokio::main]
async fn main() {
    println!("Hello, world!");
    tracing_subscriber::fmt().init();
    println!("Serve, world!");
    let cli = Cli::parse();
    println!("cli: {cli:?}");

    let cas_endpoint: String = if let Some(local_loc) = cli.get_endpoint() {
        local_loc.to_owned()
    } else {
        "localhost:50040".to_string().to_owned()
    };
    let cas_endpoint = cas_endpoint
        .strip_prefix("http://")
        .unwrap_or(&cas_endpoint)
        .to_string();
    let local_server = if let Some(cas_location) = cli.get_cas_location() {
        let server_addr: Vec<_> = cas_endpoint
            .to_socket_addrs()
            .expect("Unable to resolve domain")
            .collect();
        let server = serve(
            server_addr.first().unwrap().to_owned(),
            Some(cas_location.into()),
            true,
        );
        let server = tokio::spawn(server);
        Some(server)
    } else {
        None
    };

    let local_config = if let Some(local_loc) = cli.get_cas_location() {
        LocalConfig::new_yaba(
            cli.get_endpoint()
                .unwrap_or("http://localhost:50040")
                .to_owned(),
            "".to_string(),
            local_loc.into(),
        )
    } else {
        LocalConfig::new(
            cli.get_endpoint()
                .unwrap_or("http://localhost:50040")
                .to_owned(),
            "".to_string(),
            "".to_string(),
        )
    };
    let mut connection = local_config.into();
    upload_dir(&mut connection, &cli.upload_location)
        .await
        .unwrap();
    drop(local_server)
}
