// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! The Girderstream integration tool
//!
//! Girderstream is nominally a cli program. However it is
//! also a lib with a public api to allow other programs to
//! load a girderstream project and then analyses or manipulate
//! it.
use std::{collections::HashMap, io::Write};

use colored::Colorize;

use crate::project::{
    element::{ElementRef, LifetimedDisplay},
    BuildResult,
};
use reapi_tools::ReapiConnection;

pub type Crapshoot = anyhow::Result<()>;

mod build;
mod cache;
pub mod configure;
pub mod project;
pub mod reapi;
pub mod scheduler;
mod yaml_misc;

/// Write the configuration of the project to the supplied buffer
///
/// Typically printed the stdout on start up of Girderstream so that if you
/// look at the logs you can be sure how Girderstream was configured and how to
/// reproduce the run in the future.
pub(crate) fn show_configuration(
    buffer: &mut dyn Write,
    configuration: HashMap<String, (String, Vec<String>)>,
) {
    writeln!(buffer, "\n{}:", "Project configuration".cyan()).unwrap();
    for (config_option_name, (selected_option, options)) in configuration {
        writeln!(
            buffer,
            "\t{}: {}",
            "Configuration Option".blue(),
            config_option_name.yellow()
        )
        .unwrap();
        writeln!(
            buffer,
            "\t\t{}: {}",
            "Configuration Selected".blue(),
            selected_option.yellow()
        )
        .unwrap();
        writeln!(buffer, "\t\t{}:", "From".blue()).unwrap();
        for option in options {
            writeln!(buffer, "\t\t- {}", option.yellow()).unwrap();
        }
    }
}

/// Write all teh information about the project and its elements to the given buffer
///
/// Typically printed the stdout on start up of Girderstream so that if you
/// look at the logs you can be sure how Girderstream was configured and how to
/// reproduce the run in the future.
///
/// And you can see how much work will need to be done to build a given element
/// as the current LifetimedDisplay implementations include if the element is
/// cached.
pub async fn show<'a, T: LifetimedDisplay<'a>>(
    buffer: &mut dyn Write,
    connection: &mut ReapiConnection,
    project: &'a project::project::Project,
    element: &ElementRef,
) -> Crapshoot {
    writeln!(buffer, "{}", "GirderStream Project InfoBlock".cyan()).unwrap();
    let full_config: HashMap<String, (String, Vec<String>)> = project.get_configuration();
    show_configuration(buffer, full_config);

    writeln!(buffer, "\n{}:", "Elements".cyan()).unwrap();

    show_list::<T>(buffer, connection, project, element).await
}

/// Write a given set of elements to the supplied buffer
///
/// This function uses the LifetimedDisplay trait from element.rs as a generic
/// type perimeter to allow the caller to specify what information about the
/// elements they wish to display
pub async fn show_list<'o, 'a, T: LifetimedDisplay<'a>>(
    buffer: &mut dyn Write,
    connection: &mut ReapiConnection,
    project: &'a project::project::Project,
    element: &ElementRef,
) -> Crapshoot {
    // Rust doesn't let you have multiple formatters for a single struct
    // So we are just wrapping the element in a wrapper. The wrapper only borrows
    // the element the wrappers lifetime.
    // if we did our own trait that did the new and relied on display then
    // we could make the main display all the elements loop quite nice.
    // but still have options for displaying

    let (mut list_of_elements, _, _) =
        scheduler::get_all_deps(connection, project, element).await?;

    list_of_elements.reverse();

    for element in list_of_elements {
        let cached = element.get_buildresult_from_cache()?;
        lister::<T>(buffer, element.clone(), project, cached);
    }

    Ok(())
}

/// Use a given LifetimedDisplay implementation to write one element to the buffer
fn lister<'a, T: LifetimedDisplay<'a>>(
    buffer: &mut dyn Write,
    element: project::element::ElementRef,
    project: &'a project::project::Project,
    cached: Option<BuildResult>,
) {
    writeln!(buffer, "{}", T::new(element, project, cached)).unwrap();
}

pub mod misc {
    use std::{fs, io, path::Path};
    // Some misc functions

    /// Recessively copy a dir
    ///
    /// credit: `https://stackoverflow.com/a/65192210`
    ///
    /// It would be better if tokio so some such had a function like this so we
    /// didnt need to maintain this.
    pub fn copy_dir_all(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
        fs::create_dir_all(&dst)?;
        for entry in fs::read_dir(src)? {
            let entry = entry?;
            let ty = entry.file_type()?;
            if ty.is_dir() {
                copy_dir_all(entry.path(), dst.as_ref().join(entry.file_name()))?;
            } else {
                match fs::copy(entry.path(), dst.as_ref().join(entry.file_name())) {
                    Ok(_) => {}
                    Err(e) => println!("Error {e:?} Attempting to continue"),
                };
            }
        }
        Ok(())
    }
}

#[cfg(feature = "test_infra")]
pub mod test_infra {
    use nix::{
        sys::signal::{kill, Signal::SIGKILL, Signal::SIGTERM},
        unistd::Pid,
    };
    use reapi::build::bazel::remote::asset::v1::fetch_client::FetchClient;
    use std::{
        path::Path,
        thread,
        time::{Duration, Instant},
    };
    use std::{
        process::Stdio,
        sync::atomic::{AtomicUsize, Ordering},
    };
    use tempfile::{tempdir, TempDir};
    use tokio::process::{Child, Command};

    use crate::Crapshoot;
    static CASD_COUNT: AtomicUsize = AtomicUsize::new(10444);

    pub struct CasdWrapper {
        casd_process: Child,
        tmp_dir: TempDir,
        port_num: usize,
    }

    impl CasdWrapper {
        pub fn start_casd() -> anyhow::Result<Self> {
            let new_port = CASD_COUNT.fetch_add(1, Ordering::SeqCst);
            println!("casd port: {}", new_port);

            let tmp_dir_test = tempdir()?;
            let tmp_dir_test_path = tmp_dir_test.path();
            let tmp_dir_cas_path = tmp_dir_test_path.join("cas_sub");
            let tmp_dir_str = format!("{}", tmp_dir_cas_path.display());
            let child_process = Command::new("buildbox-casd")
                .arg("--protect-session-blobs")
                .arg("--bind")
                .arg(format!("localhost:{}", new_port))
                .arg(tmp_dir_str)
                .stderr(Stdio::null())
                .stdout(Stdio::null())
                .stdin(Stdio::null())
                .kill_on_drop(true)
                .spawn()?;

            Ok(Self {
                casd_process: child_process,
                tmp_dir: tmp_dir_test,
                port_num: new_port,
            })
        }

        pub async fn wait_for_casd(&self) -> Crapshoot {
            let start = Instant::now();
            loop {
                match FetchClient::connect(self.get_address()).await {
                    Ok(_) => return Ok(()),
                    Err(_) => tokio::task::yield_now().await,
                }
                if start.elapsed().as_secs() > 10 {
                    anyhow::bail!("Timed out waiting for casd to come up")
                }
            }
        }

        pub fn get_temp_root(&self) -> &Path {
            self.tmp_dir.path()
        }

        pub fn get_port(&self) -> usize {
            self.port_num
        }

        pub fn get_address(&self) -> String {
            format!("http://localhost:{}", self.port_num)
        }

        pub fn get_cas_upload(&self) -> String {
            format!("--cas-server=http://localhost:{}", self.port_num)
        }

        pub async fn close_with_future(&mut self) -> Crapshoot {
            if self.casd_process.try_wait()?.is_some() {
                return Ok(());
            };
            let pid: u32 = self.casd_process.id().expect("Invalid PID");
            println!("pid {pid}");
            kill(Pid::from_raw(pid as i32), SIGTERM)?;
            let start = Instant::now();
            loop {
                if let Some(exit) = self.casd_process.try_wait().expect("Could not wait") {
                    println!("casd exited nicely {exit:?}");
                    return Ok(());
                }
                if start.elapsed().as_secs() > 200 {
                    break;
                }
                tokio::task::yield_now().await;
            }
            let pid: u32 = self.casd_process.id().expect("Invalid PID");
            println!("pid {pid}");
            kill(Pid::from_raw(pid as i32), SIGKILL).expect("Could not sig kill");
            self.casd_process.wait().await?;

            Ok(())
        }
    }

    impl Drop for CasdWrapper {
        fn drop(&mut self) {
            if self
                .casd_process
                .try_wait()
                .expect("Could not wait 1")
                .is_some()
            {
                return;
            }
            let pid: u32 = self.casd_process.id().expect("Invalid PID");
            println!("pid {pid}");

            kill(Pid::from_raw(pid as i32), SIGTERM).expect("Could not sig term");
            let start = Instant::now();
            loop {
                if let Some(exit) = self.casd_process.try_wait().expect("Could not wait") {
                    println!("casd exited nicely {exit:?}");
                    return;
                }
                if start.elapsed().as_secs() > 200 {
                    break;
                }
                thread::sleep(Duration::from_millis(10));
            }
            let pid: u32 = self.casd_process.id().expect("Invalid PID");
            println!("pid {pid}");
            kill(Pid::from_raw(pid as i32), SIGKILL).expect("Could not sig kill");
        }
    }
}
