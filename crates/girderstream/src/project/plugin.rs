// Copyright 2022-2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! The plugins code is were we work out what to build
//!
//! This code leverages the plugin as defined in the project
//! to work out what and how to build.
//!
//! Currently the main entry point is the build. Currently
//! we gather sources, assemble, configure and build in one
//! giant step. This works ok for now but these should be
//! separated out like bst does.
//!
//! Currently we have source caching which makes developing
//! build elements much better but for things like git were
//! you can reuse the .git info it is not efficient. And we
//! should do something more like bst but still using the
//! remote asset api.
// Todo: We should add a function to the plugins so that they are
// involved in calculating the elements cache key. This would
// give use a good place to bump cache keys if we change
// plugin behavior
use std::{collections::HashMap, ops::Deref, path::PathBuf, sync::Arc, vec};

use hex;
use itertools::Itertools;
use reapi::build::bazel::remote::execution::v2::{command::EnvironmentVariable, Digest, Directory};
use serde::{Deserialize, Serialize};
use sha2::{Digest as _, Sha256};
use tokio::sync::broadcast;
use yaba::execution::SandboxMountSettings;

use crate::{
    build::{run_build_plugin, string_from_digest},
    cache::{cache_fetch, fetch_asset_key, get_cached_build, get_cached_fetch},
    project::{
        element::{Dependence, DependenceStageType},
        RunState,
    },
    Crapshoot,
};
use reapi_tools::{
    actor::ReapiConnections,
    cas_fs::{
        check_directory, digest_from_directory, directory_from_digest, join_digests, offset_digest,
        place_file_in_dir,
    },
    grpc_io::{digest_from_string, fetch_blob, upload_blob},
    remote::{from_tree_to_list, pull_cached_dir_cas_actor},
    FullConfig, ReapiConnection,
};

use super::{
    element::{ElementRef, SourceDetails},
    project::Project,
    BuildResult, FetchResult,
};

/// A struct is used to pass variables into the sandbox to the plugin
///
/// This struct implements Serialize so that the plugin variables
/// can be serialized added to the cas and joined into the sandboxes
/// filesystem.
#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct PluginVariables {
    plugin_vars: HashMap<String, String>,
}

/// Get if cas directory is present or can be pulled
///
/// This will try to find a cas directory locally and if not
/// it will pull it from the first remote it finds that contains
/// the desired blobs.
async fn cas_import_present_or_pull(
    full_config: &FullConfig,
    connection: &mut ReapiConnection,
    source_map: &SourceDetails,
) -> anyhow::Result<(String, FetchResult)> {
    let digest = digest_from_string(
        source_map
            .get("digest")
            .expect("CAS source must have digest"),
    )?;
    // Check we have the blob
    // Todo: use "has blob" if its cheaper
    match check_directory(&mut full_config.get_local().clone().into(), digest.clone()).await {
        Ok(_) => {
            return Ok((
                "".to_string(),
                FetchResult {
                    success: RunState::Success,
                    ref_digests: from_tree_to_list(connection, &digest).await?,
                    fetch_output: Some(digest),
                    command_result: None,
                },
            ));
        }
        Err(_) => {
            for remote in full_config.get_remote() {
                println!("Trying to pull from remote {}", remote.get_endpoint());
                let mut remote_connection = remote.clone().into();
                if (fetch_blob(&mut remote_connection, &digest).await).is_ok() {
                    let (pool, actor) = ReapiConnections::new_worker_sender(
                        connection,
                        &mut remote_connection,
                        5,
                        None,
                    )
                    .await;
                    let pulled_cas = pull_cached_dir_cas_actor(actor, digest.to_owned()).await;
                    drop(pool);
                    match pulled_cas {
                        Ok(_) => match from_tree_to_list(connection, &digest).await {
                            Ok(ref_digests) => {
                                return Ok((
                                    "".to_string(),
                                    FetchResult {
                                        success: RunState::Success,
                                        ref_digests,
                                        fetch_output: Some(digest),
                                        command_result: None,
                                    },
                                ));
                            }
                            Err(e) => {
                                println!("Could not pull child blob cas import {e:?}")
                            }
                        },
                        Err(e) => {
                            println!("Could not pull cas import {e:?}")
                        }
                    }
                };
            }
        }
    };
    anyhow::bail!(
        "Could not find CAS blob {:?} in local or remote caches",
        digest
    )
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub(crate) enum Compound {
    None,
    Replace,
    Extend,
}

/// The struct serde uses to understand the format of project plugins
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputSourcePlugin {
    kind: String,
    cli: Option<String>,
    depends: Vec<String>,
    compound: Option<Compound>,
    track: Option<String>,
}

fn update_arguments(cli: &str, source_map: &SourceDetails, track: bool) -> String {
    let mut cli = cli.to_string();
    if cli.contains("%{plugin_vars}") {
        let mut cli_vars = "".to_string();
        for (arg, val) in source_map.iter() {
            if arg != &"kind".to_string() && arg != &"shell".to_string() {
                cli_vars = format!("{} --{}={}", cli_vars, arg, val)
            }
        }
        if track {
            cli_vars = format!("{} --track", cli_vars)
        };
        cli = cli.replace("%{plugin_vars}", &cli_vars);
    }
    cli
}

fn arguments(source_map: &HashMap<String, String>, cli: String) -> Vec<String> {
    vec![
        source_map
            .get("shell")
            .unwrap_or(&"/bin/sh".to_string())
            .to_owned(),
        "-c".to_string(),
        "-e".to_string(),
        cli,
    ]
}

/// The Source Plugin used to fetch sources for elements
///
/// This struct holds the information needed to fetch specific
/// types of sources and the functions to fetch the sources are
/// provided by this struct. The functions combine the plugin
/// types information with the elements specific details.
#[derive(Clone, Debug)]
pub(crate) struct SourcePlugin {
    kind: String,
    dependencies: Vec<String>,
    cli: String,
    track: Option<String>,
    compound: Compound,
}

impl From<InputSourcePlugin> for SourcePlugin {
    /// Create a `SourcePlugin` from a `SerdeProjectPlugin`
    ///
    /// The `SerdeProjectPlugin` defines the format of sources added
    /// in the project.conf and the `SourcePlugin` then holds the
    /// information in a format more easily used during the build.
    /// And also has a number of helpful functions during the build.
    fn from(raw: InputSourcePlugin) -> Self {
        SourcePlugin {
            cli: raw.cli.unwrap_or(format!("{} %{{plugin_vars}}", raw.kind)),
            kind: raw.kind,
            dependencies: raw.depends,
            compound: raw.compound.unwrap_or(Compound::None),
            track: raw.track,
        }
    }
}

impl SourcePlugin {
    /// Create a SourcePlugin for unit testing
    ///
    /// This function is only compiled when unit testing and allow
    /// unit tests to quickly create BuildPlugins so that they
    /// can test how other code paths work without having to have
    /// a full integration tests worth of over head.
    #[cfg(test)]
    pub(crate) fn for_test(kind: String, dependencies: Vec<String>) -> Self {
        SourcePlugin {
            kind,
            dependencies,
            cli: "".to_string(),
            compound: Compound::None,
            track: None,
        }
    }

    /// Assemble the sandbox directory for this plugin
    // Note that this (clear from the function api) is the same
    // for every use of this plugin for a given project, ie
    // every use of the git plugin uses the same root.
    // There for this seems like something that could be fairly easily
    // cached to save a tiny bit of effort for every fetch.
    async fn get_root_directory(
        &self,
        connection: &mut ReapiConnection,
        project: &Project,
    ) -> anyhow::Result<Digest> {
        let root_directory = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let mut root_digest = digest_from_directory(connection, root_directory).await?;
        for dep in &self.dependencies {
            let source_dep_elem = project
                .get_existing_element(&Arc::from(dep.as_str()))
                .unwrap_or_else(|e| {
                    panic!("Could not get existing element {}! Panic {:?}", dep, e)
                });
            for sub_dep in source_dep_elem.get_all_run_dep_elements() {
                let source_dep_di = get_cached_build(connection, &sub_dep)
                    .await
                    .unwrap_or_else(|e| {
                        panic!(
                            "Could not get cache of build element {}! Panic {:?}",
                            dep, e
                        )
                    })
                    .build_output
                    .unwrap();
                root_digest = join_digests(connection, &root_digest, &source_dep_di).await?;
            }
        }
        Ok(root_digest)
    }

    pub fn get_dependencies(&self) -> &Vec<String> {
        &self.dependencies
    }

    /// Get the source plugins type
    pub fn get_kind(&self) -> &String {
        &self.kind
    }

    /// Get if this source plugin alters the source before it
    pub fn get_compound(&self) -> &Compound {
        &self.compound
    }

    /// Create a new source plugin for loading bootstrap material in from a cache
    pub fn new_cas() -> SourcePlugin {
        SourcePlugin {
            kind: "cas".to_string(),
            cli: "".to_string(),
            dependencies: vec![],
            compound: Compound::None,
            track: None,
        }
    }

    /// Get bytes that can be used to uniquely identify the source
    ///
    /// This must be combined with the bytes of the dependencies to get a
    /// completely unique identifiers.
    pub fn get_unique_bytes(&self, source_map: &SourceDetails) -> String {
        // This function is not enough to uniquely identify a source's output
        // this is combined with its dependencies when calculating a elements
        // full cache key or the final sandbox root when looking for cached
        // fetch operations.
        let mut dig = Sha256::new();
        dig.update(self.cli.as_bytes());
        dig.update(source_map.len().to_be_bytes());
        for key in source_map.deref().keys().sorted() {
            let val = source_map.get(key).unwrap();
            dig.update(key.len().to_be_bytes());
            dig.update(key.as_bytes());
            dig.update(val.len().to_be_bytes());
            dig.update(val.as_bytes());
        }
        match self.compound {
            Compound::Extend => dig.update([1]),
            Compound::None => dig.update([0]),
            Compound::Replace => dig.update([2]),
        };
        hex::encode(dig.finalize())
    }

    pub(crate) async fn track(
        &self,
        project: &Project,
        full_config: &FullConfig,
        source_map: &SourceDetails,
        cancelation: broadcast::Sender<()>,
    ) -> anyhow::Result<HashMap<String, String>> {
        let mut connection = full_config.get_local().clone().into();
        match self.get_kind().as_str() {
            "cas" => anyhow::bail!("cant track a cas source"),
            _kind => {
                let root_digest = self.get_root_directory(&mut connection, project).await?;

                let arguments = if let Some(track) = &self.track {
                    let track = update_arguments(track, source_map, true);
                    arguments(source_map, track)
                } else {
                    let cli = update_arguments(&self.cli, source_map, true);
                    arguments(source_map, cli)
                };

                let environment_variables = vec![
                            EnvironmentVariable{ name: "PATH".to_string(), value:
                            source_map
                                    .get("PATH")
                                    .unwrap_or(&"/plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin".to_string() ).to_string()
                        }
                            ];

                let sandbox_settings = SandboxMountSettings {
                    arguments,
                    root_digest,
                    with_network: true,
                    environment_variables,
                    interactive: false,
                    cwd: None,
                };
                let buildbox_result =
                    run_build_plugin(&mut connection, [].iter(), sandbox_settings, cancelation)
                        .await
                        .map_err(|e| {
                            println!("Could not run source plugin: {e:?}");
                            e
                        })?;

                if buildbox_result.success() {
                    let track_output = buildbox_result
                        .get_result_digest()
                        .cloned()
                        .ok_or_else(|| anyhow::anyhow!("No result digest"))?;
                    let track_dir = directory_from_digest(&mut connection, &track_output).await?;
                    for file in track_dir.files {
                        if file.name == "tracked.yml" {
                            let track_file =
                                fetch_blob(&mut connection, &file.digest.unwrap()).await?;
                            let tracked: HashMap<String, String> =
                                serde_yaml::from_str(&String::from_utf8_lossy(&track_file))
                                    .map_err(|err| {
                                        anyhow::anyhow!(
                                            "could not process track file because: {}",
                                            err
                                        )
                                    })?;

                            return Ok(tracked);
                        }
                    }
                    anyhow::bail!("Track command did not create track file");
                } else {
                    println!("{:?}", buildbox_result);
                    let err = string_from_digest(
                        &mut connection,
                        buildbox_result
                            .action
                            .as_ref()
                            .unwrap()
                            .stderr_digest
                            .as_ref()
                            .unwrap(),
                    )
                    .await?;
                    println!("Track stderr: {}", err);
                    let out = string_from_digest(
                        &mut connection,
                        buildbox_result
                            .action
                            .as_ref()
                            .unwrap()
                            .stdout_digest
                            .as_ref()
                            .unwrap(),
                    )
                    .await?;
                    println!("Track stdout: {}", out);

                    anyhow::bail!("Failed to run track command");
                }
            }
        }
    }

    /// Run a source plugin to get the source to build with
    ///
    /// This function has access to the cas and typically runs
    /// buildbox-run as a async subprocess and returns the
    /// digest of the resulting source.
    ///
    /// Girderstream aims to run as much of the plugin code in
    /// the sandbox as posable including the source plugins. But
    /// we must provide a bootstrap method to get our first
    /// build plugins into the sandbox so we do provide a
    /// boot strap method.
    ///
    /// We could add more bootstrap methods but we want to avoid
    /// too many because the plugin effects the build so keeping
    /// plugins in the sandbox means that chaining the plugin
    /// only effects the elements for which you use the newer
    /// plugin. but changing the bootstrap plugins will effect
    /// all the elements cache keys. So minimizing the oppatutnity
    /// for the built ins means minimizing the change of having
    /// to alter all the cache keys.
    /// Also keeping the built ins as simple as posable also
    /// minimizes your chance of having to change them later
    /// and thus altering the cache keys.
    pub(crate) async fn get_source_digest(
        &self,
        project: &Project,
        full_config: &FullConfig,
        source_map: &SourceDetails,
        base: Option<&Digest>,
        cancelation: broadcast::Sender<()>,
    ) -> anyhow::Result<(String, FetchResult)> {
        let mut connection = full_config.get_local().clone().into();
        match self.get_kind().as_str() {
            "cas" => {
                let mut cancelation = cancelation.subscribe();
                let pull_future =
                    cas_import_present_or_pull(full_config, &mut connection, source_map);
                tokio::select! {
                    pulled = pull_future => {
                        Ok(pulled)
                    }
                    canceled = cancelation.recv() => {
                        Err(anyhow::anyhow!("Canceled while fetching from cas {canceled:?}"))
                    }
                }?
            }
            kind => {
                println!("Running source plugin {}", kind);

                let root_digest = self.get_root_directory(&mut connection, project).await?;
                let root_digest = if let Some(base_digest) = base {
                    if self.compound != Compound::None {
                        join_digests(&mut connection, &root_digest, base_digest).await?
                    } else {
                        root_digest
                    }
                } else {
                    root_digest
                };
                let cli = update_arguments(&self.cli, source_map, false);
                let fetch_key = fetch_asset_key(&root_digest, self, source_map);

                match get_cached_fetch(&mut connection, fetch_key.clone()).await {
                    Ok(cached_fetch) => {
                        if cached_fetch.successfully_fetch() {
                            println!("Previously fetched source found");
                            return Ok((fetch_key, cached_fetch));
                        } else {
                            println!("Source previously fialed: {cached_fetch:?}");
                        }
                    }
                    Err(err) => {
                        if err.to_string().contains("blob not found") {
                            println!("Previous fetch not found: {err:?}");
                        } else {
                            println!("Could not fetch cached source: {err:?}");
                        }
                    }
                }
                println!("Starting fetch");
                // Todo: this should only be run if the fetch was not found, all other errors should be dealt
                // with more aproratately, eg if get_cached_fetch failed with a cas issue then we should return with that
                // err.
                // But while we make such extensive use of anyhow that is less trivial.

                let arguments = arguments(source_map, cli);

                let environment_variables = vec![
                            EnvironmentVariable{ name: "PATH".to_string(), value:
                            source_map
                                    .get("PATH")
                                    .unwrap_or(&"/plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin".to_string() ).to_string()
                        }
                            ];
                let sandbox_settings = SandboxMountSettings {
                    arguments,
                    root_digest,
                    environment_variables,
                    with_network: true,
                    interactive: false,
                    cwd: None,
                };
                let buildbox_result =
                    run_build_plugin(&mut connection, [].iter(), sandbox_settings, cancelation)
                        .await
                        .map_err(|e| {
                            println!("Could not run source plugin: {e:?}");
                            e
                        })?;

                println!("Finished : {buildbox_result:?}");

                let fetch_result = if buildbox_result.success() {
                    let digest = buildbox_result.get_result_digest().cloned();
                    (
                        fetch_key,
                        FetchResult {
                            success: RunState::Success,
                            ref_digests: if let Some(ref digest) = digest {
                                from_tree_to_list(&mut connection, digest).await?
                            } else {
                                vec![]
                            },
                            fetch_output: digest,
                            command_result: Some(buildbox_result),
                        },
                    )
                } else {
                    println!("Fetch failed");
                    if let Some(action) = &buildbox_result.action {
                        let (stdout, stderr) = action.get_std_out_err(&mut connection).await?;
                        if let Some(stdout) = stdout {
                            println!("fetch stdout: {stdout}")
                        }
                        if let Some(stderr) = stderr {
                            println!("fetch stderr: {stderr}")
                        }
                    }
                    let digest = buildbox_result.get_result_digest().cloned();
                    (
                        fetch_key,
                        FetchResult {
                            success: RunState::Failure,
                            ref_digests: if let Some(ref digest) = digest {
                                from_tree_to_list(&mut connection, digest).await?
                            } else {
                                vec![]
                            },
                            fetch_output: digest,
                            command_result: Some(buildbox_result),
                        },
                    )
                };
                cache_fetch(&mut connection, &fetch_result.0, &fetch_result.1).await?;
                Ok(fetch_result)
            }
        }
    }
}

// The largest one of these is a bout 300 bytes which considering how little this
// is used is not worth worrying about.
#[allow(clippy::large_enum_variant)]
enum DigestOrFailedBuild {
    Digest((Digest, Vec<String>)),
    FailedBuild(BuildResult),
}

/// Gather the elements dependencies into a single directory and return its digest
async fn root_digest_of_dependents(
    connection: &mut ReapiConnection,
    root_digest: Digest,
    project: &Project,
    element: &ElementRef,
) -> anyhow::Result<Digest> {
    let mut all_build_deps: Vec<Dependence> = vec![];

    // We create a list of all the dependencies needed for building
    for dep in element.get_all_deps().filter(|dep| dep.is_buildtime()) {
        // We now go through the build elements and add there required sub dependencies based on the stage scope

        // When we do resolve vs build steps #6  then we can use the two separately
        if !(dep.is_from_element() || dep.is_build_plugin()) {
            continue;
        }
        match dep.get_stage_scope() {
            DependenceStageType::Build => {
                let mut new_deps = dep.get_build_deps()?;
                for sub_dep in &mut new_deps {
                    sub_dep.set_location(dep.get_location().to_owned());
                }
                all_build_deps.extend(new_deps);
                all_build_deps.push(dep.to_owned());
            }
            DependenceStageType::NoChildren => all_build_deps.push(dep.to_owned()),
            DependenceStageType::Run => {
                let mut new_deps = dep.get_run_deps()?;
                for sub_dep in &mut new_deps {
                    sub_dep.set_location(dep.get_location().to_owned());
                }
                all_build_deps.extend(new_deps);
                all_build_deps.push(dep.to_owned());
            }
            DependenceStageType::RunAndBuild => {
                let mut new_deps: Vec<Dependence> =
                    dep.get_all_deps().map(|dep| dep.to_owned()).collect();
                for sub_dep in &mut new_deps {
                    sub_dep.set_location(dep.get_location().to_owned());
                }
                all_build_deps.extend(new_deps);
                all_build_deps.push(dep.to_owned());
            }
        };
    }

    // Now that we have a list of all the required deps we add them to the root directory
    let root_digest =
        digest_of_dependencies(connection, all_build_deps.iter(), root_digest, project).await?;
    Ok(root_digest)
}

/// Combine a iter of "Dependence" into a single Digest containing them all
pub(crate) async fn digest_of_dependencies(
    connection: &mut ReapiConnection,
    dependencies: impl Iterator<Item = &Dependence>,
    root_digest: Digest,
    project: &Project,
) -> anyhow::Result<Digest> {
    let mut root_digest = root_digest;

    // Loop through these dependencies and add create a joined directory of them all
    let mut added_deps = vec![];
    for dep in dependencies {
        let elem = project.get_existing_element(dep.get_name())?;
        let elem_result = get_cached_build(connection, &elem).await?;
        if !elem_result.successfully_build() {
            anyhow::bail!("Trying to use {} but it is not yet built", dep.get_name())
        }
        let join_digest = offset_digest(
            connection,
            elem_result.build_output.unwrap(),
            PathBuf::from(dep.get_location()),
        )
        .await
        .map_err(|e| anyhow::format_err!("Could not offset {dep:?}, caused {e:?}"))?;
        root_digest = join_digests(connection, &root_digest, &join_digest)
            .await
            .map_err(|e| {
                anyhow::format_err!(
                    "Could not add {:?}, caused {e:?}, to {added_deps:?}",
                    dep.get_name()
                )
            })?;
        added_deps.push(dep.get_name().to_owned());
    }

    Ok(root_digest)
}

/// The struct serde uses to understand the format of project plugins
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputBuildPlugin {
    kind: String,
    cli: Option<String>,
    depends: Vec<String>,
}

/// The build plugin used to build elements
///
/// This struct holds the information needed to build a specific
/// `kind` of build and the functions to build the element
/// are provided by this struct. The functions combine the plugin
/// types information with the elements specific details and the
/// relivent project context.
#[derive(Clone, Debug)]
pub(crate) struct BuildPlugin {
    kind: String,
    dependencies: Vec<String>,
    cli: String,
}

impl From<InputBuildPlugin> for BuildPlugin {
    /// Create a `BuildPlugin` from a `SerdeProjectPlugin`
    ///
    /// The `SerdeProjectPlugin` defines the format of plugins added
    /// in the project.conf and the `BuildPlugin` then holds the
    /// information in a format more easily used during the build.
    /// And also has a number of helpful functions during the build.
    fn from(raw: InputBuildPlugin) -> Self {
        BuildPlugin {
            cli: raw.cli.unwrap_or(format!("{} %{{plugin_vars}}", raw.kind)),
            kind: raw.kind,
            dependencies: raw.depends,
        }
    }
}

impl BuildPlugin {
    /// Create a BuildPlugin for unit testing
    ///
    /// This function is only compiled when unit testing and allow
    /// unit tests to quickly create BuildPlugins so that they
    /// can test how other code paths work without having to have
    /// a full integration tests worth of over head.
    #[cfg(test)]
    pub(crate) fn for_test() -> Self {
        BuildPlugin {
            kind: "bootstrap_import".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        }
    }

    /// Get the dependencies that the plugin needs for its self.
    ///
    /// These are the dependencies that the every use of the plugin
    /// needs rather than the things the specific element depends on.
    pub fn get_dependencies(&self) -> &Vec<String> {
        &self.dependencies
    }

    /// Get the kind of the plugin
    pub fn get_kind(&self) -> &String {
        &self.kind
    }

    /// Get the cli value provided by this plugin
    ///
    /// This is used by the element to gether with the elements plugin variables
    /// to decide what to run in the build sandbox to build the element.
    ///
    /// Note that this is effectively the default cli to call unless the element
    /// over rides it so don't rely on this to be what is called by the element.
    pub(crate) fn get_cli(&self) -> &String {
        &self.cli
    }

    /// Create a new junction type plugin
    pub fn new_junction() -> BuildPlugin {
        BuildPlugin {
            kind: "junction".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        }
    }

    /// Create a new bootstrap import type plugin
    pub fn new_bootstrap_import() -> BuildPlugin {
        BuildPlugin {
            kind: "bootstrap_import".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        }
    }

    /// Go through the elements sources and create a CAS directory containing them
    ///
    /// This function returns ether a failed build in the case of failure or a tuples
    /// containing the digest of the directory containing the assembled sources and
    /// the cache keys of each source.
    async fn get_source_digest(
        &self,
        full_config: &FullConfig,
        connection: &mut ReapiConnection,
        element: &ElementRef,
        project: &Project,
        cancelation: broadcast::Sender<()>,
    ) -> anyhow::Result<DigestOrFailedBuild> {
        let root_directory = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let mut root_digest = digest_from_directory(connection, root_directory).await?;
        let mut source_keys = vec![];
        let mut last_source = None;
        let mut last_root = root_digest.clone();
        for source in &element.get_sources() {
            let (source_digest, replace) = match project
                .get_source_digest(
                    full_config,
                    source,
                    last_source.as_ref(),
                    cancelation.clone(),
                )
                .await
            {
                Ok((source_key, fetch_result, replace)) => {
                    source_keys.push(source_key);
                    if !fetch_result.successfully_fetch() {
                        let failure = if let Some(err) = fetch_result.get_failure_info() {
                            RunState::FailureInternal(format!(
                                "Failed while running the source plugin: {err}"
                            ))
                        } else {
                            RunState::FailureInternal(
                                "Failed while running the source plugin".to_string(),
                            )
                        };
                        return Ok(DigestOrFailedBuild::FailedBuild(BuildResult {
                            build_output: None,
                            ref_digests: vec![],
                            success: failure,
                            command_result: None,
                            source_keys,
                        }));
                    }

                    match fetch_result.get_result_digest() {
                        Some(source_digest) => (source_digest.clone(), replace),
                        None => {
                            return Ok(DigestOrFailedBuild::FailedBuild(BuildResult {
                                build_output: None,
                                ref_digests: vec![],
                                success: RunState::FailureInternal(
                                    "Get_source_digest was empty".to_string(),
                                ),
                                command_result: None,
                                source_keys,
                            }));
                        }
                    }
                }

                Err(err) => {
                    let source_name = source.kind();
                    return Ok(DigestOrFailedBuild::FailedBuild(BuildResult {
                        build_output: None,
                        ref_digests: vec![],
                        success: RunState::FailureInternal(format!(
                            "Failed to build due to source {source_name} error: {err}"
                        )),
                        command_result: None,
                        source_keys,
                    }));
                }
            };
            let off_set = offset_digest(connection, source_digest, "/sources".into()).await?;
            last_source = Some(off_set.clone());
            if replace {
                root_digest = join_digests(connection, &last_root, &off_set).await?;
            } else {
                last_root = root_digest.clone();
                root_digest = join_digests(connection, &root_digest, &off_set).await?;
            }
        }
        Ok(DigestOrFailedBuild::Digest((root_digest, source_keys)))
    }

    /// Build the element from its sources.
    ///
    /// Currently this includes the source fetching and building.
    ///
    /// This function uses async so as not to block but it should be
    /// noted that it make take a long time for the future to complete.
    // Please note that I wish to radically over hall this code!
    //
    // I would like to have a configure step that fires up the sandbox
    // with just the plugin required deps. Run the plugin with the
    // "plugin_variables_string" using the cli options. and some info
    // about the other dependencies supplied, ie were to put them.
    //
    // This function would then take that info and assemble the sandbox
    // as appropriate for the build proper. this could be multiple steps
    // ie build, test, package...
    //
    // This would let the plugin have much more flexibility, more akin
    // to bst plugins but still not giving them quite as much info.
    //
    // Anther reason is that changing project level vars will change the
    // cache key of all elements atm. so letting the plugin do the cheap
    // configure step, creating a second key based only on what the
    // config set did that can filter out project plugin var changes that do
    // not effect this build can significantly reduce the amount of rebuild.
    //
    // I would also really like to have test steps that are like optional
    // extras that are not needed for the next element to start building
    // but must be build eventually to be possible. Im not sure how todo that
    // so i would like to work that out before re working this function too
    // much.
    //
    // For now this functions works well enough tho and I am actively improving
    // it and this work will help the eventual vision but pleas bare the above
    // in mind if you propose changes to this function.
    //
    // I am very interested in chatting about what this function can be but this
    // is key code to realizing some key parts of my vision for girderstream
    // that I will be fairly passionate about it lol
    //
    // Will
    pub(crate) async fn build(
        &self,
        project: &Project,
        full_config: &FullConfig,
        element: &ElementRef,
        cancelation: broadcast::Sender<()>,
    ) -> anyhow::Result<BuildResult> {
        let mut connection = full_config.get_local().clone().into();
        println!("starting build of {}", element.get_name());

        let (root_digest, source_keys) = match self
            .get_source_digest(
                full_config,
                &mut connection,
                element,
                project,
                cancelation.clone(),
            )
            .await?
        {
            DigestOrFailedBuild::Digest((root_digest, keys)) => (root_digest, keys),
            DigestOrFailedBuild::FailedBuild(failed_build) => return Ok(failed_build),
        };

        if self.get_kind() == "bootstrap_import" {
            #[cfg(test)] // just unit tests
            {
                if element.get_sources().is_empty() {
                    return Ok(BuildResult {
                        ref_digests: from_tree_to_list(&mut connection, &root_digest).await?,
                        build_output: Some(root_digest),
                        command_result: None,
                        success: RunState::Success,
                        source_keys,
                    });
                }
            }
            let root_directory = directory_from_digest(&mut connection, &root_digest).await?;

            let source_digest = root_directory
                .directories
                .get(
                    root_directory
                        .directories
                        .iter()
                        .position(|dir_node| dir_node.name == "sources")
                        .unwrap(),
                )
                .unwrap()
                .digest
                .as_ref()
                .unwrap()
                .clone();
            return Ok(BuildResult {
                ref_digests: from_tree_to_list(&mut connection, &root_digest).await?,
                build_output: Some(source_digest),
                success: RunState::Success,
                command_result: None,
                source_keys,
            });
        };

        if self.get_kind() == "junction" {
            let root_directory = directory_from_digest(&mut connection, &root_digest).await?;

            let source_digest = root_directory
                .directories
                .get(
                    root_directory
                        .directories
                        .iter()
                        .position(|dir_node| dir_node.name == "sources")
                        .unwrap(),
                )
                .unwrap()
                .digest
                .as_ref()
                .unwrap()
                .clone();
            return Ok(BuildResult {
                ref_digests: from_tree_to_list(&mut connection, &root_digest).await?,
                build_output: Some(source_digest),
                success: RunState::Success,
                command_result: None,
                source_keys,
            });
        }

        let mut root_digest =
            root_digest_of_dependents(&mut connection, root_digest, project, element).await?;

        #[cfg(test)]
        // This lets us test the code for prepping `root_digest` without actually running the sandbox
        {
            if element.get_plugin_variables().get("cli").unwrap() == &"skip".to_string() {
                return Ok(BuildResult {
                    ref_digests: from_tree_to_list(&mut connection, &root_digest).await?,
                    build_output: Some(root_digest),
                    success: RunState::Success,
                    command_result: None,
                    source_keys,
                });
            }
        }

        let cli = element.get_cli(project).to_owned();

        let plugin_variables_hash = PluginVariables {
            plugin_vars: element.get_plugin_variables().deref().clone(),
        };

        let plugin_variables_string = serde_yaml::to_string(&plugin_variables_hash)?;
        let file_digest =
            upload_blob(&mut connection, plugin_variables_string.as_bytes().to_vec()).await?;

        let pluin_vars_digest = place_file_in_dir(
            &mut connection,
            "plugin_vars.yaml".to_string(),
            file_digest,
            false,
        )
        .await?;
        root_digest = join_digests(&mut connection, &root_digest, &pluin_vars_digest).await?;

        let arguments = arguments(&element.get_plugin_variables(), cli);

        let environment_variables = self.environment_variables(element);

        let remote_executers = full_config
            .get_remote()
            .iter()
            .filter(|remote| remote.get_execution().is_some());
        let sandbox_settings = SandboxMountSettings {
            arguments,
            root_digest,
            environment_variables,
            with_network: false,
            interactive: false,
            cwd: None,
        };
        let buildbox_result = run_build_plugin(
            &mut connection,
            remote_executers,
            sandbox_settings,
            cancelation,
        )
        .await?;

        let ref_digests = if let Some(root_digest) = buildbox_result.get_result_digest() {
            from_tree_to_list(&mut connection, root_digest).await?
        } else {
            vec![]
        };
        Ok(BuildResult {
            ref_digests,
            success: RunState::from(buildbox_result.success()),
            build_output: buildbox_result.get_result_digest().cloned(),
            command_result: Some(buildbox_result),
            source_keys,
        })
    }

    /// Start an interactive failed build shell
    ///
    /// These can be used to debug a failed build, by interactively running
    /// commands in the sandbox.
    pub(crate) async fn failed_build_shell(
        &self,
        connection: &mut ReapiConnection,
        element: &ElementRef,
        _project: &Project,
        cancelation: broadcast::Sender<()>,
    ) -> Crapshoot {
        let build_result = get_cached_build(connection, element).await?;
        let command_result = build_result.get_command_result().expect("Missing result");
        let root = command_result
            .action
            .as_ref()
            .unwrap()
            .output_directories
            .get(".")
            .expect("no root");

        let environment_variables = self.environment_variables(element);

        println!("Debugging shell:");
        let sandbox_settings = SandboxMountSettings {
            arguments: vec!["/bin/sh".to_string()],
            root_digest: root.clone(),
            environment_variables,
            with_network: false,
            interactive: true,
            cwd: None,
        };
        let _ = run_build_plugin(connection, [].iter(), sandbox_settings, cancelation).await;
        // we ignore the result as this always fails due to the interactive setup atm
        Ok(())
    }

    /// Get the final environment variables for this build
    pub(crate) fn environment_variables(&self, element: &ElementRef) -> Vec<EnvironmentVariable> {
        let mut environment_variables: Vec<EnvironmentVariable> = element
            .get_environment_variables()
            .iter()
            .map(|(k, v)| EnvironmentVariable {
                name: k.to_owned(),
                value: v.to_owned(),
            })
            .collect();

        if !element.get_environment_variables().contains_key("PATH") {
            environment_variables.push(
                EnvironmentVariable{
                    name: "PATH".to_string(), value: "/plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin".to_string()
                }
            )
        };
        environment_variables
    }
}

#[cfg(test)]
mod tests {
    use serde_either::StringOrStruct;
    use std::{collections::HashMap, path::PathBuf, sync::Arc};
    use tokio::sync::broadcast;

    use crate::{
        cache::cache_build,
        project::{
            element::{BuildType, InputDependency, InputElement, JoinType, StageType, Variables},
            plugin::{BuildPlugin, Compound, SourcePlugin},
            project::Project,
        },
        Crapshoot,
    };
    use reapi_tools::{
        cas_fs::{directory_from_digest, offset_digest, place_file_in_dir},
        grpc_io::{digest_to_string, upload_blob},
        test_infra::YabaTestSetup,
        FullConfig, LocalConfig,
    };

    // This tests that run time deps are loaded in for a dependency when the
    // staging options require it.
    //
    // This is a bit like genimage elements that load them selves and there
    // run time deps into a sub folder to act like a fs that genimage will
    // use to construct the root dir for a target system
    #[tokio::test]
    async fn test_stage_deps() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_default(address, "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut conn = full_config.get_local().to_owned().into();

        // Create some files to represent what our depended on elements would have built
        let upload_content = "Some example contents\n".as_bytes().to_owned();
        let upload_digest = upload_blob(&mut conn, upload_content).await?;
        let upload_dir_a =
            place_file_in_dir(&mut conn, "test_build".to_string(), upload_digest, false).await?;
        let upload_dir =
            offset_digest(&mut conn, upload_dir_a, PathBuf::from("example_folder")).await?;

        let mut project = Project::for_test();

        let mut source = HashMap::new();
        source.insert("kind".to_string(), "cas".to_string());
        source.insert("digest".to_string(), digest_to_string(&upload_dir));
        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![source],
            None,
            None,
            None,
        );

        project.add_serde_test_element(element_name.clone(), input_element);
        let element_base_ref = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        let builder_import = BuildPlugin {
            kind: "bootstrap_import".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        };
        let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_base_ref,
                cancelation_issuer.clone(),
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.first().unwrap().name, "example_folder");
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_base_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // At this point we have loaded our base elements result into the local cache so later elements can build
        // on top of it.

        // Fully built base element

        // This intermediate element just needs building, it is a dummy to see that layer 2 deps are pulled through this direct dep
        let element_inner_name = "Intermediate".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "BaseElement".to_string(),
                None,
                Some("/".to_string()),
                None,
                None,
            ))]),
            None,
            None,
        );

        project.add_serde_test_element(element_inner_name.clone(), input_element);
        let element_inter_ref = project
            .get_element(&Arc::from(element_inner_name.as_str()))
            .await
            .unwrap();

        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_inter_ref,
                cancelation_issuer.clone(),
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.len(), 0);
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_inter_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // Finished building intermediate element

        // This element is representative of a genimage element creating a full file system

        let mut raw_vars = HashMap::new();
        raw_vars.insert("cli".to_string(), "skip".to_string());

        let element_top_name = "Top".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "Intermediate".to_string(),
                None,
                Some("/rootfs".to_string()),
                None,
                None,
            ))]),
            None,
            Some(Variables::from_dic(JoinType::Join, raw_vars)),
        );

        project.add_serde_test_element(element_top_name.clone(), input_element);
        let element_top_ref = project
            .get_element(&Arc::from(element_top_name.as_str()))
            .await
            .unwrap();

        let builder_build = BuildPlugin {
            kind: "mock_builder".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        };
        let built = builder_build
            .build(
                &project,
                &full_config,
                &element_top_ref,
                cancelation_issuer.clone(),
            )
            .await?;

        // Now check that everything has been put together as expected
        assert!(built.successfully_build());
        let dir = directory_from_digest(
            &mut conn,
            built
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        assert_eq!(dir.directories.first().unwrap().name, "rootfs");
        assert_eq!(dir.files.len(), 0);

        let sub_dir = directory_from_digest(
            &mut conn,
            dir.directories
                .first()
                .unwrap()
                .digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        assert_eq!(sub_dir.directories.first().unwrap().name, "example_folder");
        assert_eq!(sub_dir.files.len(), 0);

        let sub_dir = directory_from_digest(
            &mut conn,
            sub_dir
                .directories
                .first()
                .unwrap()
                .digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        assert_eq!(sub_dir.directories.len(), 0);
        assert_eq!(sub_dir.files.first().unwrap().name, "test_build");

        drop(running_yts);
        Ok(())
    }

    // This tests that run time deps are loaded in for a dependency when the
    // staging options require it.
    //
    // This is a bit like genimage elements that load them selves and there
    // run time deps into a sub folder to act like a fs that genimage will
    // use to construct the root dir for a target system
    #[tokio::test]
    async fn test_stage_source_deps() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_default(address, "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut conn = full_config.get_local().to_owned().into();

        // Create some files to represent what our depended on elements would have built
        let upload_content = "Some example contents\n".as_bytes().to_owned();
        let upload_digest = upload_blob(&mut conn, upload_content).await?;
        let upload_dir_a =
            place_file_in_dir(&mut conn, "test_build".to_string(), upload_digest, false).await?;
        let upload_dir =
            offset_digest(&mut conn, upload_dir_a, PathBuf::from("example_folder")).await?;

        let mut project = Project::for_test();

        let mut source = HashMap::new();
        source.insert("kind".to_string(), "cas".to_string());
        source.insert("digest".to_string(), digest_to_string(&upload_dir));
        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![source],
            None,
            None,
            None,
        );

        project.add_serde_test_element(element_name.clone(), input_element);
        let element_base_ref = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        let builder_import = BuildPlugin {
            kind: "bootstrap_import".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        };
        let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_base_ref,
                cancelation_issuer.clone(),
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.first().unwrap().name, "example_folder");
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_base_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // At this point we have loaded our base elements result into the local cache so later elements can build
        // on top of it.

        // Fully built base element

        // This intermediate element just needs building, it is a dummy to see that layer 2 deps are pulled through this direct dep
        let element_inner_name = "Intermediate".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "BaseElement".to_string(),
                Some(BuildType::Run),
                Some("/".to_string()),
                None,
                None,
            ))]),
            None,
            None,
        );

        project.add_serde_test_element(element_inner_name.clone(), input_element);
        let element_inter_ref = project
            .get_element(&Arc::from(element_inner_name.as_str()))
            .await
            .unwrap();

        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_inter_ref,
                cancelation_issuer,
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.len(), 0);
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_inter_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // Finished building intermediate element

        // This element test a source
        let source_plugin = SourcePlugin {
            kind: "source_a".to_string(),
            dependencies: vec![element_inner_name],
            cli: "".to_string(),
            compound: Compound::None,
            track: None,
        };
        project.add_test_source_plugin("source_a".to_string(), source_plugin.clone());

        let fetch_digest = source_plugin
            .get_root_directory(&mut conn, &project)
            .await?;

        let dir = directory_from_digest(&mut conn, &fetch_digest).await?;

        assert_eq!(dir.directories.first().unwrap().name, "example_folder");
        assert_eq!(dir.files.len(), 0);

        let sub_dir = directory_from_digest(
            &mut conn,
            dir.directories
                .first()
                .unwrap()
                .digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        assert_eq!(sub_dir.directories.len(), 0);
        assert_eq!(sub_dir.files.first().unwrap().name, "test_build");

        drop(running_yts);
        Ok(())
    }

    // This tests that run time deps are loaded in for a dependency when the
    // staging options require it.
    //
    // A bit like test_stage_no_child_deps except checking no children option
    #[tokio::test]
    async fn test_stage_no_child_deps() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_default(address, "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut conn = full_config.get_local().to_owned().into();

        // Create some files to represent what our depended on elements would have built
        let upload_content = "Some example contents\n".as_bytes().to_owned();
        let upload_digest = upload_blob(&mut conn, upload_content).await?;
        let upload_dir_a = place_file_in_dir(
            &mut conn,
            "test_build".to_string(),
            upload_digest.clone(),
            false,
        )
        .await?;
        let upload_dir =
            offset_digest(&mut conn, upload_dir_a, PathBuf::from("example_folder")).await?;

        let mut project = Project::for_test();

        let mut source = HashMap::new();
        source.insert("kind".to_string(), "cas".to_string());
        source.insert("digest".to_string(), digest_to_string(&upload_dir));
        let element_name = "BaseElement".to_string();
        let serde_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![source],
            None,
            None,
            None,
        );

        project.add_serde_test_element(element_name.clone(), serde_element);
        let element_base_ref = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        let builder_import = BuildPlugin {
            kind: "bootstrap_import".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        };
        let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_base_ref,
                cancelation_issuer.clone(),
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.first().unwrap().name, "example_folder");
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_base_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // At this point we have loaded our base elements result into the local cache so later elements can build
        // on top of it.

        // This intermediate element just needs building, it is a dummy to see that layer 2 deps are pulled through this direct dep
        let element_intermediate_name = "Intermediate".to_string();
        let serde_intermediate_element = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            None,
            Some(vec![StringOrStruct::String(element_name)]),
        );

        project.add_serde_test_element(
            element_intermediate_name.clone(),
            serde_intermediate_element,
        );
        let element_inter_ref = project
            .get_element(&Arc::from(element_intermediate_name.as_str()))
            .await
            .unwrap();

        let (cancelation_issuer, _cancelation_notification) = broadcast::channel(5);
        let build_result = builder_import
            .build(
                &project,
                &full_config,
                &element_inter_ref,
                cancelation_issuer.clone(),
            )
            .await?;
        let dir = directory_from_digest(
            &mut conn,
            build_result
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("built {:?} {:?}", dir.files, dir.directories);

        assert_eq!(dir.directories.len(), 0);
        assert_eq!(dir.files.len(), 0);

        cache_build(&mut conn, &project, &element_inter_ref, &build_result)
            .await
            .expect("Could not cache element locally");
        // Finished building intermediate element

        // This element is representative of a genimage element creating a full file system
        let element_top_name = "ToBuild".to_string();
        let mut raw_vars = HashMap::new();
        raw_vars.insert("cli".to_string(), "skip".to_string());
        let serde_top_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            Some(element_top_name.clone()),
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "Intermediate".to_string(),
                Some(BuildType::Build),
                Some("/rootfs".to_string()),
                Some(StageType::Run),
                None,
            ))]),
            None,
            Some(Variables::from_dic(JoinType::Join, raw_vars)),
        );

        project.add_serde_test_element(element_top_name.clone(), serde_top_element);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_top_name.as_str()))
            .await
            .unwrap();
        println!("el {element_fifth_ref:?}");

        let builder_build = BuildPlugin {
            kind: "mock_builder".to_string(),
            dependencies: vec![],
            cli: "".to_string(),
        };

        let built = builder_build
            .build(
                &project,
                &full_config,
                &element_fifth_ref,
                cancelation_issuer,
            )
            .await?;

        // Now check that everything has been put together as expected
        assert!(built.successfully_build());
        let dir = directory_from_digest(
            &mut conn,
            built
                .build_output
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("{dir:?}");
        assert_eq!(dir.directories.first().unwrap().name, "rootfs");
        assert_eq!(dir.files.len(), 0);

        let sub_dir = directory_from_digest(
            &mut conn,
            dir.directories
                .first()
                .unwrap()
                .digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("{sub_dir:?}");
        assert_eq!(sub_dir.directories.len(), 1);
        assert_eq!(sub_dir.files.len(), 0);

        let sub_sub_dir = directory_from_digest(
            &mut conn,
            sub_dir
                .directories
                .first()
                .unwrap()
                .digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?,
        )
        .await?;
        println!("{sub_sub_dir:?}");
        assert_eq!(sub_sub_dir.directories.len(), 0);
        assert_eq!(sub_sub_dir.files.len(), 1);
        let file = sub_sub_dir.files.first().unwrap();
        println!("{file:?}");
        assert_eq!(file.digest.as_ref().unwrap(), &upload_digest);
        drop(running_yts);
        Ok(())
    }
}
