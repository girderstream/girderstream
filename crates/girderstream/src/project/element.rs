// Copyright 2022-2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Elements contain the instructions to build a particular piece of software
//!
//! Elements need to be passed around especially by the schedular etc
//! so to avoid too many ownership issues the elements are owned by
//! the project and ElementRef's are used to point to them.
use std::{
    collections::HashMap,
    fmt,
    hash::{Hash, Hasher},
    ops::{Deref, DerefMut},
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

use colored::Colorize;
use hex;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serde_either::StringOrStruct;
use sha2::{Digest, Sha256};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
    sync::broadcast,
};
use yaba::execution::SandboxMountSettings;

use super::{
    plugin::digest_of_dependencies,
    project::{Project, ProjectContext},
    BuildResult, ConfigWhen, FetchResult,
};

use crate::{
    build::run_build_plugin,
    cache::{build_asset_key_by_parts, get_cached_build, get_cached_fetch},
    reapi::push_cached_build,
    yaml_misc::{chunked_grd_file, update_values_in_source},
    Crapshoot,
};
use reapi_tools::{
    cas_fs::{directory_from_digest, join_digests},
    fs_io::{checkout, upload_dir},
    grpc_io::digest_to_string,
    virtual_fs::{VirtualDir, VirtualFile},
    FullConfig, ReapiConnection,
};
/// What added this build time dependency to the build graph
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Eq)]
pub enum BuildSource {
    /// Added via the Elements dependencies section
    Element,
    /// Added via the Elements source plugins
    Source,
    /// Added via the Elements build plugin
    Build,
}

/// These are the build types of dependencies
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub(crate) enum BuildType {
    /// Only used after the build but needed for the element to work
    Run,
    /// Only needed while building the element but not to run it
    Build,
    /// Needed to build and run a element, ie a dynamically linked
    /// lib that the element needs
    BuildAndRun,
}

/// These are for which dependencies dencpendiences should be included.
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub(crate) enum StageType {
    /// Only stage the run dependencies along with the depended on element
    Run,
    /// Only stage the build dependencies along with the depended on element
    Build,
    /// Stage both the Build and Run dependencies along with the depended on element
    BuildAndRun,
    /// Only stage this element without any dependencies
    None,
}

/// The other elements that this element needs to ether build and or run
///
/// This struct can hold the name, scope and location to mount the element when
/// building
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputDependency {
    dependency: String,
    scope: Option<BuildType>,
    location: Option<String>,
    stage: Option<StageType>,
    when: Option<ConfigWhen>,
}

impl InputDependency {
    #[cfg(test)]
    /// Quickly create a Dependency when unit testing
    pub fn new(
        dependency: String,
        scope: Option<BuildType>,
        location: Option<String>,
        stage: Option<StageType>,
        when: Option<ConfigWhen>,
    ) -> Self {
        Self {
            dependency,
            scope,
            location,
            stage,
            when,
        }
    }
}

/// The way to combine the elements plugin and env vars with the projects
///
/// The plugin can ether combine its plugin and environment variables with the
/// project context selected
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Copy)]
pub(crate) enum JoinType {
    /// Combine with the project
    Join,
    /// Just use the element variables and not the projects
    Replace,
}

/// The elements plugin variables
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct Variables {
    join_strategy: JoinType,
    variables: HashMap<String, String>,
}

impl Variables {
    /// Get all the variables and there values
    pub fn get_variables(&self) -> HashMap<String, String> {
        self.variables.clone()
    }

    /// Get the join strategy
    pub fn get_join_stratagy(&self) -> JoinType {
        self.join_strategy
    }
    #[cfg(test)]
    pub fn from_dic(strategy: JoinType, dic: HashMap<String, String>) -> Self {
        Self {
            join_strategy: strategy,
            variables: dic,
        }
    }
}

/// This provides the format of all the .grd files in a project
///
/// This should be quick to create and have a low memory footprint as this
/// will effect how many element projects we can load for a given size of
/// computer given nominally consistent element sizes. And effect the speed at
/// which the project can be loaded.
///
/// The speed and size constraints are consistent for project size rather than
/// element that need to be built as we load all elements when constructing
/// the project not just those being built. But we only turn the element that
/// are build and there dependencies to project elements for a given run of
/// Girderstream.
#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct RawInputElement {
    kind: String,
    provides: Option<String>,
    sources: Vec<HashMap<String, String>>,
    depends: Option<Vec<StringOrStruct<InputDependency>>>,
    context: Option<String>,
    plugin_variables: Option<Variables>,
    environment: Option<Variables>,
}

#[derive(Debug, PartialEq)]
pub(crate) struct InputElement {
    raw_input: RawInputElement,
    input_source: Option<PathBuf>,
}

impl InputElement {
    /// Create a [`InputElement`] from a [`VirtualFile`]
    ///
    /// This should not panic but can return errors due to grpc subsystem failures
    /// or the element not being properly formatted yaml.
    pub async fn new(
        connection: &mut ReapiConnection,
        element_file: &mut VirtualFile,
    ) -> anyhow::Result<Self> {
        let s = &element_file.get_content(connection).await?;
        let input_element_raw: RawInputElement = serde_yaml::from_str(s).map_err(|err| {
            anyhow::anyhow!("could not process: \"{:?}\" because: {}", element_file, err)
        })?;
        let input_source = match element_file {
            VirtualFile::CasBacked(_) => None,
            VirtualFile::Local(path) => Some(path.clone()),
        };
        let input_element = InputElement {
            raw_input: input_element_raw,
            input_source,
        };

        Ok(input_element)
    }

    pub(crate) async fn resolve_local_sources(
        &mut self,
        connection: &mut ReapiConnection,
        project: &Project,
    ) -> anyhow::Result<()> {
        let root = project.get_root();
        for source in &mut self.raw_input.sources {
            if source.get("kind").unwrap() == "local" {
                let dir = source.get("location").unwrap();
                let dir = PathBuf::from(dir);
                let mut this_dir: VirtualDir = root.clone();
                for cunk in dir.components() {
                    this_dir = this_dir
                        .get_dir_name(connection, cunk.as_os_str().to_str().unwrap())
                        .await;
                }

                let dir_digest = match this_dir {
                    VirtualDir::CasBacked((dir_digest, _, _)) => dir_digest,
                    VirtualDir::Local(this_dir) => upload_dir(connection, &this_dir).await?,
                };

                *source = HashMap::from([
                    ("kind".to_string(), "cas".to_string()),
                    ("digest".to_string(), digest_to_string(&dir_digest)),
                ])
            }
        }
        Ok(())
    }

    /// Get the kind of the element
    pub fn kind(&self) -> String {
        self.raw_input.kind.clone()
    }

    /// Get what this element provides
    pub fn provides(&self) -> Option<String> {
        self.raw_input.provides.clone()
    }

    #[cfg(test)]
    pub fn new_for_test(
        kind: String,
        provides: Option<String>,
        depends: Option<Vec<StringOrStruct<InputDependency>>>,
    ) -> Self {
        Self {
            raw_input: RawInputElement {
                kind,
                provides,
                sources: vec![],
                depends,
                context: None,
                plugin_variables: None,
                environment: None,
            },
            input_source: None,
        }
    }

    #[cfg(test)]
    pub fn new_for_test_extra(
        kind: String,
        provides: Option<String>,
        sources: Vec<HashMap<String, String>>,
        depends: Option<Vec<StringOrStruct<InputDependency>>>,
        context: Option<String>,
        plugin_variables: Option<Variables>,
    ) -> Self {
        Self {
            raw_input: RawInputElement {
                kind,
                provides,
                sources,
                depends,
                context,
                plugin_variables,
                environment: None,
            },
            input_source: None,
        }
    }
    /// Get the build and run time dependencies of this element
    pub fn depends_build_run(&self) -> anyhow::Result<Vec<DependenceProto>> {
        let mut all_deps = vec![];

        if let Some(dep_list) = &self.raw_input.depends {
            for dep in dep_list {
                match dep {
                    StringOrStruct::String(s) => {
                        all_deps.push(DependenceProto::from_name_element(Arc::from(s.as_str())));
                    }
                    StringOrStruct::Struct(dep) => {
                        if let Some(scope) = &dep.scope {
                            match scope {
                                BuildType::Build => {
                                    all_deps.push(DependenceProto::new(
                                        Arc::from(dep.dependency.as_str()),
                                        dep.location.to_owned().unwrap_or_else(|| "/".to_string()),
                                        DependenceType::Build(BuildSource::Element),
                                        dep.stage.to_owned().unwrap_or(StageType::Run).into(),
                                        dep.when.clone(),
                                    ));
                                }
                                BuildType::Run => {
                                    all_deps.push(DependenceProto::new(
                                        Arc::from(dep.dependency.as_str()),
                                        dep.location.to_owned().unwrap_or_else(|| "/".to_string()),
                                        DependenceType::Run,
                                        DependenceStageType::Run,
                                        dep.when.clone(),
                                    ));

                                    if dep.stage.is_some() {
                                        anyhow::bail!("Staging properties for run time only dependencies should not be specified")
                                    }
                                }
                                BuildType::BuildAndRun => {
                                    all_deps.push(DependenceProto::new(
                                        Arc::from(dep.dependency.as_str()),
                                        dep.location.to_owned().unwrap_or_else(|| "/".to_string()),
                                        DependenceType::RunAndBuild,
                                        dep.stage.to_owned().unwrap_or(StageType::Run).into(),
                                        dep.when.clone(),
                                    ));
                                }
                            }
                        } else {
                            all_deps.push(DependenceProto::new(
                                Arc::from(dep.dependency.as_str()),
                                dep.location.to_owned().unwrap_or_else(|| "/".to_string()),
                                DependenceType::RunAndBuild,
                                dep.stage.to_owned().unwrap_or(StageType::Run).into(),
                                dep.when.clone(),
                            ));
                        }
                    }
                }
            }
        }
        Ok(all_deps)
    }
}

/// SourceDetails specifies how to fetch and stage the source files
///
/// This is just a wrapper around `HashMap<String, String>` which
/// does everything we need but by having its own type we make it much
/// easer to understand the code and the compiler has more information
/// to help spot issues.
#[derive(Debug, Clone)]
pub struct SourceDetails(HashMap<String, String>);

impl Deref for SourceDetails {
    type Target = HashMap<String, String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl SourceDetails {
    // We should have a creation function that validates that we have a `kind`
    // etc.

    /// All SourceDetails must have a type so we provide a helpful rapper
    /// for getting it from the underlying hashmap
    pub fn kind(&self) -> String {
        self.get("kind").unwrap().clone()
    }
}

/// PluginVariables gives the element specific information for the build plugin
///
/// This is just a wrapper around `HashMap<String, String>` which
/// does everything we need but by having its own type we make it much
/// easer to understand the code and the compiler has more information
/// to help spot issues.
#[derive(Debug, Default)]
pub struct PluginVariables(HashMap<String, String>);

impl Deref for PluginVariables {
    type Target = HashMap<String, String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for PluginVariables {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl PluginVariables {
    /// Create a `PluginVariables` from a `InputElement` and the project
    ///
    /// There is a bit of logic here, the Replace and Join logic here
    /// is not being used a lot in real projects but it seems like a sensible
    /// thing to have. However we should revisit this at some point soon.
    fn new(raw_element: &InputElement, project_context: &ProjectContext) -> Self {
        PluginVariables(
            if let Some(envs) = &raw_element.raw_input.plugin_variables {
                if envs.get_join_stratagy() == JoinType::Replace {
                    HashMap::new()
                } else {
                    project_context.get_variables().clone()
                }
            } else {
                project_context.get_variables().clone()
            },
        )
    }

    /// A pub(crate) initializer of PluginVariables for testing
    ///
    /// Normally SourceDetails is only create in this mod but
    /// for unit testing other girderstream mod's its helpful
    /// to create these when testing.
    #[cfg(test)]
    pub fn from_hashmap(raw: HashMap<String, String>) -> Self {
        Self(raw)
    }
}

/// Which (if any) deps should be stage along with the element
///
/// In the build sandbox when this element is a build dep.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DependenceStageType {
    NoChildren,
    Build,
    Run,
    RunAndBuild,
}

impl From<StageType> for DependenceStageType {
    fn from(stage_type: StageType) -> Self {
        match stage_type {
            StageType::Build => Self::Build,
            StageType::Run => Self::Run,
            StageType::BuildAndRun => Self::RunAndBuild,
            StageType::None => Self::NoChildren,
        }
    }
}

/// Enum to indicate when a given dependency is required
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DependenceType {
    Build(BuildSource),
    Run,
    RunAndBuild,
}

impl From<BuildType> for DependenceType {
    fn from(build_type: BuildType) -> Self {
        match build_type {
            BuildType::Build => Self::Build(BuildSource::Element),
            BuildType::Run => Self::Run,
            BuildType::BuildAndRun => Self::RunAndBuild,
        }
    }
}

/// A struct to hold information about the Dependencies
///
/// A element needs to know about the elements it needs as well as and other
/// things about it like were in the sandbox to stage it. Going forwards we
/// might want other settings like bst's filters etc.
#[derive(Clone, PartialEq, Eq)]
pub struct Dependence {
    /// The element that this dependency points to
    element: ElementRef,
    /// The location that this dep should be mounted into in the sandbox
    location: String,
    /// When this dep should be used
    dep_type: DependenceType,
    /// Which of this dependencies dependencies should be added to the sandbox
    stage_type: DependenceStageType,
}

impl fmt::Debug for Dependence {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Dependence")
            .field("element", &self.element.get_name())
            .field("location", &self.location)
            .field("dep_type", &self.dep_type)
            .field("stage_type", &self.stage_type)
            .finish()
    }
}

impl Dependence {
    pub(crate) fn from_proto(proto: DependenceProto, project: &Project) -> anyhow::Result<Self> {
        Ok(Dependence {
            element: project.get_existing_element(&proto.name)?,
            location: proto.location,
            dep_type: proto.dep_type,
            stage_type: proto.stage_type,
        })
    }

    /// Get the name of the element we depend on
    pub(crate) fn get_name(&self) -> &Arc<str> {
        self.element.get_name()
    }
    /// Is this a a run time dependency
    pub(crate) fn is_runtime(&self) -> bool {
        self.dep_type == DependenceType::Run || self.dep_type == DependenceType::RunAndBuild
    }
    /// Is this a a build time dependency
    pub(crate) fn is_buildtime(&self) -> bool {
        match self.dep_type {
            DependenceType::Build(_) => true,
            DependenceType::Run => false,
            DependenceType::RunAndBuild => true,
        }
    }

    /// Get the location in the sandbox to mount the depended on element
    pub(crate) fn get_location(&self) -> &String {
        &self.location
    }

    /// Get the location in the sandbox to mount the depended on element
    pub(crate) fn set_location(&mut self, location: String) {
        self.location = location;
    }
    /// Get all the build time dependencies of this dependency
    pub(crate) fn get_build_deps(&self) -> anyhow::Result<Vec<Dependence>> {
        Ok(self
            .element
            .get_all_deps()
            .filter(|&dep| dep.is_buildtime())
            .map(|dep| dep.to_owned())
            .collect())
    }

    /// Get all the run time dependencies of this dependency
    /// TODO element has a pretty nice `get_all_run_dep_elements`
    /// maybe we should combine the two..
    pub(crate) fn get_run_deps(&self) -> anyhow::Result<Vec<Dependence>> {
        let mut all_deps = vec![];
        for dep in self.element.get_all_deps().filter(|&dep| dep.is_runtime()) {
            for sub_dep in dep.get_run_deps()? {
                if !all_deps.contains(&sub_dep) {
                    all_deps.push(sub_dep);
                }
            }
            if !all_deps.contains(dep) {
                all_deps.push(dep.to_owned());
            }
        }
        Ok(all_deps)
    }

    /// Get all run and build time dependencies of this dependency
    pub(crate) fn get_all_deps(&self) -> impl Iterator<Item = &Dependence> {
        self.element.get_all_deps()
    }

    /// Indicate if this dependency was added via the elements depends section
    pub(crate) fn is_from_element(&self) -> bool {
        match &self.dep_type {
            DependenceType::Build(source) => match &source {
                BuildSource::Element => true,
                BuildSource::Source => false,
                BuildSource::Build => false,
            },
            DependenceType::Run => true,
            DependenceType::RunAndBuild => true,
        }
    }

    /// Indicate if this dependency was added via the elements build plugin
    pub(crate) fn is_build_plugin(&self) -> bool {
        match &self.dep_type {
            DependenceType::Build(source) => match &source {
                BuildSource::Element => false,
                BuildSource::Source => false,
                BuildSource::Build => true,
            },
            DependenceType::Run => false,
            DependenceType::RunAndBuild => false,
        }
    }

    /// Get the scope of this dependency
    pub(crate) fn get_stage_scope(&self) -> &DependenceStageType {
        &self.stage_type
    }
}
/// A struct to hold information about the Dependencies
///
/// A element needs to know about the elements it needs as well as and other
/// things about it like were in the sandbox to stage it. Going forwards we
/// might want other settings like bst's filters etc.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DependenceProto {
    name: Arc<str>,
    // Should this be a path buff
    location: String,
    dep_type: DependenceType,
    stage_type: DependenceStageType,
    when: Option<ConfigWhen>,
}

/// Create a Dependence from a name of the element you are adding a dependency for
///
/// This creates a Run and Build time dep that does not stage any extra deps and
/// is loaded into the root of the sandbox at build time.
impl From<Arc<str>> for DependenceProto {
    fn from(dep: Arc<str>) -> Self {
        DependenceProto {
            name: dep,
            location: "/".to_string(),
            dep_type: DependenceType::RunAndBuild,
            stage_type: DependenceStageType::NoChildren,
            when: None,
        }
    }
}

impl DependenceProto {
    /// Get the name of the element we depend on
    pub(crate) fn get_name(&self) -> &Arc<str> {
        &self.name
    }

    /// Create a `Dependence` from the elements name
    ///
    /// And sets the location as the root of the filesystem which will be the
    /// case most times. And no transitive build deps.
    pub(crate) fn from_name_element(name: Arc<str>) -> Self {
        DependenceProto {
            name,
            location: "/".to_string(),
            dep_type: DependenceType::RunAndBuild,
            stage_type: DependenceStageType::NoChildren,
            when: None,
        }
    }

    /// Create a `Dependence` from the elements name
    ///
    /// And sets the location as the root of the filesystem which will be the
    /// case most times. And the run time deps in the build sandbox, needed
    /// to run the plugin.
    pub(crate) fn from_name_plugin_build(name: Arc<str>) -> Self {
        DependenceProto {
            name,
            location: "/".to_string(),
            dep_type: DependenceType::Build(BuildSource::Build),
            stage_type: DependenceStageType::Run,
            when: None,
        }
    }

    pub(crate) fn from_name_plugin_source(name: String) -> Self {
        DependenceProto {
            name: Arc::from(name.as_str()),
            location: "/".to_string(),
            dep_type: DependenceType::Build(BuildSource::Source),
            stage_type: DependenceStageType::Run,
            when: None,
        }
    }

    /// Create a `Dependence` from a given name, location and type.
    pub(crate) fn new(
        name: Arc<str>,
        location: String,
        dep_type: DependenceType,
        stage_type: DependenceStageType,
        when: Option<ConfigWhen>,
    ) -> Self {
        // Todo: this would be a good point todo some kind of
        // validation that location is at least path like
        DependenceProto {
            name,
            location,
            dep_type,
            stage_type,
            when,
        }
    }
}

/// ElementEnvironment gives the element specific env needed to build the element
///
/// This is just a wrapper around `HashMap<String, String>` which
/// does everything we need but by having its own type we make it much
/// easer to understand the code and the compiler has more information
/// to help spot issues.
#[derive(Debug)]
pub struct ElementEnvironment(HashMap<String, String>);

impl Deref for ElementEnvironment {
    type Target = HashMap<String, String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for ElementEnvironment {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl ElementEnvironment {
    /// Create a `ElementEnvironment` from a `InputElement` and the project
    ///
    /// There is a bit of logic here, the Replace and Join logic here
    /// is not being used a lot in real projects but it seems like a sensible
    /// thing to have. However we should revisit this at some point soon.
    fn new(raw_element: &InputElement, project_context: &ProjectContext) -> Self {
        ElementEnvironment(if let Some(envs) = &raw_element.raw_input.environment {
            if envs.get_join_stratagy() == JoinType::Replace {
                HashMap::new()
            } else {
                project_context.get_environment().clone()
            }
        } else {
            project_context.get_environment().clone()
        })
    }
}

#[derive(Debug)]
pub(crate) struct ElementProto {
    kind: String,
    // The dependencies vec can contain multiple references to the same plugin
    // as it may be needed by both source and build plugins etc.
    depends: Vec<DependenceProto>,
    sources: Vec<SourceDetails>,
    plugin_variables: PluginVariables,
    environment: ElementEnvironment,
    input_source: Option<PathBuf>,
}

impl ElementProto {
    // This fn is fairly complex but also has a lot of repeated code.
    // I think the complexity would be more manageable if there was less repeated code.
    // But lets not make the mistake of splitting complexity to the point that the complexity
    // is obfuscated. As hidden complexity is worse than clear complexity.
    /// Combine the project level context with the elements own variables
    fn merge_contexts(
        raw_element: &InputElement,
        project: &Project,
    ) -> (PluginVariables, ElementEnvironment) {
        // First we ether inherit or create blank vars and envs
        let (mut plugin_variables, mut environment): (PluginVariables, ElementEnvironment) =
            if let Some(context_name) = &raw_element.raw_input.context {
                // If the element sets a context we use that context or error out if that context is not in project.conf
                if let Some(project_context) = project.get_context().get(context_name) {
                    // The context was set so we need to use that context
                    let vars = PluginVariables::new(raw_element, project_context);
                    let envs = ElementEnvironment::new(raw_element, project_context);
                    (vars, envs)
                } else {
                    panic!(
                        // Todo: Add plumbing so this also includes the element name,
                        // Either add the name to many function calls or add results
                        // so the error bubbles up some were a better to form the error
                        // message.
                        "Must pic a context '{context_name}' from project.conf '{:?}'",
                        project.get_context().keys(),
                    )
                }
            } else {
                (
                    PluginVariables(HashMap::new()),
                    ElementEnvironment(HashMap::new()),
                )
            };
        // Now we join the per element vars and envs to those we have just got from the project.
        if let Some(vars) = &raw_element.raw_input.plugin_variables {
            for (key, val) in vars.get_variables() {
                plugin_variables.insert(key, val);
            }
        }
        if let Some(vars) = &raw_element.raw_input.environment {
            for (key, val) in vars.get_variables() {
                environment.insert(key, val);
            }
        }
        (plugin_variables, environment)
    }

    /// Create a `Element` from a `InputElement` as well as the project
    ///
    /// This turns the `InputElement` into a `Element` mixing in project
    /// information. I am not sure if this is ideal we might be able to reduce
    /// the amount of getters on `InputElement` by creating a builder patten
    /// for `Element` and then having InputElement have a `into_element`
    pub(crate) fn from_serde(raw_element: InputElement, project: &Project) -> anyhow::Result<Self> {
        let sources = raw_element
            .raw_input
            .sources
            .iter()
            .map(|hashmap| SourceDetails(hashmap.clone()))
            .collect();
        let depends = raw_element.depends_build_run()?;

        let (plugin_variables, environment) = Self::merge_contexts(&raw_element, project);

        let mut element = ElementProto {
            kind: raw_element.kind(),
            depends,
            sources,
            plugin_variables,
            environment,
            input_source: raw_element.input_source,
        };
        element.depends = element.requires(project)?;
        Ok(element)
    }

    /// Calculate the dependencies as part of creating a new `Element`
    ///
    /// Get everything this element points to directly, The result of this functions
    /// is used by the project to "recessively" transform all elements this element
    /// points towards.
    ///
    /// Note, that this can end up with multiple reference to the same dep, as
    /// you may need base element(s) for source, build and standard deps. This may
    /// have simlar or different build source's
    fn requires(&self, project: &Project) -> anyhow::Result<Vec<DependenceProto>> {
        let mut dependencies: Vec<DependenceProto> = vec![];

        for dep in &self.depends {
            if let Some(condition) = &dep.when {
                if condition.met(project.get_selected_configuration())? {
                    dependencies.push(DependenceProto {
                        name: dep.name.clone(),
                        location: dep.location.clone(),
                        dep_type: dep.dep_type.clone(),
                        stage_type: dep.stage_type.clone(),
                        when: None,
                    })
                }
            } else {
                dependencies.push(dep.clone())
            }
        }

        for source in &self.sources {
            for dep in project.get_source_plugin_dependencies(&source.kind()) {
                dependencies.push(dep);
            }
        }
        for dep in project.get_build_plugin_dependencies(&self.kind()) {
            dependencies.push(dep);
        }

        Ok(dependencies)
    }

    /// Get the kind of the element
    // Todo: this should return a borrow.
    pub(crate) fn kind(&self) -> String {
        self.kind.clone()
    }
    /// Get all the dependencies as `Dependencies`
    ///
    /// This is used to generate all the elements for the project
    /// so needs to know about all the direct deps.
    pub(crate) fn get_all_deps(&self) -> impl Iterator<Item = &DependenceProto> {
        self.depends.iter()
    }

    /// Get the weak cache key of the element
    ///
    /// This is used to uniquely identify a element, the weak part signifies
    /// that this only takes the element into account and not its dependencies.
    fn weak_cache_key(&self, project: &Project) -> String {
        let mut dig = Sha256::new();

        dig.update(self.kind.len().to_be_bytes());
        dig.update(self.kind.as_bytes());
        let cli = self.get_cli(project);
        dig.update(cli.len().to_be_bytes());
        dig.update(cli.as_bytes());
        dig.update(self.plugin_variables.len().to_be_bytes());
        for key in self.plugin_variables.keys().sorted() {
            let val = self.plugin_variables.get(key).unwrap();
            dig.update(key.len().to_be_bytes());
            dig.update(key.as_bytes());
            dig.update(val.len().to_be_bytes());
            dig.update(val.as_bytes());
        }
        dig.update(self.environment.len().to_be_bytes());
        for key in self.environment.keys().sorted() {
            let val = self.environment.get(key).unwrap();
            dig.update(key.len().to_be_bytes());
            dig.update(key.as_bytes());
            dig.update(val.len().to_be_bytes());
            dig.update(val.as_bytes());
        }
        dig.update(self.sources.len().to_be_bytes());
        for source in &self.sources {
            let source_bytes =
                project.get_source_bytes(source.get("kind").unwrap().to_owned(), source);
            dig.update(source_bytes.len().to_be_bytes());
            dig.update(&source_bytes);
        }
        hex::encode(dig.finalize())
    }

    /// Get the cli value this element will use to trigger a build inside the sandbox
    ///
    /// This wil combine the elements plugin info and its own plugin variables
    /// to workout what should be called to trigger the build plugins build.
    pub(crate) fn get_cli<'a>(&'a self, project: &'a Project) -> &'a String {
        if let Some(cli) = self.plugin_variables.get("cli") {
            cli
        } else {
            project
                .get_build_plugin(&self.kind())
                .unwrap_or_else(|| panic!("Could not find {}", self.kind()))
                .get_cli()
        }
    }
}

// By the end of a build then all the instance of this enum will be the largest large_enum_variant
// there for we are not saveing memory by suing a Box, we also dont access Elements in a vec but
// through ElementRef via a Arc so are unlikely to get meory access advantages from having better
// packed arrays
#[allow(clippy::large_enum_variant)]
#[derive(Debug)]
pub(crate) enum CacheState {
    NotChecked,
    NotPressent,
    Cached(BuildResult),
}

/// A element ruffly maps to a single pieces of software within a project
///
/// A element struct holds the sources, dependencies and configuration. As well
/// as providing functions between the project build functions and the plugin
/// build functions.
#[derive(Debug)]
pub(crate) struct Element {
    kind: String,
    name: Arc<str>,
    // The dependencies vec can contain multiple references to the same plugin
    // as it may be needed by both source and build plugins etc.
    depends: Vec<Dependence>,
    sources: Vec<SourceDetails>,
    plugin_variables: PluginVariables,
    environment: ElementEnvironment,
    cache_key: Arc<str>,
    weak_cache_key: Arc<str>,
    asset_key: Arc<str>,
    input_source: Option<PathBuf>,
    cached: Mutex<CacheState>,
}

impl Hash for Element {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

/// Get the cache key of the element
///
/// This is used to uniquely identify a element, this element takes the
/// element and its dependencies into account.
fn cache_key(
    local_bytes: &Arc<str>,
    depends: &Vec<Dependence>,
    name: &Arc<str>,
    project: &Project,
) -> anyhow::Result<String> {
    let mut dig = Sha256::new();
    dig.update(local_bytes.as_bytes());

    dig.update(name.len().to_be_bytes());
    dig.update(name.as_bytes());
    for dep in depends {
        if dep.is_buildtime() {
            dig.update(
                project
                    .get_existing_element(dep.get_name())
                    .unwrap()
                    .cache_key()
                    .as_bytes(),
            );
            let location = dep.get_location();
            dig.update(location.len().to_be_bytes());
            dig.update(location.as_bytes());

            let extra_deps = match dep.stage_type {
                DependenceStageType::Run => dep.get_run_deps()?,
                DependenceStageType::Build => dep.get_build_deps()?,
                DependenceStageType::NoChildren => vec![],
                DependenceStageType::RunAndBuild => dep.get_all_deps().cloned().collect(),
            };
            for sub_dep in extra_deps {
                dig.update(
                    project
                        .get_existing_element(sub_dep.get_name())
                        .unwrap()
                        .cache_key()
                        .as_bytes(),
                );
            }
        }
    }
    Ok(hex::encode(dig.finalize()))
}

impl Element {
    /// Create a element for unit testing
    ///
    /// This function is only compiled when unit testing and allow
    /// unit tests to quickly create a set of elements so that they
    /// can test how other code paths work without having to have
    /// a full integration tests worth of over head.
    #[cfg(test)]
    pub(crate) fn for_test(kind: String, name: Arc<str>) -> Self {
        Element {
            kind,
            depends: vec![],
            sources: vec![],
            plugin_variables: PluginVariables(HashMap::new()),
            environment: ElementEnvironment(HashMap::new()),
            cache_key: Arc::from(""),
            weak_cache_key: Arc::from(""),
            asset_key: Arc::from(""),
            name,
            input_source: None,
            cached: CacheState::NotChecked.into(),
        }
    }

    /// Get the kind of the element
    // Todo: this should return a borrow.
    pub(crate) fn kind(&self) -> String {
        self.kind.clone()
    }

    /// Get the runtime dependencies as `Dependence`
    pub(crate) fn get_run_dependencies(&self) -> impl Iterator<Item = &Dependence> {
        self.depends.iter().filter(|dep| dep.is_runtime())
    }

    /// Get all the dependencies as `Dependencies`
    ///
    /// This is used to generate all the elements for the project
    /// so needs to know about all the direct deps.
    pub(crate) fn get_all_deps(&self) -> impl Iterator<Item = &Dependence> {
        self.depends.iter()
    }

    /// Get the cli value this element will use to trigger a build inside the sandbox
    ///
    /// This wil combine the elements plugin info and its own plugin variables
    /// to workout what should be called to trigger the build plugins build.
    pub(crate) fn get_cli<'a>(&'a self, project: &'a Project) -> &'a String {
        if let Some(cli) = self.plugin_variables.get("cli") {
            cli
        } else {
            project
                .get_build_plugin(&self.kind())
                .unwrap_or_else(|| panic!("Could not find {}", self.kind()))
                .get_cli()
        }
    }

    pub(crate) fn dep_count(&self) -> usize {
        self.depends.len()
    }

    pub(crate) fn from_proto(
        name: Arc<str>,
        element: ElementProto,
        project: &Project,
    ) -> anyhow::Result<Self> {
        let weak_cache_key = Arc::from(element.weak_cache_key(project));
        let depends = element
            .depends
            .into_iter()
            .map(|dep| Dependence::from_proto(dep, project).unwrap())
            .collect();
        let cache_key = Arc::from(cache_key(&weak_cache_key, &depends, &name, project)?.as_str());
        let build_asset_key = build_asset_key_by_parts(project, &element.kind, &cache_key);

        Ok(Self {
            kind: element.kind,
            depends,
            sources: element.sources,
            plugin_variables: element.plugin_variables,
            environment: element.environment,
            cache_key,
            weak_cache_key,
            asset_key: Arc::from(build_asset_key),
            name,
            input_source: element.input_source,
            cached: CacheState::NotChecked.into(),
        })
    }
}

/// A struct to hold a Arc pointer to a Element and some other details
///
/// This struct is fairly cheap to clone and points to the element that the
/// project owns. This makes it use full for the project and schedulers
/// etc to pass around the element info while the project owns the element.
#[allow(clippy::derived_hash_with_manual_eq)]
#[derive(Debug, Clone, Hash)]
pub struct ElementRef {
    element: Arc<Element>,
}

impl PartialEq for ElementRef {
    fn eq(&self, other: &ElementRef) -> bool {
        self.eq_element(other)
    }
}
impl Eq for ElementRef {}

impl fmt::Display for ElementRef {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ElementRef of '{}'", self.element.name)
    }
}

impl ElementRef {
    /// Create a `ElementRef` from a `Element`
    ///
    /// This can be called again but the `ElementRef` can also be cloned.
    pub(crate) fn new(element: Element) -> Self {
        ElementRef {
            element: Arc::new(element),
        }
    }

    /// Return if two ElementRefs point to the same Element
    pub fn eq_element(&self, other: &Self) -> bool {
        Arc::ptr_eq(&self.element, &other.element)
    }

    /// Get the sources of this element
    pub fn get_sources(&self) -> Vec<SourceDetails> {
        self.element.sources.clone()
    }

    /// Get the build and runtime dependencies of this element
    pub fn get_all_deps(&self) -> impl Iterator<Item = &Dependence> {
        self.element.get_all_deps()
    }

    /// Get the build and there runtime dependencies of this element
    pub fn get_recursive_build_elements(&self) -> impl Iterator<Item = ElementRef> {
        let mut all_deps = vec![];
        for build_deps in self.get_all_deps().filter(|dep| dep.is_buildtime()) {
            let new_element = &build_deps.element;
            if !all_deps.contains(new_element) {
                all_deps.push(new_element.to_owned());

                match build_deps.get_stage_scope() {
                    DependenceStageType::Build => {
                        for sub_ele in new_element.get_recursive_build_elements() {
                            if !all_deps.contains(&sub_ele) {
                                all_deps.push(sub_ele)
                            }
                        }
                    }
                    DependenceStageType::Run => {
                        for sub_ele in new_element.get_all_run_dep_elements() {
                            if !all_deps.contains(&sub_ele) {
                                all_deps.push(sub_ele)
                            }
                        }
                    }
                    DependenceStageType::NoChildren => {}
                    DependenceStageType::RunAndBuild => {
                        for sub_ele in new_element.get_all_run_dep_elements() {
                            if !all_deps.contains(&sub_ele) {
                                all_deps.push(sub_ele)
                            }
                        }
                        for sub_ele in new_element.get_recursive_build_elements() {
                            if !all_deps.contains(&sub_ele) {
                                all_deps.push(sub_ele)
                            }
                        }
                    }
                };
            };
        }
        all_deps.into_iter()
    }

    /// Get the runtime dependencies of this element as `Dependence`
    pub(crate) fn get_run_dependencies(&self) -> impl Iterator<Item = &Dependence> {
        self.element.get_run_dependencies()
    }

    /// Get the runtime dependencies of this element as `Dependence`
    ///
    /// This is used by the plugin code etc and so helpfully actually includes itself
    /// in the [`ElementRef`]'s that it returns
    pub(crate) fn get_all_run_dep_elements(&self) -> impl Iterator<Item = ElementRef> {
        let mut all_run_deps = vec![self.clone()];
        self.extend_all_run_dep_elements(&mut all_run_deps);
        all_run_deps.into_iter()
    }

    fn extend_all_run_dep_elements(&self, all_run_deps: &mut Vec<ElementRef>) {
        for run_deps in self.element.get_run_dependencies() {
            let new_element = &run_deps.element;
            if !all_run_deps.contains(new_element) {
                all_run_deps.push(new_element.to_owned());

                for sub_ele in new_element.get_all_run_dep_elements() {
                    if !all_run_deps.contains(&sub_ele) {
                        all_run_deps.push(sub_ele)
                    }
                }
            };
        }
    }

    /// Get the plugin variables of the underlying element
    pub(crate) fn get_plugin_variables(&self) -> PluginVariables {
        PluginVariables(self.element.plugin_variables.clone())
    }

    /// Get the plugin variables of the underlying element
    pub(crate) fn get_environment_variables(&self) -> ElementEnvironment {
        ElementEnvironment(self.element.environment.clone())
    }

    /// Calculate the weak cache key of this element
    ///
    /// The cache key is a hash of the element, it will
    /// change if anything about the element changes.
    ///
    /// The weak part denotes that it is local to this
    /// element, a strong cache key changes if any of the
    /// elements dependencies change.
    pub fn weak_cache_key(&self) -> &str {
        &self.element.weak_cache_key
    }

    /// Calculate the cache key of this element
    ///
    /// The cache key is a hash of the element, it will
    /// change if anything about the element changes.
    ///
    /// A strong cache key is combination of a element and its
    /// dependencies collective cache keys.
    pub fn cache_key(&self) -> &str {
        &self.element.cache_key
    }

    /// Get the kind of this element
    pub fn get_kind(&self) -> String {
        self.element.kind()
    }

    /// Get the name of the element
    ///
    /// This is what this element provides to the project and other elements
    /// may depend on it by this name.
    pub fn get_name(&self) -> &Arc<str> {
        &self.element.name
    }

    pub(crate) fn build_asset_key(&self) -> &Arc<str> {
        &self.element.asset_key
    }
    /// Get the cli value this element will use to trigger a build inside the sandbox
    ///
    /// This wil combine the elements plugin info and its own plugin variables
    /// to workout what should be called to trigger the build plugins build.
    pub fn get_cli<'a>(&'a self, project: &'a Project) -> &'a String {
        self.element.get_cli(project)
    }

    /// Check out the element
    pub async fn checkout(
        &self,
        connection: &mut ReapiConnection,
        location: &String,
        fix_abs_links: bool,
        checkout_run_deps: bool,
    ) -> Crapshoot {
        let mut build_result = get_cached_build(connection, self)
            .await
            .unwrap_or_else(|_| panic!("Could not get cached result for {self:?}"))
            .get_result_digest()
            .unwrap()
            .clone();
        let directory = directory_from_digest(connection, &build_result).await?;
        println!("directory: {:?}", directory);
        println!("checkout_run_deps: {checkout_run_deps}");
        if checkout_run_deps {
            for dep in self.get_all_run_dep_elements() {
                let dep_name = dep.get_name();
                println!("dep: {dep_name:?}");
                let new_digest = get_cached_build(connection, &dep)
                    .await
                    .unwrap_or_else(|_| panic!("Could not get cached result for {dep:?}"))
                    .get_result_digest()
                    .unwrap()
                    .clone();
                build_result = join_digests(connection, &build_result, &new_digest)
                    .await
                    .unwrap();
            }
        }
        println!("Checking out Digest: {build_result:?}");
        checkout(
            connection,
            build_result,
            Path::new(&location),
            fix_abs_links,
        )
        .await
    }

    /// Generate an interactive build shell
    pub async fn build_shell(
        &self,
        connection: &mut ReapiConnection,
        project: &Project,
        cancelation: broadcast::Sender<()>,
    ) -> Crapshoot {
        let build_plugin = project
            .get_build_plugin(&self.get_kind())
            .ok_or(anyhow::anyhow!("Could not find build plugin"))?;
        build_plugin
            .failed_build_shell(connection, self, project, cancelation)
            .await?;
        Ok(())
    }

    /// Generate an interactive shell
    pub async fn shell(
        &self,
        connection: &mut ReapiConnection,
        project: &Project,
        cancelation: broadcast::Sender<()>,
    ) -> Crapshoot {
        let artifact = self.get_build_artifact(connection).await?;

        let deps: Vec<Dependence> = self
            .get_all_run_dep_elements()
            .map(|el| Dependence {
                element: el,
                location: "/".to_string(),
                dep_type: DependenceType::Run,
                stage_type: DependenceStageType::Run,
            })
            .collect();
        let root_digest = artifact.build_output.unwrap();

        let root_digest =
            digest_of_dependencies(connection, deps.iter(), root_digest, project).await?;

        // Im not sure we should load all the build plugin env vars in
        // But we shoul be adding in at least PATH
        let build_plugin = project
            .get_build_plugin(&self.get_kind())
            .ok_or(anyhow::anyhow!("Could not find build plugin"))?;
        let environment_variables = build_plugin.environment_variables(self);

        println!("Debugging shell:");
        let sandbox_settings = SandboxMountSettings {
            root_digest,
            environment_variables,
            arguments: vec!["/bin/sh".to_string()],
            with_network: false,
            interactive: true,
            cwd: None,
        };
        let _ = run_build_plugin(connection, [].iter(), sandbox_settings, cancelation).await;

        Ok(())
    }

    /// Get the fetch artifacts used for the build
    pub async fn get_source_artifacts(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<Vec<FetchResult>> {
        let build_result = get_cached_build(connection, self).await.map_err(|e| {
            let cache_key = self.cache_key();
            anyhow::anyhow!(
                "Error when retrieving cached build for {self:?} :: {cache_key} got: {e:?}"
            )
        })?;
        let mut result = vec![];
        for key in build_result.source_keys {
            if key.is_empty() {
                continue;
            }
            result.push(
                get_cached_fetch(connection, key.clone())
                    .await
                    .map_err(|e| {
                        anyhow::anyhow!(
                    "when retrieving cached fetch for {self:?} with key: {key:?} got: {e:?}"
                )
                    })?,
            );
        }
        Ok(result)
    }

    /// Populate the in memory artifact cache from the reapi cache
    pub(crate) async fn populate_build_cache(&self, connection: &mut ReapiConnection) -> Crapshoot {
        {
            let cache = self.element.cached.lock().unwrap();
            if let CacheState::Cached(_buildresult) = &*cache {
                return Ok(());
            }
        }
        if let Ok(build_result) = get_cached_build(connection, self).await {
            let mut cache = self.element.cached.lock().unwrap();
            *cache = CacheState::Cached(build_result);
        } else {
            let mut cache = self.element.cached.lock().unwrap();
            *cache = CacheState::NotPressent;
        };
        Ok(())
    }

    /// Update the Build Artifact in memory cache
    pub(crate) fn update_build_cache(&self, build_result: BuildResult) {
        let mut cache = self.element.cached.lock().unwrap();
        *cache = CacheState::Cached(build_result);
    }

    /// Get the Build Artifact
    ///
    /// This will check the in memory and the local reapi cache.
    pub async fn get_build_artifact(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<BuildResult> {
        {
            let cache = self.element.cached.lock().unwrap();
            if let CacheState::Cached(build_result) = &*cache {
                return Ok(build_result.clone());
            }
        }

        match get_cached_build(connection, self).await {
            Ok(build_result) => {
                {
                    let mut cache = self.element.cached.lock().unwrap();
                    *cache = CacheState::Cached(build_result.clone());
                }
                Ok(build_result)
            }
            Err(e) => Err(e),
        }
    }

    /// Get the Build Artifact from the in memory cache
    ///
    /// This will not check from the local reapi cache.
    pub fn get_buildresult_from_cache(&self) -> anyhow::Result<Option<BuildResult>> {
        match &*self.element.cached.lock().unwrap() {
            CacheState::NotChecked => Ok(None),
            CacheState::NotPressent => Ok(None),
            CacheState::Cached(cached_build) => Ok(Some(cached_build.clone())),
        }
    }

    /// Get if a Sussesful build has been cached in the in memory cache
    ///
    /// This does not check the local reapi cache
    pub async fn get_build_success_from_cache(&self) -> anyhow::Result<bool> {
        match &*self.element.cached.lock().unwrap() {
            CacheState::NotChecked => Ok(false),
            CacheState::NotPressent => Ok(false),
            CacheState::Cached(cached_build) => Ok(cached_build.successfully_build()),
        }
    }

    /// Get if a Sussesful build has been cached
    ///
    /// This does check the local reapi cache and the in memory cache.
    pub async fn get_build_success(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<bool> {
        // get_build_artifact checks both the local reapi and in memory cache
        let build_result = if let Ok(build_result) = self.get_build_artifact(connection).await {
            build_result
        } else {
            return Ok(false);
        };
        Ok(build_result.successfully_build())
    }

    /// push a element to a remote cas
    pub async fn push(
        &self,
        connection_local: &mut ReapiConnection,
        connection_remote: &mut ReapiConnection,
        project: &Project,
    ) -> Crapshoot {
        push_cached_build(connection_local, connection_remote, project, self).await
    }

    pub fn dep_count(&self) -> usize {
        self.element.dep_count()
    }

    pub async fn track(
        &self,
        project: &Project,
        full_config: &FullConfig,
        cancelation: broadcast::Sender<()>,
    ) -> Crapshoot {
        if let Some(input_source) = &self.element.input_source {
            if let VirtualDir::Local(root_path) = project.get_root() {
                let raw_path = root_path.join(input_source);
                let mut raw_input_file = File::open(raw_path).await?;
                let mut raw_input = vec![];
                raw_input_file.read_to_end(&mut raw_input).await?;
                let grd_file_string = String::from_utf8(raw_input)
                    .expect("Could not read input as utf8 so can not safely track");
                drop(raw_input_file);

                let (start, sources, end) = chunked_grd_file(grd_file_string)?;
                let mut new_sources = vec![];

                // Todo:
                // * allow for tracking specific source
                for (source_details, raw_source) in self.element.sources.iter().zip(sources) {
                    let source = project.get_source(&source_details.kind());
                    let updates = source
                        .track(project, full_config, source_details, cancelation.clone())
                        .await?;
                    new_sources.push(update_values_in_source(raw_source, updates));
                }

                let mut raw_file = File::create(input_source).await?;
                raw_file.write_all(start.as_bytes()).await?;
                for source in new_sources {
                    raw_file.write_all(source.as_bytes()).await?;
                }
                raw_file.write_all(end.as_bytes()).await?;

                Ok(())
            } else {
                anyhow::bail!("A element with a Path should only be able to be generated from a project with a path")
            }
        } else {
            anyhow::bail!("Can not track a element through a junction")
        }
    }
}

/// Get the status of the build archived by the `Option<BuildResult>`
///
/// The options are `Success` `Failure` or `Not Built`
fn cached_status(build_result: &Option<BuildResult>) -> String {
    if let Some(build) = build_result {
        if build.successfully_build() {
            "Success".green().to_string()
        } else {
            "Failure".red().to_string()
        }
    } else {
        "Not Built".yellow().to_string()
    }
}

// The rest of the structs in this file provide a variety of ways to `display`
// a elementRef and a trait. And a trait to make it easy to write code for
// any of the structs.

/// A trait that can be used to create generic functions that display a number
/// of elements in a way that can be generic to any type that implements this
/// trait.
pub trait LifetimedDisplay<'a, T = Self>: fmt::Display {
    type Output;

    /// Create a struct of type `T` which implements `fmt::Display`
    fn new(element: ElementRef, project: &'a Project, cached: Option<BuildResult>) -> Self;
}

/// For a concise implementation of Display for elements
// The life time stuff here is just to say that we only borrow the element
// as long as this element lives. once the struct is dropped
// the borrow of the elementref is returned
pub struct ElementDisplayStd<'a> {
    element: ElementRef,
    _project: &'a Project,
    cached: Option<BuildResult>,
}

impl<'a> LifetimedDisplay<'a> for ElementDisplayStd<'a> {
    type Output = ElementDisplayStd<'a>;

    fn new(element: ElementRef, project: &'a Project, cached: Option<BuildResult>) -> Self {
        Self {
            element,
            _project: project,
            cached,
        }
    }
}

impl fmt::Display for ElementDisplayStd<'_> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "- {}: {: <20}\t",
            "Element".blue(),
            &self.element.element.name.yellow()
        )?;
        write!(
            formatter,
            "{}: {} ",
            "Key".blue(),
            &self.element.cache_key().yellow()
        )?;
        write!(
            formatter,
            "{}: {}",
            "Cached".blue(),
            cached_status(&self.cached)
        )?;
        Ok(())
    }
}

/// For a detailed and verbose implementation of Display for elements
pub struct ElementDisplayLong<'a> {
    element: ElementRef,
    _project: &'a Project,
    cached: Option<BuildResult>,
}

impl<'a> LifetimedDisplay<'a> for ElementDisplayLong<'a> {
    type Output = ElementDisplayLong<'a>;

    fn new(element: ElementRef, project: &'a Project, cached: Option<BuildResult>) -> Self {
        Self {
            element,
            _project: project,
            cached,
        }
    }
}

impl fmt::Display for ElementDisplayLong<'_> {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            formatter,
            "- {}: {: <20}\t",
            "Element".blue(),
            &self.element.element.name.yellow()
        )?;
        writeln!(
            formatter,
            "  {}: {:10}\t",
            "Kind".blue(),
            &self.element.get_kind().yellow()
        )?;
        writeln!(
            formatter,
            "  {}: {} ",
            "Key w".blue(),
            &self.element.weak_cache_key().yellow()
        )?;
        writeln!(
            formatter,
            "  {}: {} ",
            "Key s".blue(),
            &self.element.cache_key().yellow()
        )?;
        writeln!(
            formatter,
            "  {}: {} ",
            "Cached".blue(),
            cached_status(&self.cached).to_string().yellow()
        )?;
        if let Some(result) = &self.cached {
            if let Some(res) = result.get_result_digest() {
                writeln!(
                    formatter,
                    "  {}: {}",
                    "Result Digest".blue(),
                    format!("{}/{}", res.hash, res.size_bytes).yellow(),
                )?;
            }
        }
        let mut deps = self.element.get_all_deps().peekable();
        if deps.peek().is_some() {
            writeln!(formatter, "  {}:", "Depends".blue())?;
        }
        for dep in deps {
            writeln!(
                formatter,
                "  - {}: {}",
                "Name".blue(),
                dep.get_name().yellow()
            )?;
            writeln!(
                formatter,
                "    {}: {}",
                "Build-time".blue(),
                dep.is_buildtime().to_string().yellow()
            )?;
            writeln!(
                formatter,
                "    {}: {}",
                "Run-time".blue(),
                dep.is_runtime().to_string().yellow()
            )?;
        }
        let sources = self.element.get_sources();
        if !sources.is_empty() {
            writeln!(formatter, "  {}:", "sources".blue())?;
        }
        for source in sources {
            writeln!(
                formatter,
                "  - {}: {}",
                "Kind".blue(),
                source.kind().yellow()
            )?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::{
        collections::HashMap,
        fs::{self, File},
        io::Write,
        sync::Arc,
    };

    use anyhow::Ok;
    use reapi::build::bazel::remote::execution::v2::{Directory, DirectoryNode, FileNode};
    use reapi_tools::{
        cas_fs::{digest_from_directory, directory_from_digest},
        fs_io::upload_dir,
        grpc_io::{digest_from_string, upload_blob},
        test_infra::YabaTestSetup,
        virtual_fs::VirtualDir,
        LocalConfig,
    };
    use serde_either::StringOrStruct;

    use crate::{
        project::{
            element::{Dependence, InputDependency, InputElement, JoinType, StageType, Variables},
            plugin::SourcePlugin,
            project::Project,
            ConfigWhen,
        },
        Crapshoot,
    };

    use super::{BuildType, ElementRef};

    async fn build_element_stack(
        base_key: String,
        first_dep: BuildType,
        second_dep: StageType,
    ) -> anyhow::Result<ElementRef> {
        let mut project = Project::for_test();

        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            None,
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([(base_key, "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let base_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("basic cache key {}", base_element.element.cache_key);

        let element_name = "SecondElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "BaseElement".to_string(),
                Some(first_dep),
                None,
                None,
                None,
            ))]),
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([("A".to_string(), "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let second_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("second cache key {}", second_element.element.cache_key);

        let element_name = "TopElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "SecondElement".to_string(),
                None,
                None,
                Some(second_dep),
                None,
            ))]),
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([("A".to_string(), "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let top_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("Top cache key {}", top_element.element.cache_key);

        Ok(top_element)
    }

    #[tokio::test]
    async fn test_element_key_run() -> Crapshoot {
        // The intermediate elements cache key is the same for all 3 as the first two perams are constant
        // there for whether the element is added from adding a Build or Run dep the cache key is the same
        // It only changes when we dont stage ether.
        let build =
            build_element_stack("A".to_string(), BuildType::BuildAndRun, StageType::Build).await?;
        let run =
            build_element_stack("A".to_string(), BuildType::BuildAndRun, StageType::Run).await?;
        let none =
            build_element_stack("A".to_string(), BuildType::BuildAndRun, StageType::None).await?;

        assert!(build
            .element
            .cache_key
            .contains(&run.element.cache_key.to_string()));
        assert!(!build
            .element
            .cache_key
            .contains(&none.element.cache_key.to_string()));

        Ok(())
    }

    #[tokio::test]
    async fn test_element_key_build() -> Crapshoot {
        // The intermediate elements cache key is the same for all 3 as the first two perams are constant
        // But because the base element is only a build dep the only cache key that changes is when we stage
        // the build dep
        let build =
            build_element_stack("A".to_string(), BuildType::Build, StageType::Build).await?;
        let run = build_element_stack("A".to_string(), BuildType::Build, StageType::Run).await?;
        let none = build_element_stack("A".to_string(), BuildType::Build, StageType::None).await?;

        assert!(none
            .element
            .cache_key
            .contains(&run.element.cache_key.to_string()));
        assert!(!build
            .element
            .cache_key
            .contains(&none.element.cache_key.to_string()));

        Ok(())
    }

    #[tokio::test]
    async fn test_element_key_transitive() -> Crapshoot {
        // if we change a run time dep that is not stage then we dont change the cache key!
        let rna = build_element_stack("A".to_string(), BuildType::Run, StageType::None).await?;
        let rnv = build_element_stack("V".to_string(), BuildType::Run, StageType::None).await?;
        // but if we stage it directly we do even tho the intermdiate cache key did not change
        let rra = build_element_stack("A".to_string(), BuildType::Run, StageType::Run).await?;
        let rrv = build_element_stack("V".to_string(), BuildType::Run, StageType::Run).await?;
        // if the build dep changed then we change the top cache key even tho the changed element was not staged
        let bna = build_element_stack("A".to_string(), BuildType::Build, StageType::None).await?;
        let bnv = build_element_stack("V".to_string(), BuildType::Build, StageType::None).await?;

        assert!(rna
            .element
            .cache_key
            .contains(&rnv.element.cache_key.to_string()));
        assert!(!rra
            .element
            .cache_key
            .contains(&rrv.element.cache_key.to_string()));
        assert!(!bna
            .element
            .cache_key
            .contains(&bnv.element.cache_key.to_string()));

        Ok(())
    }

    async fn source_element_stack(
        source_key: String,
        source_dep: BuildType,
    ) -> anyhow::Result<ElementRef> {
        let mut project = Project::for_test();

        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            None,
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([(source_key, "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let base_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("basic cache key {}", base_element.element.cache_key);

        let element_name = "SecondElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                "BaseElement".to_string(),
                Some(source_dep),
                None,
                None,
                None,
            ))]),
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([("A".to_string(), "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let second_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("second cache key {}", second_element.element.cache_key);

        let source_name = "test_source".to_string();
        let source_plugin =
            SourcePlugin::for_test(source_name.clone(), vec!["SecondElement".to_string()]);
        project.add_test_source_plugin(source_name.clone(), source_plugin);

        let element_name = "TopElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![HashMap::from([("kind".to_string(), source_name)])],
            None,
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([("A".to_string(), "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let top_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("Top cache key {}", top_element.element.cache_key);

        Ok(top_element)
    }

    #[tokio::test]
    async fn test_element_key_source() -> Crapshoot {
        let build = source_element_stack("A".to_string(), BuildType::Build).await?;
        let run = source_element_stack("A".to_string(), BuildType::Run).await?;
        let both = source_element_stack("A".to_string(), BuildType::BuildAndRun).await?;

        assert!(!both
            .element
            .cache_key
            .contains(&run.element.cache_key.to_string()));
        assert!(!build
            .element
            .cache_key
            .contains(&both.element.cache_key.to_string()));
        assert!(!build
            .element
            .cache_key
            .contains(&run.element.cache_key.to_string()));
        Ok(())
    }

    #[tokio::test]
    async fn test_element_key_source_eff_run() -> Crapshoot {
        // This shows that changing a sources dependencies run time dep the element using the source
        // even though the element the source depends on has not changed its own cache key
        let run_a = source_element_stack("A".to_string(), BuildType::Run).await?;
        let run_b = source_element_stack("B".to_string(), BuildType::Run).await?;

        assert!(!run_a
            .element
            .cache_key
            .contains(&run_b.element.cache_key.to_string()));
        let mid_a = run_a.element.depends.first().unwrap().element.cache_key();
        let mid_b = run_b.element.depends.first().unwrap().element.cache_key();
        assert!(mid_a.contains(mid_b));

        Ok(())
    }

    #[tokio::test]
    async fn test_element_key_source_eff() -> Crapshoot {
        // This shows that changing a sources dependency even through its dep changes the element using the source
        let run_a = source_element_stack("A".to_string(), BuildType::Build).await?;
        let run_b = source_element_stack("B".to_string(), BuildType::Build).await?;

        assert!(!run_a
            .element
            .cache_key
            .contains(&run_b.element.cache_key.to_string()));
        let mid_a = run_a.element.depends.first().unwrap().element.cache_key();
        let mid_b = run_b.element.depends.first().unwrap().element.cache_key();
        assert!(!mid_a.contains(mid_b));

        Ok(())
    }

    async fn element_stack(
        new_element_name: String,
        previous_element_name: String,
        deps_type: BuildType,
        when: Option<ConfigWhen>,
        project: &mut Project,
    ) -> anyhow::Result<ElementRef> {
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                previous_element_name,
                Some(deps_type),
                None,
                None,
                when,
            ))]),
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([("A".to_string(), "B".to_string())]),
            )),
        );
        project.add_serde_test_element(new_element_name.clone(), input_element);
        let second_element = project
            .get_element(&Arc::from(new_element_name.as_str()))
            .await?;
        println!("next cache key {}", second_element.element.cache_key);

        Ok(second_element)
    }

    async fn tall_element_stack(
        source_key: String,
        deps_type: BuildType,
    ) -> anyhow::Result<ElementRef> {
        let mut project = Project::for_test();

        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            None,
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([(source_key, "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let base_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("basic cache key {}", base_element.element.cache_key);

        let mut last_element = element_name.clone();
        let mut next_element = None;
        for new_element in [
            "level1".to_string(),
            "level2".to_string(),
            "level3".to_string(),
            "level4".to_string(),
            "level5".to_string(),
        ] {
            next_element = Some(
                element_stack(
                    new_element.clone(),
                    last_element.clone(),
                    deps_type.clone(),
                    None,
                    &mut project,
                )
                .await?,
            );
            last_element = new_element;
        }
        let top_element = next_element.unwrap();
        println!("Top cache key {}", top_element.element.cache_key);

        Ok(top_element)
    }

    #[tokio::test]
    async fn test_tall_element_stack() -> Crapshoot {
        let build_a = tall_element_stack("A".to_string(), BuildType::BuildAndRun).await?;
        let build_b = tall_element_stack("B".to_string(), BuildType::BuildAndRun).await?;

        assert!(!build_a
            .element
            .cache_key
            .contains(&build_b.element.cache_key.to_string()));

        let build_a = tall_element_stack("A".to_string(), BuildType::Run).await?;
        let build_b = tall_element_stack("B".to_string(), BuildType::Run).await?;

        assert!(build_a
            .element
            .cache_key
            .contains(&build_b.element.cache_key.to_string()));

        Ok(())
    }

    #[tokio::test]
    async fn test_element_deps() -> Crapshoot {
        let build_a = tall_element_stack("A".to_string(), BuildType::BuildAndRun).await?;

        let all_deps: Vec<Dependence> = build_a.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 1);
        assert_eq!(direct_dep.get_run_deps()?.len(), 4);
        // this produces all the run time deps of the first dep of the element.
        // so top, direct_dep, then len(level3 -> beselement) == 4
        for el in direct_dep.get_run_deps()? {
            println!("a {el:?}");
        }
        let all_run_deps: Vec<ElementRef> = build_a.get_all_run_dep_elements().collect();
        // this produces everything you need to run build_a including its self.
        // so you get 6
        for el in &all_run_deps {
            println!("b {el}");
        }
        assert_eq!(all_run_deps.len(), 6);

        let build_b = tall_element_stack("A".to_string(), BuildType::Run).await?;

        let all_deps: Vec<Dependence> = build_b.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 0);
        assert_eq!(direct_dep.get_run_deps()?.len(), 4);
        let all_run_deps: Vec<ElementRef> = build_b.get_all_run_dep_elements().collect();
        assert_eq!(all_run_deps.len(), 6);

        let build_c = tall_element_stack("A".to_string(), BuildType::Build).await?;

        let all_deps: Vec<Dependence> = build_c.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 1);
        assert_eq!(direct_dep.get_run_deps()?.len(), 0);
        let all_run_deps: Vec<ElementRef> = build_c.get_all_run_dep_elements().collect();
        assert_eq!(all_run_deps.len(), 1);

        Ok(())
    }

    #[tokio::test]
    async fn test_element_deps_build_all() -> Crapshoot {
        let build_a = tall_element_stack("A".to_string(), BuildType::BuildAndRun).await?;

        let all_deps: Vec<Dependence> = build_a.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 1);
        assert_eq!(direct_dep.get_run_deps()?.len(), 4);
        // this produces all the run time deps of the first dep of the element.
        // so top, direct_dep, then len(level3 -> beselement) == 4
        for el in direct_dep.get_run_deps()? {
            println!("a {el:?}");
        }
        let all_run_deps: Vec<ElementRef> = build_a.get_recursive_build_elements().collect();
        // this produces everything you need to run build_a including its self.
        // so you get 6
        for el in &all_run_deps {
            println!("b {el}");
        }
        assert_eq!(all_run_deps.len(), 5);

        let build_b = tall_element_stack("A".to_string(), BuildType::Run).await?;

        let all_deps: Vec<Dependence> = build_b.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 0);
        assert_eq!(direct_dep.get_run_deps()?.len(), 4);
        let all_run_deps: Vec<ElementRef> = build_b.get_recursive_build_elements().collect();
        assert_eq!(all_run_deps.len(), 0);

        let build_c = tall_element_stack("A".to_string(), BuildType::Build).await?;

        let all_deps: Vec<Dependence> = build_c.get_all_deps().cloned().collect();
        assert_eq!(all_deps.len(), 1);
        let direct_dep = all_deps.first().unwrap();
        assert_eq!(direct_dep.get_all_deps().count(), 1);
        assert_eq!(direct_dep.get_build_deps()?.len(), 1);
        assert_eq!(direct_dep.get_run_deps()?.len(), 0);
        let all_run_deps: Vec<ElementRef> = build_c.get_recursive_build_elements().collect();
        assert_eq!(all_run_deps.len(), 1);

        Ok(())
    }

    async fn tall_element_stack_when(
        source_key: String,
        config: HashMap<String, String>,
        when: Option<ConfigWhen>,
    ) -> anyhow::Result<ElementRef> {
        let mut project = Project::for_test_configuration(config);

        let element_name = "BaseElement".to_string();
        let input_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            None,
            vec![],
            None,
            None,
            Some(Variables::from_dic(
                JoinType::Join,
                HashMap::from([(source_key, "B".to_string())]),
            )),
        );
        project.add_serde_test_element(element_name.clone(), input_element);
        let base_element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await?;
        println!("basic cache key {}", base_element.element.cache_key);

        let mut last_element = element_name.clone();
        let mut next_element = None;
        for new_element in [
            "level1".to_string(),
            "level2".to_string(),
            "level3".to_string(),
            "level4".to_string(),
            "level5".to_string(),
        ] {
            let this_when = if new_element == *"level3" {
                when.clone()
            } else {
                None
            };
            next_element = Some(
                element_stack(
                    new_element.clone(),
                    last_element.clone(),
                    BuildType::BuildAndRun,
                    this_when,
                    &mut project,
                )
                .await?,
            );
            last_element = new_element;
        }
        let top_element = next_element.unwrap();
        println!("Top cache key {}", top_element.element.cache_key);

        Ok(top_element)
    }

    #[tokio::test]
    async fn test_tall_element_stack_when() -> Crapshoot {
        let when = Some(ConfigWhen {
            inner: HashMap::from([("extra".to_string(), "include".to_string())]),
        });

        let build_a = tall_element_stack_when(
            "A".to_string(),
            HashMap::from([("extra".to_string(), "".to_string())]),
            when,
        )
        .await?;
        let build_b = tall_element_stack_when(
            "A".to_string(),
            HashMap::from([("extra".to_string(), "include".to_string())]),
            None,
        )
        .await?;

        assert!(!build_a
            .element
            .cache_key
            .contains(&build_b.element.cache_key.to_string()));

        let all_run_deps: Vec<ElementRef> = build_a.get_all_run_dep_elements().collect();
        assert_eq!(all_run_deps.len(), 3);

        let all_run_deps: Vec<ElementRef> = build_b.get_all_run_dep_elements().collect();
        assert_eq!(all_run_deps.len(), 6);

        Ok(())
    }

    #[tokio::test]
    async fn resolve_local_source_file() -> Crapshoot {
        // todo, why is this test SOOOOO slow?
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let tmp_dir_project = tmp_dir_test_path.join("project");
        let files = tmp_dir_project.join("files");
        fs::create_dir_all(&files)?;
        let mut file = File::create(files.join("example.txt"))?;
        file.write_all(b"Hello, world!")?;
        drop(file);
        let project_root = VirtualDir::from(tmp_dir_project);

        let running_yts = yts.run_cas_thread().await;

        let address = running_yts.get_address();

        let config = LocalConfig::new_default(address.to_owned(), "".to_string());

        let mut connection = config.into();

        let mut base_element = InputElement::new_for_test_extra(
            "build_plugin".to_owned(),
            None,
            Vec::from([HashMap::from([
                ("kind".to_string(), "local".to_string()),
                ("location".to_string(), "files".to_string()),
            ])]),
            None,
            None,
            None,
        );
        let project = Project::for_test_with_root(project_root);

        base_element
            .resolve_local_sources(&mut connection, &project)
            .await?;

        println!("base_element {:?}", base_element);

        let source = base_element.raw_input.sources.first().unwrap();
        assert_eq!(source.get("kind"), Some(&"cas".to_string()));
        assert_eq!(source.get("location"), None);
        assert!(source.get("digest").is_some());

        let captured_dir = directory_from_digest(
            &mut connection,
            &digest_from_string(source.get("digest").unwrap())?,
        )
        .await?;
        assert!(captured_dir.directories.is_empty());
        assert_eq!(captured_dir.files.first().unwrap().name, "example.txt");
        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn resolve_local_source_cas() -> Crapshoot {
        // todo, why is this test SOOOOO slow?
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let tmp_dir_project = tmp_dir_test_path.join("project");
        let files = tmp_dir_project.join("files");
        fs::create_dir_all(&files)?;
        let mut file = File::create(files.join("example.txt"))?;
        file.write_all(b"Hello, world!")?;
        drop(file);

        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address();

        let config = LocalConfig::new_default(address.to_owned(), "".to_string());
        let mut connection = config.into();
        let files_cas = upload_dir(&mut connection, &files).await?;

        let root_dir = Directory {
            files: vec![],
            directories: vec![DirectoryNode {
                name: "files".to_string(),
                digest: Some(files_cas),
            }],
            symlinks: vec![],
            node_properties: None,
        };

        let root_cas = digest_from_directory(&mut connection, root_dir).await?;
        let project_root = VirtualDir::from(root_cas);

        let mut base_element = InputElement::new_for_test_extra(
            "build_plugin".to_owned(),
            None,
            Vec::from([HashMap::from([
                ("kind".to_string(), "local".to_string()),
                ("location".to_string(), "files".to_string()),
            ])]),
            None,
            None,
            None,
        );
        let project = Project::for_test_with_root(project_root);

        base_element
            .resolve_local_sources(&mut connection, &project)
            .await?;

        println!("base_element {:?}", base_element);

        let source = base_element.raw_input.sources.first().unwrap();
        assert_eq!(source.get("kind"), Some(&"cas".to_string()));
        assert_eq!(source.get("location"), None);
        assert!(source.get("digest").is_some());

        let captured_dir = directory_from_digest(
            &mut connection,
            &digest_from_string(source.get("digest").unwrap())?,
        )
        .await?;
        assert!(captured_dir.directories.is_empty());
        assert_eq!(captured_dir.files.first().unwrap().name, "example.txt");
        drop(running_yts);
        Ok(())
    }
    #[tokio::test]
    async fn test_local_file() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address();
        let config = LocalConfig::new_default(address.to_owned(), "".to_string());
        let mut connection = config.into();
        let blob = upload_blob(&mut connection, b"Hellow, world!".into()).await?;
        let files = Directory {
            files: vec![FileNode {
                digest: Some(blob.clone()),
                name: "example.txt".to_string(),
                is_executable: false,
                node_properties: None,
            }],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let files_dir = digest_from_directory(&mut connection, files).await?;

        let blob_element = upload_blob(
            &mut connection,
            b"kind: bootstrap_import\nprovides:local\nsournce:\n  kind: local\n  location: ../files".into(),
        )
        .await?;
        let elements = Directory {
            files: vec![FileNode {
                digest: Some(blob_element),
                name: "local.grd".to_string(),
                is_executable: false,
                node_properties: None,
            }],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let elements_dir = digest_from_directory(&mut connection, elements).await?;

        let blob_project = upload_blob(&mut connection, b"elements_path: elements".into()).await?;
        let project = Directory {
            files: vec![FileNode {
                digest: Some(blob_project),
                name: "project.conf".to_string(),
                is_executable: false,
                node_properties: None,
            }],
            directories: vec![DirectoryNode {
                digest: Some(elements_dir),
                name: "elements".to_string(),
            }],
            symlinks: vec![],
            node_properties: None,
        };
        let project_dir = digest_from_directory(&mut connection, project).await?;

        let base = Directory {
            files: vec![],
            directories: vec![
                DirectoryNode {
                    digest: Some(files_dir),
                    name: "files".to_string(),
                },
                DirectoryNode {
                    digest: Some(project_dir),
                    name: "project".to_string(),
                },
            ],
            symlinks: vec![],
            node_properties: None,
        };
        let base_dir = digest_from_directory(&mut connection, base).await?;
        // base/
        //   files/
        //     example.txt
        //   project/
        //     project.conf
        //     elements/
        //       local.grd

        let mut base_root = VirtualDir::from(base_dir);
        let project_root = base_root.get_dir_name(&mut connection, "project").await;
        let mut base_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_owned(),
            None,
            Vec::from([HashMap::from([
                ("kind".to_string(), "local".to_string()),
                ("location".to_string(), "../files".to_string()),
            ])]),
            None,
            None,
            None,
        );

        let project = Project::for_test_with_root(project_root);
        println!("project: {project:?}");

        base_element
            .resolve_local_sources(&mut connection, &project)
            .await?;

        println!("base_element {:?}", base_element);

        let source = base_element.raw_input.sources.first().unwrap();
        assert_eq!(source.get("kind"), Some(&"cas".to_string()));
        assert_eq!(source.get("location"), None);
        assert!(source.get("digest").is_some());

        let captured_dir = directory_from_digest(
            &mut connection,
            &digest_from_string(source.get("digest").unwrap())?,
        )
        .await?;
        assert!(captured_dir.directories.is_empty());
        let file_node = captured_dir.files.first().unwrap();
        assert_eq!(file_node.name, "example.txt");
        assert_eq!(file_node.digest, Some(blob));
        drop(running_yts);
        Ok(())
    }
}
