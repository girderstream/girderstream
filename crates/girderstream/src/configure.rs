// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This file contains code to load in the various places that
//! girderstream gets its configure-ation
use clap::{Parser, Subcommand};
use itertools::Itertools;
use reapi_tools::RemoteCacheConfig;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    env,
    path::{Path, PathBuf},
    str,
    sync::Arc,
};
use tokio::{fs::File, io::AsyncReadExt};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Cli {
    /// Specify the url of the local cas endpoint
    #[clap(long)]
    endpoint: Option<String>,
    /// Specify the instance of the local cas
    #[clap(long)]
    instance: Option<String>,
    /// Specify the program to use as a local executor
    #[clap(long)]
    executor: Option<String>,
    /// Specify the location of a cas for internal exection
    #[clap(long)]
    cas_location: Option<String>,
    /// Specify the project root to load from, defaults to pwd if non given
    #[clap(long)]
    project_root: Option<String>,
    /// Flag to turn on verbose output
    #[clap(short, long)]
    verbose: bool,
    /// Allow for specifying options to use
    ///
    /// Both the option name and value must be options given in the project.conf eg:
    /// --options=opt1=value
    #[clap(long)]
    options: Option<Vec<String>>,
    /// The command to run on this invocation of Girderstream
    ///
    /// The options must a valid `Command` enum option, eg build, checkout etc
    #[clap(subcommand)]
    command: Command,
}

impl Cli {
    /// Parse the cli and turn it in to a nice hash map
    pub fn get_options(&self) -> HashMap<String, String> {
        let mut output_map = HashMap::new();
        if let Some(options) = &self.options {
            for option in options {
                if let Some((option_name, option_value)) = option.split('=').collect_tuple() {
                    output_map.insert(option_name.to_string(), option_value.to_string());
                } else {
                    panic!("Could not parse cli options {option}")
                };
            }
        };
        output_map
    }

    /// Get the way to run girderstream
    pub fn get_command(&self) -> Command {
        self.command.clone()
    }

    /// Get weather or not to run in verbose mode
    pub fn get_verbose(&self) -> bool {
        self.verbose
    }

    /// Get if a override for the local cas has been given
    pub fn get_endpoint(&self) -> Option<&String> {
        self.endpoint.as_ref()
    }

    pub fn get_local_exec(&self) -> Option<&String> {
        // blow up if local cas
        self.executor.as_ref()
    }
    pub fn get_cas_location(&self) -> Option<&String> {
        // blow up if local exec
        self.cas_location.as_ref()
    }

    /// Get the directory that contains the Girderstream project
    pub fn get_root(&self) -> anyhow::Result<PathBuf> {
        Ok(if let Some(root) = &self.project_root {
            PathBuf::from(root.clone())
        } else {
            env::current_dir()?
        })
    }
}

/// The cli details for a element
///
/// This is used for several commands that act on or get information about a element
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about=None)]
pub struct ElementCli {
    #[clap(long)]
    name: String,
    #[clap(long)]
    force: Option<bool>,
}

impl ElementCli {
    /// Get the name of the element given on the cli
    pub fn get_name(&self) -> String {
        self.name.clone()
    }

    /// Get the name of the element given on the cli
    pub fn get_name_arc(&self) -> Arc<str> {
        Arc::from(self.name.as_str())
    }

    /// Get the name of the element given on the cli
    pub fn force(&self) -> Option<bool> {
        self.force
    }
}

/// The cli details for a shell
///
/// This is used for starting a interactive shell
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about=None)]
pub struct ShellCli {
    #[clap(long)]
    name: String,
}

impl ShellCli {
    /// Get the name of the element given on the cli
    pub fn get_name(&self) -> String {
        self.name.clone()
    }
    /// Get the name of the element given on the cli
    pub fn get_name_arc(&self) -> Arc<str> {
        Arc::from(self.name.as_str())
    }
}

/// The cli details needed for a checkout
///
/// The checkout command needs to know not only the element to be checked out
/// but also the location on the local files system to check it out.
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about=None)]
pub struct CheckoutCli {
    #[clap(long)]
    /// Name of the element to be checked out
    name: String,
    #[clap(long)]
    /// Path on the fs to checkout the element to
    location: String,
    #[clap(long)]
    /// Whether or not to fix absolute link targets
    fix_abs_links: Option<bool>,
    #[clap(long)]
    /// Whether or not to checkout run deps as well
    run_deps: Option<bool>,
}

impl CheckoutCli {
    /// Get the name of the element to checkout
    pub fn get_name(&self) -> &String {
        &self.name
    }

    /// Get the name of the element given on the cli
    pub fn get_name_arc(&self) -> Arc<str> {
        Arc::from(self.name.as_str())
    }

    /// Get the path to checkout the element too
    pub fn get_location(&self) -> &String {
        &self.location
    }

    /// Update absolute links to point into the checkout root
    ///
    /// This fixes broken links within the checkout but means the
    /// checkout can not be used for chroots or importing into docker
    /// images etc.
    pub fn get_fix_links(&self) -> bool {
        self.fix_abs_links.unwrap_or(false)
    }

    /// Indicate if the run time deps should also be checked out
    pub fn checkout_run_deps(&self) -> bool {
        self.run_deps.unwrap_or(false)
    }
}

#[derive(Subcommand, Debug, Clone)]
pub enum Command {
    /// Trigger a Girderstream build of a given element
    Build(ElementCli),
    /// Show a given Girderstream element and all of the information about it.
    Show(ElementCli),
    /// Shell into a given element
    Shell(ShellCli),
    /// Checkout a given elements output to the local file system.
    Checkout(CheckoutCli),
    /// Push a given element to the remote caches
    ///
    /// Generally this should not be needed but if a push failed or you have added
    /// a new cache since the build then this is needed to ensure that the new
    /// cache is populated.
    Push(ElementCli),
    /// Export logs
    ExportLogs(CheckoutCli),
    /// Track a given element to update its source to the latest version
    Track(ElementCli),
}

pub fn get_cli() -> Cli {
    Cli::parse()
}

/// The user configuration
///
/// This holds defaults that a user always wants to be true
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct UserConfig {
    #[serde(rename = "force-rebuild")]
    force_rebuild: Option<bool>,
    remotes: Option<Vec<RemoteCacheConfig>>,
    local_cas: Option<String>,
    local_exec: Option<String>,
}

impl UserConfig {
    /// Create a remote cas' from a remote cas configuration file
    pub async fn open_from_file(config_path: &Path) -> Self {
        let mut buff = vec![];

        if let Ok(mut file) = File::open(&config_path).await {
            file.read_to_end(&mut buff).await.unwrap();
        } else {
            return Self {
                force_rebuild: None,
                remotes: None,
                local_cas: None,
                local_exec: None,
            };
        };

        let s = std::str::from_utf8(&buff).unwrap();

        let new: Self = serde_yaml::from_str(s).expect("Could parse yaml");
        new
    }
    /// Get all the remote cache configs for the project
    pub fn get_force(&self) -> Option<bool> {
        self.force_rebuild
    }
    /// Get all the remote cache configs for the project
    pub fn get_remotes(&self) -> impl Iterator<Item = &RemoteCacheConfig> + '_ {
        self.remotes.iter().flatten()
    }

    pub fn get_local_cas(&self) -> Option<&String> {
        self.local_cas.as_ref()
    }
    pub fn get_local_exec(&self) -> Option<&String> {
        self.local_exec.as_ref()
    }
}
