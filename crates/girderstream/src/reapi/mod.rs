// Copyright 2022-2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Push and fetch artifacts from the local cache to the remote cache

use reapi_tools::{Crapshoot, ReapiConnection};

use crate::project::{element::ElementRef, project::Project};

use crate::BuildResult;
use reapi_tools::{
    grpc_io::{fetch_asset, fetch_blob, push_asset, upload_blob},
    remote::{from_tree_to_list, push_blobs},
};

/// Add a build result for a element to the cache
pub(crate) async fn push_cached_build(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    _project: &Project,
    element_ref: &ElementRef,
) -> Crapshoot {
    println!("fetch");
    let asset_key = element_ref.build_asset_key();

    let build_asset_digest = fetch_asset(connection_local, asset_key.to_string()).await?;
    let build_asset_blob = fetch_blob(connection_local, &build_asset_digest).await?;
    let encoded_result = bincode::deserialize::<BuildResult>(&build_asset_blob)?;
    let result_digest = encoded_result.get_result_digest();
    println!("got cached blob");

    let mut dirs = from_tree_to_list(
        connection_local,
        result_digest.ok_or(anyhow::anyhow!("missing directory blob"))?,
    )
    .await?;
    dirs.push(result_digest.unwrap().clone());
    println!("got the dirs to push");
    let reference_directories = dirs.clone();

    push_blobs(connection_local, connection_remote, dirs.clone()).await?;

    push_asset(
        connection_remote,
        &asset_key.to_string(),
        build_asset_digest,
        reference_directories,
    )
    .await?;
    let element_name = element_ref.get_name();
    println!("Pushed {element_name} to asset cache");

    Ok(())
}

/// Add a build result for a element to the cache
pub(crate) async fn pull_cached_build(
    connection_to: &mut ReapiConnection,
    connection_from: &mut ReapiConnection,
    _project: &Project,
    element_ref: &ElementRef,
) -> Crapshoot {
    println!("trying to pull remote build for {}", element_ref.get_name());
    let asset_key = element_ref.build_asset_key();

    let build_asset_digest = fetch_asset(connection_from, asset_key.to_string()).await?;
    println!("remote has asset, pulling");

    let build_asset_blob = fetch_blob(connection_from, &build_asset_digest).await?;
    println!("remote has asset, pulled");

    let build_result = bincode::deserialize::<BuildResult>(&build_asset_blob).unwrap();

    upload_blob(connection_to, build_asset_blob).await?;

    let reference_directories = build_result.get_ref_blob();

    println!("pull cached cas");
    push_blobs(
        connection_from,
        connection_to,
        reference_directories.clone(),
    )
    .await?;
    println!("pulled cached cas");

    push_asset(
        connection_to,
        &asset_key.to_string(),
        build_asset_digest,
        reference_directories.clone(),
    )
    .await?;
    Ok(())
}
