// Copyright 2022-2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Read and write from the file system to the cas
//!
//! Most of these functions use buildbox-casd to do the actual io.
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::{fs::read_dir, os::unix::fs::PermissionsExt};

use anyhow::{anyhow, bail, Ok};
use async_compression::tokio::bufread::XzDecoder;
use async_recursion::async_recursion;
use reapi::build::bazel::remote::execution::v2::{Digest, Directory, DirectoryNode, FileNode};
use tokio::{
    fs::{self, File},
    io::{AsyncReadExt, BufReader},
};
use tokio_stream::*;
use tokio_tar::{Archive, EntryType};

use crate::{
    actor::{ReapiActions, ReapiConnections},
    cas_fs::directory_from_digest,
    grpc_io::write_blob,
    Crapshoot, ReapiConnection,
};

/// Checkout the build result of a element to the local file system
///
/// This uses buildbox-casd to do the cas to fs logic but as buildbox-casd
/// only does a fuse mount that will be unmounted when it stops we copy
/// the files from the mount to the final location on the local file system.
///
/// This is not great, we could probably do a better job of writing directly
/// from casd to the fs with our own code. But as we already have the recursive
/// function, then this seems like a good option for now.
pub async fn checkout(
    connection: &mut ReapiConnection,
    target: Digest,
    location: &Path,
    fix_abs_links: bool,
) -> Crapshoot {
    if location.is_dir() || location.is_file() || location.is_symlink() {
        anyhow::bail!("Checkout directory must not exists already")
    };
    let parent = location.parent().expect("Could not determine parent");
    if !(parent.is_dir() || parent.is_symlink()) {
        anyhow::bail!("Checkout directory's parent must exists")
    };
    let fix_abs_links = if fix_abs_links { Some(location) } else { None };
    recursive_checkout(connection, target, location, fix_abs_links).await
}

#[async_recursion]
pub async fn recursive_checkout(
    connection: &mut ReapiConnection,
    target: Digest,
    location: &Path,
    fix_abs_links: Option<&'async_recursion Path>,
) -> Crapshoot {
    tokio::fs::create_dir(location).await?;
    let root = directory_from_digest(connection, &target).await?;

    for dir in root.directories {
        let new_dir = location.join(dir.name);
        recursive_checkout(connection, dir.digest.unwrap(), &new_dir, fix_abs_links).await?
    }

    for file in root.files {
        let new_file = location.join(file.name);
        let mut open_file = fs::File::create(&new_file).await?;
        write_blob(
            connection,
            file.digest.as_ref().ok_or(anyhow!("missing file blob"))?,
            &mut open_file,
        )
        .await?;
        if file.is_executable {
            fs::set_permissions(new_file, std::fs::Permissions::from_mode(0o744)).await?;
        }
    }

    for link in root.symlinks {
        let target = Path::new(&link.target);
        let target = if target.is_relative() {
            target.to_path_buf()
        } else if let Some(root_dir) = fix_abs_links {
            // This means that any absolute links work once checked out
            // but if you checkout to use as a chroot etc then you want them
            // to be broken on the checkout so that they work in the chroot..
            root_dir.join(target)
        } else {
            target.to_path_buf()
        };
        let link_path = location.join(link.name);
        fs::symlink(&target, link_path).await?;
    }

    Ok(())
}

#[async_recursion]
pub async fn upload_tar(
    connection: &mut ReapiConnection,
    location: &Path,
) -> anyhow::Result<Digest> {
    let (pool, reapi) =
        ReapiConnections::new_worker_sender(&mut connection.clone(), connection, 5, None).await;
    let result = upload_tar_actions(reapi, location.to_owned()).await;
    drop(pool);
    result
}

/// Open a tar file on disk and up load its contents to the cas
///
/// The current implmenation assumes a very sensible tar layout and can only deal with normal files
/// and standard directories.
#[async_recursion]
pub async fn upload_tar_actions(reapi: ReapiActions, location: PathBuf) -> anyhow::Result<Digest> {
    if !location.is_file() {
        bail!("Upload location must be a tar file")
    }

    let tar_xz = File::open(location).await?;
    let tar = XzDecoder::new(BufReader::new(tar_xz));
    let mut archive = Archive::new(tar);
    let mut entries = archive.entries().unwrap();
    let mut dir_map = HashMap::new();
    let mut file_list = vec![];
    let mut dir_list = vec![];
    while let Some(entry_base) = entries.next().await {
        let mut entry = entry_base.unwrap();
        let path_tmp = PathBuf::from(entry.path().unwrap());
        let path = path_tmp.clone();
        drop(path_tmp);
        let header = entry.header();
        println!("path: {path:?} {:?}", header.entry_type());
        let name = path
            .file_name()
            .unwrap()
            .to_owned()
            .to_str()
            .unwrap()
            .to_string();
        let parent = if let Some(parent) = path.parent() {
            let parent = parent.to_owned().to_str().unwrap().to_string();
            if parent.is_empty() {
                None
            } else {
                Some(parent)
            }
        } else {
            None
        };
        if header.entry_type() == EntryType::Directory {
            let new_dir = Directory {
                files: vec![],
                directories: vec![],
                symlinks: vec![],
                node_properties: None,
            };
            let path = path
                .to_owned()
                .as_os_str()
                .to_string_lossy()
                .strip_suffix("/")
                .unwrap_or(&name)
                .to_owned();
            dir_map.insert(path.clone(), (name, parent, new_dir));
            dir_list.push(path);
        } else if header.entry_type() == EntryType::Regular {
            let metadata = entry.header();
            let this_reapi = reapi.clone();
            let executable: bool = metadata.mode()? & 0o100 == 0o100;
            let mut this_data = vec![];
            entry.read_to_end(&mut this_data).await?;
            let digest_join = this_reapi.upload_blob_send(this_data).await?;
            file_list.push((path, parent, name, executable, digest_join))
        } else if path.is_symlink() {
            todo!()
        } else {
            println!("not a known file directory entry {path:?}");
        }
    }

    for (path, parent, name, executable, digest_join) in file_list {
        println!("{path:?} {parent:?}, {name:?}");
        let file_node = FileNode {
            name,
            digest: Some(digest_join.get_digest().await?),
            is_executable: executable,
            node_properties: None,
        };
        // assume root is a dir
        let (_, _, dir) = dir_map.get_mut(&parent.unwrap()).unwrap();
        dir.files.push(file_node);
    }

    dir_list.reverse();

    let mut top_dir = None;
    for dir in dir_list {
        let (name, parent, cas_dir) = dir_map.remove(&dir).unwrap();
        let digest = Some(reapi.digest_from_directory(cas_dir).await?);
        if let Some(parent) = parent {
            println!("parent {parent:?}");
            let (_, _, dir) = dir_map.get_mut(&parent).unwrap();
            let node = DirectoryNode {
                name: name.to_owned(),
                digest,
            };
            dir.directories.push(node)
        } else {
            top_dir = digest
        }
    }
    Ok(top_dir.unwrap())
}

#[async_recursion]
pub async fn upload_dir(
    connection: &mut ReapiConnection,
    location: &Path,
) -> anyhow::Result<Digest> {
    let (pool, reapi) =
        ReapiConnections::new_worker_sender(&mut connection.clone(), connection, 5, None).await;
    let result = upload_dir_actions(reapi, location.to_owned()).await;
    drop(pool);
    result
}

#[async_recursion]
pub async fn upload_dir_actions(reapi: ReapiActions, location: PathBuf) -> anyhow::Result<Digest> {
    if !location.is_dir() {
        bail!("Upload location must be a directory")
    }
    let mut base_dir = Directory {
        files: vec![],
        directories: vec![],
        symlinks: vec![],
        node_properties: None,
    };

    let mut file_joins = vec![];

    let mut dir_joins = vec![];

    for entry in read_dir(location)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            let node = DirectoryNode {
                name: path.file_name().unwrap().to_str().unwrap().to_string(),
                digest: None,
            };
            let this_reapi = reapi.clone();
            let this_path = path.clone();
            let digest_join =
                tokio::spawn(async move { upload_dir_actions(this_reapi, this_path).await });

            dir_joins.push((node, digest_join));
        } else if path.is_file() {
            let name = path
                .file_name()
                .unwrap()
                .to_owned()
                .to_str()
                .unwrap()
                .to_string();
            let metadata = fs::metadata(&path).await?;
            let this_reapi = reapi.clone();
            let this_path = path.clone();
            let digest_join = tokio::spawn(async move { this_reapi.upload_file(this_path).await });
            let executable: bool = metadata.permissions().mode() & 0o100 == 0o100;
            let file_node = FileNode {
                name,
                digest: None,
                is_executable: executable,
                node_properties: None,
            };
            file_joins.push((file_node, digest_join))
        } else if path.is_symlink() {
            todo!()
        } else {
            println!("not a known file directory entry {entry:?}");
        }
    }
    for file_join in file_joins {
        let (mut node, digest_join) = file_join;
        node.digest = Some(digest_join.await??);
        base_dir.files.push(node);
    }
    for dir_join in dir_joins {
        let (mut node, digest_join) = dir_join;
        node.digest = Some(digest_join.await??);
        base_dir.directories.push(node);
    }

    reapi.digest_from_directory(base_dir).await
}

#[cfg(test)]
mod tests {
    use tokio::fs;

    use crate::{
        cas_fs::{join_digests, offset_digest, place_file_in_dir},
        grpc_io::upload_blob,
        test_infra::YabaTestSetup,
        Crapshoot, LocalConfig,
    };

    use super::checkout;

    #[tokio::test]
    async fn test_checkout_blob() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        let blob_original = "Hello there how lovely to see you".to_string();

        let digest_blob = upload_blob(&mut connection, blob_original.as_bytes().to_vec())
            .await
            .expect("Could not upload blob");
        let mid_digest_dir =
            place_file_in_dir(&mut connection, "example".to_string(), digest_blob, false)
                .await
                .unwrap();
        let digest_dir_with_sub =
            offset_digest(&mut connection, mid_digest_dir.clone(), "sub_folder".into()).await?;

        let digest_dir =
            join_digests(&mut connection, &digest_dir_with_sub, &mid_digest_dir).await?;

        let temp_dir_out = tmp_dir_test_path.to_owned().join("buildbox_hates_dots");

        checkout(&mut connection, digest_dir, &temp_dir_out, false)
            .await
            .unwrap();
        let raw = fs::read(temp_dir_out.join("sub_folder").join("example")).await?;
        let result = String::from_utf8_lossy(&raw);
        assert_eq!(result, blob_original);
        let raw = fs::read(temp_dir_out.join("example")).await?;
        let result = String::from_utf8_lossy(&raw);
        assert_eq!(result, blob_original);

        drop(running_yts);
        Ok(())
    }
}
