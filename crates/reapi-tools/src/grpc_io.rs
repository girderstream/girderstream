// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// REAPI push and pull functions
//
// These are currently a set of single blob read and write to the cas and asset store
// To use REAPI effectively we should also implement bulk operations.
//! REAPI push and pull functions
//!
//! These are currently a set of single blob read and write to
//! the cas and asset store
//!
//! Before implementing functions like artifact checkout or
//! uploading helpers for directories or tars we should add some
//! kind of bulk upload system.
use anyhow::{anyhow, Context};
use async_stream::stream;
use sha2::{Digest as _, Sha256};
use tokio::{fs::File, io::AsyncWriteExt};

use super::ReapiConnection;
use crate::Crapshoot;
use reapi::{
    build::bazel::remote::{
        asset::v1::{FetchBlobRequest, PushBlobRequest},
        execution::v2::{
            batch_update_blobs_request::Request, BatchReadBlobsRequest, BatchUpdateBlobsRequest,
            Digest, FindMissingBlobsRequest,
        },
    },
    google::bytestream::{ReadRequest, WriteRequest},
};

/// Create the string representation of a digest from a `Digest`
pub fn digest_to_string(digest: &Digest) -> String {
    format!("{}/{}", digest.hash, digest.size_bytes)
}

/// Create the Digest represetned by a string in the standard format
///
/// This take a string in the format
///
/// `HASH/1234`
///
/// eg.
/// `e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855/0`
///
/// And returns a result containing a `Digest` if the string is properly
/// formated otherwise returns a error.
pub fn digest_from_string(digest_string: &str) -> anyhow::Result<Digest> {
    let mut raw = digest_string.split('/');
    // TODO: we should add a bit more validation here
    Ok(Digest {
        hash: raw.next().context("string did not have hash")?.to_string(),
        size_bytes: raw
            .next()
            .context("string did not have a size")?
            .parse()
            .unwrap(),
    })
}

pub(crate) async fn move_blobs_between_cas(
    connection_from: &mut ReapiConnection,
    connection_too: &mut ReapiConnection,
    digests: Vec<Digest>,
) -> Crapshoot {
    let batch_request = BatchReadBlobsRequest {
        instance_name: "".to_string(),
        digests: digests.clone(),
        acceptable_compressors: vec![],
    };
    let cas_from = connection_from.get_cas_client().await?;
    let read_blobs = cas_from.batch_read_blobs(batch_request).await?;
    let mut requests = vec![];
    for (response, reference) in read_blobs
        .into_inner()
        .responses
        .into_iter()
        .zip(digests.iter())
    {
        if let Some(status) = response.status {
            if status.code != 0 {
                return Err(anyhow!("did not read a blob"));
            }
        }
        if response.digest.as_ref().unwrap() != reference {
            return Err(anyhow!("read blob does not match"));
        }

        requests.push(Request {
            digest: response.digest,
            data: response.data,
            compressor: 0,
        });
    }
    let batch_request = BatchUpdateBlobsRequest {
        instance_name: "".to_string(),
        requests,
    };
    let cas_too = connection_too.get_cas_client().await?;
    let responses = cas_too
        .batch_update_blobs(batch_request)
        .await?
        .into_inner();
    //todo spell check ^
    for response in responses.responses {
        if response.status.unwrap().code != 0 {
            return Err(anyhow!("did not write a blob"));
        }
    }
    Ok(())
}

/// Move a blob from one cas to another cas
///
/// This is done in chunks so the hole file does not need to be downloaded in one go
/// but note that this does involve pulling down the blob from the `from` and pushing
/// to the `too` cas.
pub(crate) async fn move_blob_between_cas(
    connection_from: &mut ReapiConnection,
    connection_too: &mut ReapiConnection,
    blob: Digest,
) -> Crapshoot {
    let max_pull_size: usize = connection_too.get_blob_size().await? as usize - 1024 * 4;
    let pull_size: usize = max_pull_size.min(blob.size_bytes as usize);

    let name = digest_to_string(&blob);
    let base_read_request = if connection_from.get_instance_name().is_empty() {
        ReadRequest {
            resource_name: format!("blobs/{}", name),
            read_offset: 0,
            read_limit: pull_size as i64,
        }
    } else {
        ReadRequest {
            resource_name: format!("{}/blobs/{}", connection_from.get_instance_name(), name),
            read_offset: 0,
            read_limit: pull_size as i64,
        }
    };

    let base_write_request: WriteRequest = if !connection_too.get_instance_name().is_empty() {
        WriteRequest {
            resource_name: format!(
                "{}/uploads/shortname/blobs/{}",
                connection_too.get_instance_name().clone(),
                name
            ),
            write_offset: 0,
            finish_write: false,
            data: Vec::new(),
        }
    } else {
        WriteRequest {
            resource_name: format!("uploads/shortname/blobs/{}", name),
            write_offset: 0,
            finish_write: false,
            data: Vec::new(),
        }
    };

    let mut reader_client = connection_from.get_bytestream_client().await?.clone();

    let total_size = blob.size_bytes;

    let stream = stream! {
            let mut total_bytes: i64 = 0;
            if loop {
                let mut read_request = base_read_request.clone();
                read_request.read_offset = total_bytes;
                read_request.read_limit = (max_pull_size as i64).min(total_size - total_bytes);
                let mut reader = match reader_client.read(read_request).await {Ok(reader)=>reader,
        Err(err) => {
    println!("err: {err:?}");
                        return;
                    } }
                .into_inner();

                let mut chunk: Vec<u8> = Vec::with_capacity(max_pull_size);
                if let Some(msg) = reader.message().await.unwrap() {
                    chunk.extend_from_slice(&msg.data);
                }

                let mut request = base_write_request.clone();
                request.write_offset = total_bytes;
                total_bytes += chunk.len() as i64;
                request.data = chunk;

                yield request;
                if total_bytes == total_size {
                    break true;
                }
            } {
                let mut request = base_write_request.clone();
                request.write_offset = total_bytes;
                request.finish_write = true;
                yield request;
            }
        };

    let response = connection_too
        .get_bytestream_client()
        .await?
        .write(stream)
        .await?;
    if response.get_ref().committed_size != blob.size_bytes {
        anyhow::bail!("Woah, committed size was wrong");
    }

    Ok(())
}

/// Fetch a blob from the cas
///
/// Fetch a digest from a cas into memory, this will fetch the hole blob
/// so care should be taken as this can use a lot of ram for large blobs.
///
/// [`move_blob_between_cas`] and [`write_blob`] process blobs by chunks so do not
/// have such large ram requirements but are specialized for specific tasks.
pub async fn fetch_blob(
    connection: &mut ReapiConnection,
    digest: &Digest,
) -> anyhow::Result<Vec<u8>> {
    let name = digest_to_string(digest);
    let request = if connection.get_instance_name().is_empty() {
        ReadRequest {
            resource_name: format!("blobs/{}", name),
            read_offset: 0,
            read_limit: 0,
        }
    } else {
        ReadRequest {
            resource_name: format!("{}/blobs/{}", connection.get_instance_name(), name),
            read_offset: 0,
            read_limit: 0,
        }
    };

    let mut reader = connection
        .get_bytestream_client()
        .await?
        .read(request)
        .await?
        .into_inner();

    let mut result: Vec<u8> = vec![];
    while let Some(msg) = reader.message().await? {
        result.extend_from_slice(&msg.data);
    }

    if result.len() != digest.size_bytes as usize {
        anyhow::bail!("Woah, fetched size was wrong");
    }

    Ok(result)
}

/// Write a blob from the cas to a file
///
/// The blob should be given as its string representation
/// of its digest.
pub(crate) async fn write_blob(
    connection: &mut ReapiConnection,
    digest: &Digest,
    file: &mut File,
) -> Crapshoot {
    let name = digest_to_string(digest);
    let request = if connection.get_instance_name().is_empty() {
        ReadRequest {
            resource_name: format!("blobs/{}", name),
            read_offset: 0,
            read_limit: 0,
        }
    } else {
        ReadRequest {
            resource_name: format!("{}/blobs/{}", connection.get_instance_name(), name),
            read_offset: 0,
            read_limit: 0,
        }
    };

    let mut reader = connection
        .get_bytestream_client()
        .await?
        .read(request)
        .await?
        .into_inner();

    let mut total_fetched = 0;
    while let Some(msg) = reader.message().await? {
        file.write_all(&msg.data).await?;
        total_fetched += msg.data.len();
    }

    if total_fetched != digest.size_bytes as usize {
        anyhow::bail!("Woah, fetched size was wrong");
    }

    Ok(())
}

/// Upload a blob to the cas
///
/// Takes a `Vec<u8>` ie `bytes` and uploads them to the cas
///
/// Note that this requires the hole blob be in ram at once so
/// it can be passed to this function. If you are uploading
/// large files we should consider another function that uses
/// the reader trait.
///
/// This file was envisaged for use with functions like those found
/// in the cas_fs section ie, small blobs called infrequently.
/// If that is not how it is being used then its time for us to
/// expand this mod.
pub async fn upload_blob(
    connection: &mut ReapiConnection,
    blob: Vec<u8>,
) -> anyhow::Result<Digest> {
    let max_size: usize = connection.get_blob_size().await? as usize - 1024;
    let flen = blob.len();
    let (blob, sha) = if flen > 1024 * 50 {
        let (blob, sha) = tokio::task::spawn_blocking(move || {
            let mut dig = Sha256::new();
            dig.update(&blob);
            let sha = format!("{:x}", dig.finalize());
            (blob, sha)
        })
        .await?;
        (blob, sha)
    } else {
        let mut dig = Sha256::new();
        dig.update(&blob);
        let sha = format!("{:x}", dig.finalize());
        (blob, sha)
    };
    // Check if the blob already exists and skip.
    // This may only be a speed up for sufficently large files
    // so maybe gate on some logic
    if flen > 1024 * 10 {
        let this_digets = Digest {
            hash: sha.clone(),
            size_bytes: flen as i64,
        };
        let fmbr = FindMissingBlobsRequest {
            instance_name: "".to_string(),
            blob_digests: vec![this_digets.clone()],
        };
        let fmbres = connection
            .get_cas_client()
            .await?
            .find_missing_blobs(fmbr)
            .await?;
        let (meta_map, res, _) = fmbres.into_parts();
        if meta_map.into_headers().get("grpc-status").unwrap() == "0"
            && res.missing_blob_digests.is_empty()
        {
            return Ok(this_digets);
        }
    }

    let base_request = if !connection.get_instance_name().is_empty() {
        WriteRequest {
            resource_name: format!(
                "{}/uploads/shortname/blobs/{}/{}",
                connection.get_instance_name().clone(),
                sha,
                flen
            ),
            write_offset: 0,
            finish_write: false,
            data: Vec::new(),
        }
    } else {
        WriteRequest {
            resource_name: format!("uploads/shortname/blobs/{}/{}", sha, flen),
            write_offset: 0,
            finish_write: false,
            data: Vec::new(),
        }
    };

    let mut buffer = Vec::with_capacity(max_size);

    let stream = stream! {
        let mut total_bytes: i64 = 0;
        if loop {
            buffer.resize(max_size, 0u8);
            let byte_count = (max_size).min(blob.len() - total_bytes as usize);
            //std::mem::swap(buffer[..], blob[total_bytes as usize ..(total_bytes as usize + bytecount)]);
            // this seems fairly stupid
            buffer[..byte_count].clone_from_slice(&blob[total_bytes as usize ..(total_bytes as usize + byte_count)]);
            buffer.resize(byte_count, 0);
            let mut request = base_request.clone();
            request.write_offset = total_bytes;
            request.data = std::mem::replace(&mut buffer, Vec::with_capacity(max_size));
            yield request;
            total_bytes += byte_count as i64;
            if byte_count != max_size {
                break true;
            }
        } {
            let mut request = base_request.clone();
            request.write_offset = total_bytes;
            request.finish_write = true;
            yield request;
        }
    };

    let response = connection
        .get_bytestream_client()
        .await?
        .write(stream)
        .await?;
    if response.get_ref().committed_size != flen as i64 {
        anyhow::bail!("Woah, committed size was wrong");
    }

    Ok(Digest {
        hash: sha,
        size_bytes: flen as i64,
    })
}

/// Upload a existing asset
///
/// This takes the given asset_key and combines it with a
/// girderstream specific string.
// Todo: add some kind of check?
// Todo: investigate if the other optional parts of the Request
//       would be sensible to be used.
pub async fn push_asset(
    connection: &mut ReapiConnection,
    asset_key: &String,
    asset_digest: Digest,
    references_directories: Vec<Digest>,
) -> anyhow::Result<()> {
    let request = PushBlobRequest {
        blob_digest: Some(asset_digest),
        expire_at: None,
        instance_name: "".to_string(),
        qualifiers: vec![],
        references_blobs: vec![],
        references_directories,
        uris: vec![format!(
            "urn:fdc:girderstream.build:2020:artifact:{}",
            asset_key
        )],
    };

    let _response = connection
        .get_asset_push_client()
        .await?
        .push_blob(request)
        .await?;
    Ok(())
}

/// Upload a existing asset
///
/// This takes the given asset_key combines it with a
/// girderstream specific string. And then returns any
/// found assets matching the combined key.
///
/// There is not a easy way for consumers of this function
/// to know if a general GRPC error accured or if the asset
/// was just not there. This should be rectified in the future.
// Todo: Fix the above
pub async fn fetch_asset(
    connection: &mut ReapiConnection,
    asset_key: String,
) -> anyhow::Result<Digest> {
    let request = FetchBlobRequest {
        instance_name: "".to_string(),
        qualifiers: vec![],
        uris: vec![format!(
            "urn:fdc:girderstream.build:2020:artifact:{}",
            asset_key
        )],
        oldest_content_accepted: None,
        timeout: None,
    };

    let response = connection
        .get_asset_fetch_client()
        .await?
        .fetch_blob(request)
        .await?;
    let resulting_digest = response.into_inner().blob_digest.unwrap();

    Ok(resulting_digest)
}

#[cfg(test)]
mod tests {
    use sha2::{Digest, Sha256};
    use tempfile::tempdir;
    use tokio::{fs::File, io::AsyncReadExt};

    use crate::{
        grpc_io::{
            fetch_blob, move_blob_between_cas, move_blobs_between_cas, upload_blob, write_blob,
        },
        test_infra::YabaTestSetup,
        Crapshoot, LocalConfig,
    };

    #[tokio::test]
    async fn test_push_pull_blob() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();
        let blob_original: Vec<u8> = (0..200).collect();

        let upload_digest = upload_blob(&mut connection, blob_original.clone())
            .await
            .expect("Could not upload blob");

        assert_eq!(upload_digest.size_bytes as usize, blob_original.len());
        let mut dig = Sha256::new();
        dig.update(&blob_original);
        let digest = format!("{:x}", dig.finalize());
        assert_eq!(upload_digest.hash, digest);

        let fetched_blob = fetch_blob(&mut connection, &upload_digest)
            .await
            .expect("Could not upload blob");

        assert_eq!(blob_original, fetched_blob);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_move_blob() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection_a = config.into();

        let yts_b = YabaTestSetup::setup_server().await?;
        let running_yts_b = yts_b.run_cas_thread().await;
        let address_b = running_yts_b.get_address().to_owned();

        let config = LocalConfig::new_yaba(address_b, "".to_string(), tmp_dir_test_path.into());
        let mut connection_b = config.into();

        // less than one pull chunk
        let blob_original_small: Vec<u8> = (0..255).collect::<Vec<u8>>().repeat(10);

        // many more than one pull chunk
        let blob_original_big: Vec<u8> = (0..255).collect::<Vec<u8>>().repeat(300_000);

        let upload_digest_small = upload_blob(&mut connection_a, blob_original_small.clone())
            .await
            .expect("Could not upload blob");
        println!("big ish");
        let upload_digest_big = upload_blob(&mut connection_a, blob_original_big.clone())
            .await
            .expect("Could not upload blob");
        println!("big big");

        assert_eq!(
            upload_digest_small.size_bytes as usize,
            blob_original_small.len()
        );
        let mut dig = Sha256::new();
        dig.update(&blob_original_small);
        let digest = format!("{:x}", dig.finalize());
        assert_eq!(upload_digest_small.hash, digest);

        assert_eq!(
            upload_digest_big.size_bytes as usize,
            blob_original_big.len()
        );
        let mut dig = Sha256::new();
        dig.update(&blob_original_big);
        let digest = format!("{:x}", dig.finalize());
        assert_eq!(upload_digest_big.hash, digest);

        println!("big ish move");
        move_blob_between_cas(
            &mut connection_a,
            &mut connection_b,
            upload_digest_small.clone(),
        )
        .await?;
        println!("big move");
        move_blob_between_cas(
            &mut connection_a,
            &mut connection_b,
            upload_digest_big.clone(),
        )
        .await?;
        println!("big move done");

        let fetched_blob = fetch_blob(&mut connection_b, &upload_digest_small)
            .await
            .expect("Could not upload blob");

        assert_eq!(blob_original_small, fetched_blob);

        let fetched_blob = fetch_blob(&mut connection_b, &upload_digest_big)
            .await
            .expect("Could not upload blob");

        assert_eq!(blob_original_big, fetched_blob);

        drop(running_yts);
        drop(running_yts_b);
        Ok(())
    }

    #[tokio::test]
    async fn test_write_blob() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        let blob_original: Vec<u8> = (0..255).collect::<Vec<u8>>().repeat(30_000);

        let upload_digest = upload_blob(&mut connection, blob_original.clone())
            .await
            .expect("Could not upload blob");

        assert_eq!(upload_digest.size_bytes as usize, blob_original.len());
        let mut dig = Sha256::new();
        dig.update(&blob_original);
        let digest = format!("{:x}", dig.finalize());
        assert_eq!(upload_digest.hash, digest);

        let tmp_dir_test = tempdir()?;
        let tmp_dir_test_path = tmp_dir_test.path();
        let tmp_dir_cas_path = tmp_dir_test_path.join("download.blob");
        let tmp_dir_str = format!("{}", tmp_dir_cas_path.display());

        let mut open_file = File::create(&tmp_dir_str).await?;

        write_blob(&mut connection, &upload_digest, &mut open_file)
            .await
            .expect("Could not upload blob");

        drop(open_file);

        let mut re_open_file = File::open(&tmp_dir_str).await?;

        let mut fetched_blob: Vec<u8> = vec![];
        re_open_file.read_to_end(&mut fetched_blob).await?;

        assert_eq!(blob_original, fetched_blob);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_move_blobs() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection_a = config.into();

        let yts_b = YabaTestSetup::setup_server().await?;
        let running_yts_b = yts_b.run_cas_thread().await;
        let address_b = running_yts_b.get_address().to_owned();

        let config = LocalConfig::new_yaba(address_b, "".to_string(), tmp_dir_test_path.into());
        let mut connection_b = config.into();

        let mut digests = vec![];
        for iii in 0..100 {
            let mut data = vec![];
            data.extend(0..iii);
            digests.push(upload_blob(&mut connection_a, data).await?);
        }

        move_blobs_between_cas(&mut connection_a, &mut connection_b, digests.clone()).await?;

        for digest in digests {
            fetch_blob(&mut connection_b, &digest).await?;
        }

        drop(running_yts);
        drop(running_yts_b);
        Ok(())
    }
}
