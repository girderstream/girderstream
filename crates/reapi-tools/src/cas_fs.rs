// Copyright 2022-2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Virtual directory helper functions
//!
//! This module provides a set of sequenceal file operations
//! that let you manipulate virtual files and directories
//! these are all pub(crate) and are not meant as general
//! purpose libs to be reused in there own write.
//!
//! If you would like to re-use them for other purposes then
//! I think they would be best placed in a separate crate but
//! for the sake of development it is much easer for them to
//! be here. That said i anticipate clear api boundaries and
//! the functions being fairly stable so they would be easy to
//! move into there own crate.
use super::{
    grpc_io::{fetch_blob, upload_blob},
    ReapiConnection,
};
use crate::Crapshoot;
use async_recursion::async_recursion;
use prost::Message;
use reapi::build::bazel::remote::execution::v2::{
    Digest, Directory, DirectoryNode, FileNode, FindMissingBlobsRequest, NodeProperties,
    SymlinkNode, Tree,
};
use std::{
    collections::BTreeMap,
    fmt,
    iter::Rev,
    path::{Component, Components, PathBuf},
};

/// Take the digest of a file and create a directory containing that file
///
/// The returned Digest will point to a directory containing a single
/// file named by `file_name` containing the content pointed to in `file_digest`
pub async fn place_file_in_dir(
    connection: &mut ReapiConnection,
    file_name: String,
    file_digest: Digest,
    executable: bool,
) -> anyhow::Result<Digest> {
    let file_node = FileNode {
        name: file_name,
        digest: Some(file_digest),
        is_executable: executable,
        node_properties: None,
    };

    let new_dir = Directory {
        files: vec![file_node],
        directories: vec![],
        symlinks: vec![],
        // todo
        node_properties: Some(NodeProperties {
            properties: vec![],
            mtime: None,
            unix_mode: None,
        }),
    };
    digest_from_directory(connection, new_dir).await
}

/// Place a directory into another directory
///
/// The function takes the Digest of a directory and places it into
/// a new directory.
///
/// Eg if you with to place a file in a sub folder  of a root you could
/// take the file digest and use `place_file_in_dir` and then use this
/// function to offset that root containing the file into a named
/// subdirectory with this function.
pub async fn offset_digest(
    connection: &mut ReapiConnection,
    directory: Digest,
    offset: PathBuf,
) -> anyhow::Result<Digest> {
    offset_digest_iter(connection, directory, offset.components().rev()).await
}

// This function should not be called directly from the rest of the crate.
// use `offset_digest` instead.
#[async_recursion]
async fn offset_digest_iter(
    connection: &mut ReapiConnection,
    directory: Digest,
    mut offset: Rev<Components<'async_recursion>>,
) -> anyhow::Result<Digest> {
    if let Some(Component::Normal(parent_name)) = offset.next() {
        let new_node = DirectoryNode {
            name: parent_name.to_string_lossy().to_string(),
            digest: Some(directory),
        };
        let new_dir = Directory {
            files: vec![],
            directories: vec![new_node],
            symlinks: vec![],
            // todo
            node_properties: Some(NodeProperties {
                properties: vec![],
                mtime: None,
                unix_mode: None,
            }),
        };
        let new_digest = digest_from_directory(connection, new_dir).await?;
        Ok(offset_digest_iter(connection, new_digest, offset).await?)
    } else {
        Ok(directory)
    }
}

/// Join/merge directories into a single directory structure
///
/// This function takes directory digests and fetches, unpacks
/// and joins them and then uploads the new joined directory.
///
/// See `join_directories` if it is more convenient to work with
/// the raw directories as this function just wraps this function
/// with some REAPI IO.
///
/// If both directories or there subdirectories have the same named
/// file or link at the same location but with different digests then
/// the function will panic.
///
/// This function will in the future have options for how to deal with
/// cases were the directories have non-identical files in the same
/// point in both trees.
pub async fn join_digests(
    connection: &mut ReapiConnection,
    directory_a: &Digest,
    directory_b: &Digest,
) -> anyhow::Result<Digest> {
    if directory_a == directory_b {
        return Ok(directory_a.clone());
    }
    let directory_a = directory_from_digest(connection, directory_a).await?;

    let directory_b = directory_from_digest(connection, directory_b).await?;

    let combo = join_directories(connection, &directory_a, &directory_b).await?;

    // upload the joined digest
    digest_from_directory(connection, combo).await
}

/// Verify a directory from the cas
///
/// Verify that the cas has a hole directory structure
#[async_recursion]
pub async fn check_directory(connection: &mut ReapiConnection, dir: Digest) -> Crapshoot {
    let directory = directory_from_digest(connection, &dir).await?;

    let mut blobs = vec![];
    for file_node in directory.files {
        blobs.push(file_node.digest.unwrap());
    }
    for dir_node in &directory.directories {
        blobs.push(dir_node.digest.clone().unwrap());
    }
    let find_missing_req = FindMissingBlobsRequest {
        instance_name: "".to_string(),
        blob_digests: blobs,
    };
    let local = connection.get_cas_client().await?;
    local.find_missing_blobs(find_missing_req).await?;

    for dir_node in directory.directories {
        check_directory(connection, dir_node.digest.unwrap()).await?;
    }

    Ok(())
}

/// Create a REAPI Tree from its digest
///
/// Most REAPI operations use directories but some of the
/// build steps return trees.
///
/// These are very bad for providing large directory structures
/// specially if the hole or a subset is likely to be re-used
/// or already used.
pub async fn tree_from_digest(
    connection: &mut ReapiConnection,
    digest: &Digest,
) -> anyhow::Result<Tree> {
    let tree_blob = fetch_blob(connection, digest).await?;

    let tree = Tree::decode(&*tree_blob).expect("could not decode directory");
    Ok(tree)
}

/// Encode and upload a directory into the cas
///
/// Upload a REAPI `Directory` in to the cas and return
/// its `Digest` so it can be referred to by other REAPI
/// operations.
pub async fn digest_from_directory(
    connection: &mut ReapiConnection,
    directory: Directory,
) -> anyhow::Result<Digest> {
    let mut buf: Vec<u8> = vec![];
    directory.encode(&mut buf).unwrap();
    upload_blob(connection, buf).await
}

/// Fetch and decode a directory from the cas
///
/// Fetch a REAPI directory from the cas and decode
/// it. Returns the `Directory` structure represented
/// by the given digest.
pub async fn directory_from_digest(
    connection: &mut ReapiConnection,
    digest: &Digest,
) -> anyhow::Result<Directory> {
    let dir_blob = fetch_blob(connection, digest).await?;

    let directory = Directory::decode(&*dir_blob).expect("could not decode directory");
    Ok(directory)
}

enum AllNodes {
    File(FileNode),
    Symlink(SymlinkNode),
    Dir(DirectoryNode),
}

impl fmt::Display for AllNodes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::File(_) => write!(f, "File"),
            Self::Symlink(_) => write!(f, "Symlink"),
            Self::Dir(_) => write!(f, "Directory"),
        }
    }
}

/// Join/merge directories into a single directory structure
///
/// This function takes directory structs and joins them and
/// then returns new joined directory.
///
/// See `join_digests` if it is more convenient to use digests
/// as that function just wraps this function with som REAPI IO.
///
/// If both directories or there subdirectories have the same named
/// file or link at the same location but with different digests then
/// the function will panic.
///
/// This function will in the future have options for how to deal with
/// cases were the directories have non-identical files in the same
/// point in both trees.
#[async_recursion]
pub(crate) async fn join_directories(
    connection: &mut ReapiConnection,
    directory_a: &Directory,
    directory_b: &Directory,
) -> anyhow::Result<Directory> {
    let mut full_list: BTreeMap<String, AllNodes> = BTreeMap::new();
    for file in &directory_a.files {
        full_list.insert(file.name.clone(), AllNodes::File(file.clone()));
    }
    for file in &directory_b.files {
        if let Some(existing) = full_list.insert(file.name.clone(), AllNodes::File(file.clone())) {
            match existing {
                AllNodes::File(file_a) => {
                    if file.digest != file_a.digest {
                        anyhow::bail!("Repeated file \"{}\" that does not match", file.name)
                    }
                }
                AllNodes::Dir(_) | AllNodes::Symlink(_) => {
                    anyhow::bail!("Logic Error in join directories",)
                }
            }
        }
    }
    for symlink in &directory_a.symlinks {
        if let Some(existsting) =
            full_list.insert(symlink.name.clone(), AllNodes::Symlink(symlink.clone()))
        {
            anyhow::bail!(
                "Repeated symlink \"{}\" that does not match {}",
                symlink.name,
                existsting
            )
        };
    }
    for symlink in &directory_b.symlinks {
        if let Some(existing) =
            full_list.insert(symlink.name.clone(), AllNodes::Symlink(symlink.clone()))
        {
            match &existing {
                AllNodes::Symlink(symlink_a) => {
                    if symlink.target != symlink_a.target {
                        anyhow::bail!("Repeated symlink \"{}\" that does not match", symlink.name,)
                    }
                }
                AllNodes::File(_) | AllNodes::Dir(_) => {
                    anyhow::bail!(
                        "Repeated symlink \"{}\" that does not match {}",
                        symlink.name,
                        existing
                    )
                }
            }
        }
    }
    for dir in &directory_a.directories {
        if let Some(existing) = full_list.insert(dir.name.clone(), AllNodes::Dir(dir.clone())) {
            anyhow::bail!(
                "Repeated directory \"{}\" that does not match {}",
                dir.name,
                existing
            )
        };
    }
    for dir_b in &directory_b.directories {
        if let Some(existing) = full_list.remove(&dir_b.name) {
            match existing {
                AllNodes::File(_) | AllNodes::Symlink(_) => {
                    anyhow::bail!(
                        "Repeated directory \"{}\" that does not match {}",
                        dir_b.name,
                        existing
                    )
                }
                AllNodes::Dir(dir_a) => {
                    if dir_a.digest != dir_b.digest {
                        let dir_a_dir = directory_from_digest(
                            connection,
                            dir_a
                                .digest
                                .as_ref()
                                .ok_or(anyhow::anyhow!("missing directory blob"))?,
                        )
                        .await?;
                        let dir_b_dir = directory_from_digest(
                            connection,
                            dir_b
                                .digest
                                .as_ref()
                                .ok_or(anyhow::anyhow!("missing directory blob"))?,
                        )
                        .await?;
                        let new_dir = join_directories(connection, &dir_a_dir, &dir_b_dir).await?;
                        let new_node = DirectoryNode {
                            name: dir_a.name.clone(),
                            digest: Some(digest_from_directory(connection, new_dir).await?),
                        };

                        full_list.insert(dir_b.name.clone(), AllNodes::Dir(new_node));
                    } else {
                        full_list.insert(dir_a.name.clone(), AllNodes::Dir(dir_a));
                    };
                }
            };
        } else {
            full_list.insert(dir_b.name.clone(), AllNodes::Dir(dir_b.clone()));
        }
    }
    let mut combo = Directory {
        files: vec![],
        directories: vec![],
        symlinks: vec![],
        node_properties: directory_a.node_properties.clone(),
    };
    for (_node_name, new_node) in full_list {
        match new_node {
            AllNodes::File(new_file) => combo.files.push(new_file),
            AllNodes::Symlink(new_sym) => combo.symlinks.push(new_sym),
            AllNodes::Dir(new_dir) => combo.directories.push(new_dir),
        }
    }

    Ok(combo)
}

#[cfg(test)]
mod tests {
    use reapi::build::bazel::remote::execution::v2::{
        Digest, Directory, DirectoryNode, FileNode, SymlinkNode,
    };
    use sha2::{Digest as _, Sha256};

    use crate::{
        cas_fs::{directory_from_digest, join_directories, offset_digest, place_file_in_dir},
        test_infra::YabaTestSetup,
        Crapshoot, LocalConfig,
    };

    #[tokio::test]
    async fn test_join_two_dirs_with_repeated_dir() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        // This creates a empty digest that can be used later
        // Note, A hidden bit of the REAPI spec is that the empty digest should always
        // be present so we don't have to push it.
        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };

        // This is a basic directory node, its not really valid but provides a Node
        // but is good enough for the test
        let directory_node = DirectoryNode {
            digest: Some(empty_digest.clone()),
            name: "bob".to_string(),
        };

        // It is valid to have a valid file but this still a very simple repeated
        // file
        let file_node = FileNode {
            digest: Some(empty_digest),
            is_executable: true,
            name: "file".to_string(),
            node_properties: None,
        };

        // Simple symlink, should be fine so long as both targets match
        let link_node = SymlinkNode {
            target: "some_target".to_string(),
            name: "aLink".to_string(),
            node_properties: None,
        };

        let directory = Directory {
            directories: vec![directory_node],
            files: vec![file_node],
            node_properties: None,
            symlinks: vec![link_node],
        };

        // We now join the same Directory with its self, it contains a file and a folder
        // join_directories should see that they are the same and repeated and then just
        // return a single version of each
        let new_directories = join_directories(&mut connection, &directory, &directory).await?;

        assert_eq!(new_directories, directory);
        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_join_two_dirs_file_link() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        // This creates a empty digest that can be used later
        // Note, A hidden bit of the REAPI spec is that the empty digest should always
        // be present so we don't have to push it.
        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };

        // It is valid to have a valid file but this still a very simple repeated
        // file
        let file_node = FileNode {
            digest: Some(empty_digest),
            is_executable: true,
            name: "file".to_string(),
            node_properties: None,
        };

        // It is valid to have a valid file but this still a very simple repeated
        // file
        let link_node = SymlinkNode {
            target: "some_target".to_string(),
            name: "file".to_string(),
            node_properties: None,
        };

        let directory_a = Directory {
            directories: vec![],
            files: vec![file_node],
            node_properties: None,
            symlinks: vec![],
        };

        let directory_b = Directory {
            directories: vec![],
            files: vec![],
            node_properties: None,
            symlinks: vec![link_node],
        };

        // We now join the same Directory with its self, it contains a file and a folder
        // join_directories should see that they are the same and repeated and then just
        // return a single version of each
        let new_directories = join_directories(&mut connection, &directory_a, &directory_b).await;

        assert!(new_directories.is_err());
        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_join_two_dirs_dir_link() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        // This creates a empty digest that can be used later
        // Note, A hidden bit of the REAPI spec is that the empty digest should always
        // be present so we don't have to push it.
        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };

        // This is a basic directory node, its not really valid but provides a Node
        // but is good enough for the test
        let directory_node = DirectoryNode {
            digest: Some(empty_digest.clone()),
            name: "dir".to_string(),
        };

        // It is valid to have a valid file but this still a very simple repeated
        // file
        let link_node = SymlinkNode {
            target: "some_target".to_string(),
            name: "dir".to_string(),
            node_properties: None,
        };

        let directory_a = Directory {
            directories: vec![directory_node],
            files: vec![],
            node_properties: None,
            symlinks: vec![],
        };

        let directory_b = Directory {
            directories: vec![],
            files: vec![],
            node_properties: None,
            symlinks: vec![link_node],
        };

        // We now join the same Directory with its self, it contains a file and a folder
        // join_directories should see that they are the same and repeated and then just
        // return a single version of each
        let new_directories = join_directories(&mut connection, &directory_a, &directory_b).await;

        assert!(new_directories.is_err());
        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_join_two_dirs_with_files() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };
        let directory_a_digest = place_file_in_dir(
            &mut connection,
            "empty_file_one".to_string(),
            empty_digest.clone(),
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_a = directory_from_digest(&mut connection, &directory_a_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_a.files.len(), 1);
        assert_eq!(directory_a.directories.len(), 0);

        let directory_b_digest = place_file_in_dir(
            &mut connection,
            "empty_file_two".to_string(),
            empty_digest,
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_b = directory_from_digest(&mut connection, &directory_b_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_b.files.len(), 1);
        assert_eq!(directory_b.directories.len(), 0);

        let joined_directory = join_directories(&mut connection, &directory_a, &directory_b)
            .await
            .expect("Can not join directories");

        assert_eq!(joined_directory.directories.len(), 0);
        assert_eq!(joined_directory.files.len(), 2);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_join_two_dirs_with_dirs() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };
        let directory_a_digest = place_file_in_dir(
            &mut connection,
            "empty_file_one".to_string(),
            empty_digest.clone(),
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_a = directory_from_digest(&mut connection, &directory_a_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_a.files.len(), 1);
        assert_eq!(directory_a.directories.len(), 0);

        let directory_b_digest = place_file_in_dir(
            &mut connection,
            "empty_file_two".to_string(),
            empty_digest,
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_b = directory_from_digest(&mut connection, &directory_b_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_b.files.len(), 1);
        assert_eq!(directory_b.directories.len(), 0);

        let directory_c_digest =
            offset_digest(&mut connection, directory_a_digest, "directory_a".into())
                .await
                .expect("Could not create sub folder");
        let directory_d_digest =
            offset_digest(&mut connection, directory_b_digest, "directory_b".into())
                .await
                .expect("Could not create sub folder");
        let directory_c = directory_from_digest(&mut connection, &directory_c_digest)
            .await
            .expect("Could not create directory from digest");
        let directory_d = directory_from_digest(&mut connection, &directory_d_digest)
            .await
            .expect("Could not create directory from digest");
        let directory_e = join_directories(&mut connection, &directory_c, &directory_d)
            .await
            .expect("could not join directories");

        assert_eq!(directory_e.files.len(), 0);
        assert_eq!(directory_e.directories.len(), 2);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_join_two_dirs_with_a_dir() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.into();

        let mut dig = Sha256::new();
        dig.update([]);
        let empty_digest_string = format!("{:x}", dig.finalize());
        let empty_digest = Digest {
            hash: empty_digest_string,
            size_bytes: 0,
        };
        let directory_a_digest = place_file_in_dir(
            &mut connection,
            "empty_file_one".to_string(),
            empty_digest.clone(),
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_a = directory_from_digest(&mut connection, &directory_a_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_a.files.len(), 1);
        assert_eq!(directory_a.directories.len(), 0);

        let directory_b_digest = place_file_in_dir(
            &mut connection,
            "empty_file_two".to_string(),
            empty_digest,
            false,
        )
        .await
        .expect("Can not create directory");
        let directory_b = directory_from_digest(&mut connection, &directory_b_digest)
            .await
            .expect("Could not create directory from Digest");

        assert_eq!(directory_b.files.len(), 1);
        assert_eq!(directory_b.directories.len(), 0);

        let directory_c_digest = offset_digest(
            &mut connection,
            directory_a_digest,
            "directory_common".into(),
        )
        .await
        .expect("Could not create sub folder");
        let directory_d_digest = offset_digest(
            &mut connection,
            directory_b_digest,
            "directory_common".into(),
        )
        .await
        .expect("Could not create sub folder");
        let directory_c = directory_from_digest(&mut connection, &directory_c_digest)
            .await
            .expect("Could not create directory from digest");
        let directory_d = directory_from_digest(&mut connection, &directory_d_digest)
            .await
            .expect("Could not create directory from digest");
        let directory_e = join_directories(&mut connection, &directory_c, &directory_d)
            .await
            .expect("could not join directories");

        assert_eq!(directory_e.files.len(), 0);
        assert_eq!(directory_e.directories.len(), 1);

        drop(running_yts);
        Ok(())
    }
}
