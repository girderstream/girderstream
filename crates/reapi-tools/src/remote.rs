// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Push and fetch artifacts from the local cache to the remote cache

use std::vec;

use async_channel::bounded;
use async_recursion::async_recursion;
use prost::Message;
use reapi::build::bazel::remote::execution::v2::{
    Digest, Directory, FindMissingBlobsRequest, Tree,
};
use tokio::task::{self, JoinHandle};

use crate::{
    actor::ReapiActions,
    cas_fs::directory_from_digest,
    grpc_io::{fetch_blob, move_blob_between_cas, upload_blob},
    Crapshoot, ReapiConnection,
};

/// Gather a list of all the digests that need to be uploaded to fully upload a tree
///
/// It would be nice to be able to start pushing as soon as we have some elements
/// maybe this could be done as a stream..
#[async_recursion]
pub async fn from_tree_to_list(
    connection: &mut ReapiConnection,
    directory_digest: &Digest,
) -> anyhow::Result<Vec<Digest>> {
    let directory = directory_from_digest(connection, directory_digest).await?;

    let mut list = vec![directory_digest.clone()];
    for file in directory.files {
        list.push(file.digest.unwrap());
    }
    for dir in directory.directories {
        list.extend(from_tree_to_list(connection, &dir.digest.unwrap()).await?);
    }
    Ok(list)
}

/// Upload blobs to a remote cas in pure rust
///
/// Chunks the [`FindMissingBlobsRequest`] to avoid
/// creating over sized Requests.
pub async fn fetch_blobs_actor(actor: ReapiActions, blob_digests: Vec<Digest>) -> Crapshoot {
    let max_blob_size = actor.get_max_blob().await? - 5120 * 10;
    if blob_digests.is_empty() {
        return Ok(());
    }

    let mut futures = vec![];
    let mut missing_blobs = vec![];
    let mut batch_blobs = vec![];
    let mut batch_size = 0;
    // Chunk up the blobs so that [`FindMissingBlobsRequest`] requests are not too big
    // each request should be no bigger than the max request size.
    // We should check the max size of requests and then set the chunk size as appropriate.
    for blobs in blob_digests.chunks(10_000) {
        // Check for which blobs need uploading
        missing_blobs.extend(actor.find_missing_blobs(blobs.to_owned()).await?);
        for digest in &missing_blobs {
            let effective_size = digest.size_bytes as usize + 5120 * 10;
            if effective_size > max_blob_size {
                futures.push(actor.move_blob_down(digest.to_owned()).await?);
            } else if batch_size + effective_size < max_blob_size {
                batch_blobs.push(digest.clone());
                batch_size += effective_size;
            } else {
                futures.push(actor.move_blobs_down_batch(batch_blobs.clone()).await?);
                batch_blobs.truncate(0);
                // This blob is always small enough to add as its passed the first check in this
                // 'if block' that checks if the blob is not bigger than the max fetch size
                batch_blobs.push(digest.clone());
                batch_size = effective_size;
            }
        }
    }
    if !batch_blobs.is_empty() {
        futures.push(actor.move_blobs_down_batch(batch_blobs.clone()).await?);
    }

    for future in futures {
        future.get_finished().await?;
    }
    Ok(())
}

/// Upload blobs to a remote cas in pure rust
///
/// Chunks the [`FindMissingBlobsRequest`] to avoid
/// creating over sized Requests.
pub async fn push_blobs(
    connection_from: &mut ReapiConnection,
    connection_to: &mut ReapiConnection,
    blob_digests: Vec<Digest>,
) -> Crapshoot {
    // Set up a set of workers that run `move_blob_between_cas` when there are blobs
    // to move.
    if blob_digests.is_empty() {
        return Ok(());
    }
    let (s_job, r_job) = bounded(1000);
    let mut joins: Vec<JoinHandle<()>> = vec![];
    // this will error if blob_digests is empty hence early exit above
    for _processor in 0..(100.min(1.max(blob_digests.len() as i64 / 3 - 1))) {
        let mut local_connection_from = connection_from.clone();
        let mut local_connection_to = connection_to.clone();
        let reciver: async_channel::Receiver<Option<Digest>> = r_job.clone();

        let join = task::spawn(async move {
            while let Ok(recived) = reciver.recv().await {
                if let Some(blob) = recived {
                    move_blob_between_cas(
                        &mut local_connection_from,
                        &mut local_connection_to,
                        blob,
                    )
                    .await
                    .unwrap();
                } else {
                    break;
                }
            }
        });
        joins.push(join);
    }

    // Setup the client to query which blobs to move.
    let instance_name = connection_to.get_instance_name().to_string();
    let casc = connection_to.get_cas_client().await?;

    // Chunk up the blobs so that [`FindMissingBlobsRequest`] requests are not too big
    // each request should be no bigger than the max request size.
    // We should check the max size of requests and then set the chunk size as appropriate.
    for blobs in blob_digests.chunks(10_000) {
        // Check for which blobs need uploading
        let fmbr = FindMissingBlobsRequest {
            instance_name: instance_name.to_string(),
            blob_digests: blobs.to_owned(),
        };
        let fmbres = casc.find_missing_blobs(fmbr).await?;
        let (meta_map, res, _) = fmbres.into_parts();
        assert_eq!(meta_map.into_headers().get("grpc-status").unwrap(), "0");

        // Let the worker tasks know which blobs to move
        for blob in res.missing_blob_digests {
            // The bounded queue means that these will wait once the queue fills up.
            // The queue stops this loop dumping all the blobs into the queue and the queue
            // becoming huge.
            s_job.send(Some(blob)).await?;
        }
    }
    // We have now issued all the requests so now signal to the worker tasks to stop
    for _join in &joins {
        s_job.send(None).await?;
    }
    // Wait for all the works to finish pushing all their blobs
    for join in joins {
        join.await?;
    }

    Ok(())
}

/// Take a [`Directory`], ensure it is in the local cache and return the Digests of its files
///
/// This function is helper for [`pull_cached_cas_manual`]
async fn gather_directories(
    connection_local: &mut ReapiConnection,
    directory: &Directory,
) -> anyhow::Result<Vec<Digest>> {
    let mut root_blob = vec![];
    directory.encode(&mut root_blob)?;
    upload_blob(connection_local, root_blob).await?;
    let mut files = vec![];

    for file in &directory.files {
        files.push(file.digest.clone().unwrap())
    }

    Ok(files)
}

/// Pull a cas structure from a remote to local
///
/// This does not use casd's protos and so can be use with non casd based
/// cas, eg buildbarn
pub async fn pull_cached_cas(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    tree_digest: &Digest,
) -> Crapshoot {
    let tree_blob = fetch_blob(connection_remote, tree_digest).await?;
    let tree = Tree::decode(tree_blob.as_slice())?;

    let root = tree.root.as_ref().unwrap();
    let mut files = gather_directories(connection_local, root).await?;

    for child_dir in &tree.children {
        files.extend(
            gather_directories(connection_local, child_dir)
                .await?
                .into_iter(),
        );
    }

    push_blobs(connection_remote, connection_local, files).await?;

    Ok(())
}

/// Pull a cas structure from a remote to local
///
/// This does not use casd's protos and so can be use with non casd based
/// cas, eg buildbarn
#[async_recursion]
pub async fn pull_cached_dir_cas(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    directory_digest: &Digest,
) -> Crapshoot {
    let tree_blob = fetch_blob(connection_remote, directory_digest).await?;
    let directory = Directory::decode(tree_blob.as_slice())?;

    push_blobs(
        connection_remote,
        connection_local,
        directory
            .files
            .iter()
            .map(|node| node.digest.clone().unwrap())
            .collect(),
    )
    .await?;

    for dir_digest in directory
        .directories
        .iter()
        .map(|dir| dir.digest.clone().unwrap())
    {
        pull_cached_dir_cas(connection_local, connection_remote, &dir_digest).await?;
    }
    upload_blob(connection_local, tree_blob).await?;
    Ok(())
}

/// Pull a cas structure from a remote to local
///
/// This does not use casd's protos and so can be use with non casd based
/// cas, eg buildbarn
#[async_recursion]
pub async fn pull_cached_dir_cas_actor(actor: ReapiActions, directory_digest: Digest) -> Crapshoot {
    let tree_blob = actor.pull_remote_blob(directory_digest.clone()).await?;
    let directory = Directory::decode(tree_blob.as_slice())?;
    pull_cached_dir_actor(actor.clone(), directory).await?;

    let dir = actor.upload_blob(tree_blob).await?;

    assert_eq!(dir, directory_digest);
    Ok(())
}

#[async_recursion]
pub async fn pull_cached_dir_actor(actor: ReapiActions, directory: Directory) -> Crapshoot {
    let files = tokio::spawn(fetch_blobs_actor(
        actor.clone(),
        directory
            .files
            .iter()
            .map(|node| node.digest.clone().unwrap())
            .collect(),
    ));

    let mut directories = vec![];
    for dir_digest in directory
        .directories
        .iter()
        .map(|dir| dir.digest.clone().unwrap())
    {
        directories.push(dir_digest)
    }
    // fetch_blobs_actor does a missing blobs check its self.
    fetch_blobs_actor(actor.clone(), directories.clone()).await?;
    let mut directory_handles = vec![];
    for dir_digest in directories {
        directory_handles.push(tokio::spawn({
            let actor = actor.clone();
            let directory = actor.directory_from_digest(dir_digest).await?;
            pull_cached_dir_actor(actor, directory)
        }));
    }

    files.await??;
    for dir in directory_handles {
        dir.await??;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use anyhow::anyhow;
    use reapi::build::bazel::remote::execution::v2::{Directory, DirectoryNode, FileNode};

    use crate::{
        actor::ReapiConnections,
        cas_fs::digest_from_directory,
        grpc_io::{fetch_blob, upload_blob},
        remote::from_tree_to_list,
        test_infra::YabaTestSetup,
        Crapshoot, LocalConfig, RemoteConfig,
    };

    use super::pull_cached_dir_cas_actor;

    // This test checks the most basic functioning of a add_element_to_build
    #[tokio::test]
    async fn pull_test() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let yts_remote = YabaTestSetup::setup_server().await?;
        let running_yts_remote = yts_remote.run_cas_thread().await;
        let address_remote = running_yts_remote.get_address().to_owned();

        let config = LocalConfig::new_yaba(address.clone(), "".to_string(), "".to_string().into());
        let remote = RemoteConfig::new(address_remote, "".to_string(), true, None, false);

        let mut connection = config.into();
        let mut remote_connection = remote.into();

        let file_digest = upload_blob(&mut remote_connection, vec![0, 1, 2, 3, 4, 5]).await?;
        let file_node = FileNode {
            name: "bob".to_string(),
            digest: Some(file_digest.clone()),
            is_executable: false,
            node_properties: None,
        };
        let base_dir = Directory {
            files: vec![file_node.clone()],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        let digest_inner_dir = digest_from_directory(&mut remote_connection, base_dir).await?;

        let mut base_dir = Directory {
            files: vec![file_node],
            directories: vec![DirectoryNode {
                name: "dir".to_string(),
                digest: Some(digest_inner_dir.clone()),
            }],
            symlinks: vec![],
            node_properties: None,
        };

        let file_digest_2 = upload_blob(&mut remote_connection, vec![0, 1, 2, 3, 4, 6]).await?;
        let file_node = FileNode {
            name: "bob".to_string(),
            digest: Some(file_digest_2.clone()),
            is_executable: false,
            node_properties: None,
        };
        base_dir.files.push(file_node);

        let digest = digest_from_directory(&mut remote_connection, base_dir).await?;

        let (pool, actor) =
            ReapiConnections::new_worker_sender(&mut connection, &mut remote_connection, 5, None)
                .await;
        let pulled_cas = pull_cached_dir_cas_actor(actor, digest.to_owned()).await;
        drop(pool);
        match pulled_cas {
            Ok(_) => match from_tree_to_list(&mut connection, &digest).await {
                Ok(ref_digests) => {
                    assert!(ref_digests.contains(&digest));
                    assert!(ref_digests.contains(&digest_inner_dir));
                    assert!(ref_digests.contains(&file_digest));
                    assert!(ref_digests.contains(&file_digest_2));
                }
                Err(e) => {
                    return Err(anyhow!("Could not pull child blob cas import {e:?}"));
                }
            },
            Err(e) => {
                return Err(anyhow!("Could not pull cas import {e:?}"));
            }
        }

        fetch_blob(&mut connection, &digest).await?;
        fetch_blob(&mut connection, &digest_inner_dir).await?;
        fetch_blob(&mut connection, &file_digest).await?;
        fetch_blob(&mut connection, &file_digest_2).await?;
        fetch_blob(&mut remote_connection, &digest).await?;
        fetch_blob(&mut remote_connection, &digest_inner_dir).await?;
        fetch_blob(&mut remote_connection, &file_digest).await?;
        fetch_blob(&mut remote_connection, &file_digest_2).await?;
        drop(running_yts);
        Ok(())
    }
}
