// Copyright 2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! REAPI actor structs and functions
//!
//! Provides a way to mange a worker pool and cheap to create and drop actors.
use reapi::build::bazel::remote::execution::v2::{Digest, Directory, FindMissingBlobsRequest};
use std::{path::PathBuf, sync::Arc};
use tokio::{
    fs::File,
    io::AsyncReadExt,
    sync::{
        mpsc::{channel, Receiver, Sender},
        oneshot, Mutex,
    },
    task::JoinHandle,
};

use crate::{
    cas_fs::{digest_from_directory, directory_from_digest},
    grpc_io::{fetch_blob, move_blob_between_cas, move_blobs_between_cas, upload_blob},
    ReapiConnection,
};

/// Internal type alowing the same work queue to be used for multiple types of request
enum ReapiAction {
    /// A request to upload from a local path
    Upload(oneshot::Sender<Digest>, PathBuf),
    /// A request to upload a in memeory blob
    UploadBlob(oneshot::Sender<Digest>, Vec<u8>),
    /// Fetch a remote blob
    PullRemoteBlob(oneshot::Sender<Vec<u8>>, Digest),
    /// Fetch a remote blob
    PullBlob(oneshot::Sender<Vec<u8>>, Digest),
    /// A request to upload a [`Directory`]
    DigestFromDir(oneshot::Sender<Digest>, Directory),
    FindMissingBlobs(oneshot::Sender<Vec<Digest>>, Vec<Digest>),
    DirFromDigest(oneshot::Sender<Directory>, Digest),
    MoveBlobDown(oneshot::Sender<()>, Digest),
    MoveBlobsDown(oneshot::Sender<()>, Vec<Digest>),
    GetMaxSize(oneshot::Sender<usize>),
}

/// A pool of reapi Connections performing the tasks sent to actors
pub struct ReapiConnections {
    _join: Vec<JoinHandle<()>>,
}

async fn reapi_server(
    connection: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    receiver: Receiver<ReapiAction>,
    worker_pool_size: Option<usize>,
) -> Vec<JoinHandle<()>> {
    let reciver = Arc::new(Mutex::new(receiver));
    let mut joins = vec![];
    for _iii in 0..worker_pool_size.unwrap_or(num_cpus::get()) {
        let this_reciver = reciver.clone();
        let mut this_connection = connection.clone();
        let mut this_connection_remote = connection_remote.clone();
        let join = tokio::task::spawn(async move {
            loop {
                let mut lock = this_reciver.lock().await;

                let next = lock.recv().await;
                drop(lock);
                if let Some(new_action) = next {
                    match new_action {
                        ReapiAction::Upload(returner, path) => {
                            let mut blob: Vec<u8> = vec![];
                            File::open(&path)
                                .await
                                .expect("Could not open file 'test_source_second.output'")
                                .read_to_end(&mut blob)
                                .await
                                .expect("Could not read from file 'test_source_second.output'");
                            let digest = upload_blob(&mut this_connection, blob).await.unwrap();
                            returner.send(digest).unwrap();
                        }
                        ReapiAction::DigestFromDir(returner, mut dir) => {
                            dir.files.sort_by(|a, b| a.name.cmp(&b.name));
                            dir.directories.sort_by(|a, b| a.name.cmp(&b.name));
                            dir.symlinks.sort_by(|a, b| a.name.cmp(&b.name));
                            let digest = digest_from_directory(&mut this_connection, dir)
                                .await
                                .unwrap();
                            returner.send(digest).unwrap();
                        }
                        ReapiAction::DirFromDigest(returner, digest) => {
                            let dir = directory_from_digest(&mut this_connection, &digest)
                                .await
                                .unwrap();
                            returner.send(dir).unwrap();
                        }
                        ReapiAction::UploadBlob(returner, blob) => {
                            let digest = upload_blob(&mut this_connection, blob).await.unwrap();
                            returner.send(digest).unwrap();
                        }
                        ReapiAction::PullBlob(returner, digest) => {
                            let blob = fetch_blob(&mut this_connection, &digest).await.unwrap();
                            returner.send(blob).unwrap();
                        }
                        ReapiAction::PullRemoteBlob(returner, digest) => {
                            let blob = fetch_blob(&mut this_connection_remote, &digest)
                                .await
                                .unwrap();
                            returner.send(blob).unwrap();
                        }
                        ReapiAction::FindMissingBlobs(returner, blob_digests) => {
                            let fmbr = FindMissingBlobsRequest {
                                instance_name: this_connection.get_instance_name().to_owned(),
                                blob_digests,
                            };
                            let fmbres = this_connection
                                .get_cas_client()
                                .await
                                .unwrap()
                                .find_missing_blobs(fmbr)
                                .await
                                .unwrap();
                            let (meta_map, res, _) = fmbres.into_parts();
                            assert_eq!(meta_map.into_headers().get("grpc-status").unwrap(), "0");

                            // Let the worker tasks know which blobs to move
                            returner.send(res.missing_blob_digests).unwrap()
                        }
                        ReapiAction::MoveBlobDown(returner, blob) => {
                            move_blob_between_cas(
                                &mut this_connection_remote,
                                &mut this_connection,
                                blob,
                            )
                            .await
                            .unwrap();
                            returner.send(()).unwrap()
                        }
                        ReapiAction::MoveBlobsDown(returner, blobs) => {
                            //let mut total = 0;
                            //for blob in &blobs {
                            //    total += blob.size_bytes;
                            //}
                            //println!("total {total}");
                            move_blobs_between_cas(
                                &mut this_connection_remote,
                                &mut this_connection,
                                blobs,
                            )
                            .await
                            .unwrap();
                            returner.send(()).unwrap()
                        }
                        ReapiAction::GetMaxSize(returner) => returner
                            .send(
                                this_connection
                                    .get_blob_size()
                                    .await
                                    .unwrap()
                                    .max(this_connection_remote.get_blob_size().await.unwrap())
                                    as usize,
                            )
                            .unwrap(),
                    }
                } else {
                    break;
                }
            }
        });
        joins.push(join);
    }
    joins
}

impl ReapiConnections {
    /// Create a worker pool and a [`ReapiActions`] to send actions
    ///
    /// The pool [`ReapiConnections`] will shutdown if all the [`ReapiActions`] are dropped.
    ///
    /// The pool [`Self`] must be owned for actions to be processed and should then be dropped once
    /// it is no longer needed, the `ReapiActions` will stop working processing work "Acting" once
    /// the pool [`Self`] is dropped.
    pub async fn new_worker_sender(
        connection: &mut ReapiConnection,
        connection_remote: &mut ReapiConnection,
        queue_size: usize,
        worker_pool_size: Option<usize>,
    ) -> (Self, ReapiActions) {
        let (sender, receiver) = channel(queue_size);
        let join = reapi_server(connection, connection_remote, receiver, worker_pool_size).await;
        (Self { _join: join }, ReapiActions { sender })
    }
}

/// This "actor" will send and return reapi actions
///
/// This actor is just a easy way to queue up and recive actions from a worker pool of connecations
/// to a reapi cas server.
///
/// This "actor" is light wheigh and so can be cheaply cloned and dropped without createing or
/// destroying connections to the reapi server.
#[derive(Clone)]
pub struct ReapiActions {
    sender: Sender<ReapiAction>,
}

impl ReapiActions {
    /// Upload from the local file system to the cas
    ///
    /// This will await until the file is uploaded and return a [`Digest`]
    pub async fn upload_file(&self, path: PathBuf) -> anyhow::Result<Digest> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::Upload(returner, path))
            .await?;
        Ok(returnme.await?)
    }

    /// Upload from the local file system to the cas
    ///
    /// This will await until the file is uploaded into the queue and then return a
    /// [`UploadResult`] which its self can be awaited on to return the uploaded [`Digest`]
    pub async fn upload_file_send(&self, path: PathBuf) -> anyhow::Result<UploadResult> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::Upload(returner, path))
            .await?;
        Ok(UploadResult { reciver: returnme })
    }

    /// Upload a in memory blob to the cas
    ///
    /// This will await until the blob is uploaded and return a [`Digest`]
    pub async fn upload_blob(&self, blob: Vec<u8>) -> anyhow::Result<Digest> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::UploadBlob(returner, blob))
            .await?;
        Ok(returnme.await?)
    }

    /// Upload a in memory blob to the cas
    ///
    /// This will await until the in memory blob is uploaded into the queue and then return a [`UploadResult`]
    /// which its self can be awaited on to return the uploaded [`Digest`]
    pub async fn upload_blob_send(&self, blob: Vec<u8>) -> anyhow::Result<UploadResult> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::UploadBlob(returner, blob))
            .await?;
        Ok(UploadResult { reciver: returnme })
    }

    /// Upload a [`Directory`] to the cas
    ///
    /// This will await until the [`Directory`] is uploaded and return a [`Digest`]
    pub async fn digest_from_directory(&self, dir: Directory) -> anyhow::Result<Digest> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::DigestFromDir(returner, dir))
            .await?;
        Ok(returnme.await?)
    }
    /// Fetch a [`Directory`] from the cas
    ///
    /// This will await until the [`Directory`] is uploaded and return a [`Digest`]
    pub async fn directory_from_digest(&self, digest: Digest) -> anyhow::Result<Directory> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::DirFromDigest(returner, digest))
            .await?;
        Ok(returnme.await?)
    }

    /// Fetch a blob into memory from the cas
    ///
    /// This will await until the blob is pulled and return a [`Vec<u8>`]
    pub async fn pull_blob(&self, digest: Digest) -> anyhow::Result<Vec<u8>> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::PullBlob(returner, digest))
            .await?;
        Ok(returnme.await?)
    }

    /// Fetch a blob into memory from the remote cas
    ///
    /// This will await until the blob is pulled and return a [`Vec<u8>`]
    pub async fn pull_remote_blob(&self, digest: Digest) -> anyhow::Result<Vec<u8>> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::PullRemoteBlob(returner, digest))
            .await?;
        Ok(returnme.await?)
    }

    /// Fetch a blob into memory from the remote cas
    ///
    /// This will await until the digest is added to the queue and return a [`PullResult`] that
    /// can in turn be awaited on to return when the blob has been fetched.
    pub async fn pull_remote_blob_send(&self, digest: Digest) -> anyhow::Result<PullResult> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::PullRemoteBlob(returner, digest))
            .await?;
        Ok(PullResult { reciver: returnme })
    }

    /// Upload a in memory blob to the cas
    ///
    /// This will await until the blob is uploaded and return a [`Digest`]
    pub async fn find_missing_blobs(&self, digest: Vec<Digest>) -> anyhow::Result<Vec<Digest>> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::FindMissingBlobs(returner, digest))
            .await?;
        Ok(returnme.await?)
    }
    /// Upload a in memory blob to the cas
    ///
    /// This will await until the blob is uploaded and return a [`Digest`]
    pub async fn move_blob_down(&self, digest: Digest) -> anyhow::Result<PullComplete> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::MoveBlobDown(returner, digest))
            .await?;
        Ok(PullComplete { reciver: returnme })
    }

    pub async fn get_max_blob(&self) -> anyhow::Result<usize> {
        let (returner, returnme) = oneshot::channel();
        self.sender.send(ReapiAction::GetMaxSize(returner)).await?;
        Ok(returnme.await?)
    }

    pub(crate) async fn move_blobs_down_batch(
        &self,
        digests: Vec<Digest>,
    ) -> anyhow::Result<PullComplete> {
        let (returner, returnme) = oneshot::channel();
        self.sender
            .send(ReapiAction::MoveBlobsDown(returner, digests))
            .await?;
        Ok(PullComplete { reciver: returnme })
    }
}

/// A stuct used to get the result of a upload action
#[derive(Debug)]
#[must_use]
pub struct UploadResult {
    reciver: oneshot::Receiver<Digest>,
}

impl UploadResult {
    /// Get the digest of the upload
    ///
    /// This will await until the upload is complete and will then return the upload's [`Digest`]
    pub async fn get_digest(self) -> anyhow::Result<Digest> {
        Ok(self.reciver.await?)
    }
}

/// A stuct used to get the result of a upload action
#[derive(Debug)]
#[must_use]
pub struct PullResult {
    reciver: oneshot::Receiver<Vec<u8>>,
}

impl PullResult {
    /// Get the digest of the upload
    ///
    /// This will await until the upload is complete and will then return the upload's [`Digest`]
    pub async fn get_blob(self) -> anyhow::Result<Vec<u8>> {
        Ok(self.reciver.await?)
    }
}

/// A stuct used to get the result of a upload action
#[derive(Debug)]
#[must_use]
pub struct PullComplete {
    reciver: oneshot::Receiver<()>,
}

impl PullComplete {
    /// Get the digest of the upload
    ///
    /// This will await until the upload is complete and will then return the upload's [`Digest`]
    pub async fn get_finished(self) -> anyhow::Result<()> {
        Ok(self.reciver.await?)
    }
}

#[cfg(test)]
mod tests {

    use reapi::build::bazel::remote::execution::v2::Digest;
    use sha2::{Digest as _, Sha256};

    use crate::{
        grpc_io::{fetch_blob, upload_blob},
        test_infra::YabaTestSetup,
        Crapshoot, LocalConfig,
    };

    use super::ReapiConnections;

    #[tokio::test]
    async fn test_actor_upload() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.clone().into();
        let mut remote_connection = config.into();

        let blob = vec![0, 1, 2, 3, 4, 5, 6];

        let mut dig = Sha256::new();
        dig.update(&blob);
        let ref_digest_string = format!("{:x}", dig.finalize());
        let ref_digest = Digest {
            hash: ref_digest_string,
            size_bytes: blob.len() as i64,
        };

        let (pool, actor) =
            ReapiConnections::new_worker_sender(&mut connection, &mut remote_connection, 5, None)
                .await;

        let digest = actor.upload_blob(blob.clone()).await?;
        assert_eq!(digest, ref_digest);
        drop(pool);

        let fetched_blob = fetch_blob(&mut connection, &digest).await?;
        assert_eq!(blob, fetched_blob);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_actor_upload_send() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.clone().into();
        let mut remote_connection = config.into();

        let (pool, actor) =
            ReapiConnections::new_worker_sender(&mut connection, &mut remote_connection, 5, None)
                .await;

        let blob = vec![0, 1, 2, 3, 4, 5, 6];

        let waiter = actor.upload_blob_send(blob.clone()).await?;

        let digest = waiter.get_digest().await?;
        drop(pool);

        let fetched_blob = fetch_blob(&mut connection, &digest).await?;
        assert_eq!(blob, fetched_blob);

        drop(running_yts);
        Ok(())
    }

    #[tokio::test]
    async fn test_actor_pull() -> Crapshoot {
        let yts = YabaTestSetup::setup_server().await?;
        let tmp_dir_test_path = yts.get_temp_root();
        let running_yts = yts.run_cas_thread().await;
        let address = running_yts.get_address().to_owned();

        let config = LocalConfig::new_yaba(address, "".to_string(), tmp_dir_test_path.into());
        let mut connection = config.clone().into();
        let mut remote_connection = config.into();

        let blob = vec![0, 1, 2, 3, 4, 5, 6];

        let mut dig = Sha256::new();
        dig.update(&blob);
        let ref_digest_string = format!("{:x}", dig.finalize());
        let ref_digest = Digest {
            hash: ref_digest_string,
            size_bytes: blob.len() as i64,
        };

        let uploaded_digest = upload_blob(&mut connection, blob.clone()).await?;
        assert_eq!(ref_digest, uploaded_digest);

        let (pool, actor) =
            ReapiConnections::new_worker_sender(&mut connection, &mut remote_connection, 5, None)
                .await;

        let blob_fetch = actor.pull_blob(ref_digest.clone()).await?;
        assert_eq!(blob_fetch, blob);
        drop(pool);

        drop(running_yts);
        Ok(())
    }
}
