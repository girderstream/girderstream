// Copyright 2022-2025 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! REAPI helper functions
//!
//! Provide functions for implementing directly with REAPI and for
//! meta concepts.
//!
//! grpc_io provides functions like push and pull for cas blobs and
//! assets.
//!
//! cas_fs provides functions for higher level concepts like merkle
//! trees.
//!
//! Girderstream will also need more abstract layers like meta_fs
//! which will be described else were but the reapi backed implementation
//! may live here or maybe just more helper functions for that
//! task.
use std::{
    convert::{TryFrom, TryInto},
    env, fmt,
    io::Read,
    marker::PhantomData,
    path::Path,
    str::FromStr,
    {collections::BTreeMap, path::PathBuf},
};

use reapi::{
    build::bazel::remote::{
        asset::v1::{fetch_client::FetchClient, push_client::PushClient},
        execution::v2::{
            capabilities_client::CapabilitiesClient,
            content_addressable_storage_client::ContentAddressableStorageClient,
            execution_client::ExecutionClient, GetCapabilitiesRequest,
        },
    },
    google::bytestream::byte_stream_client::ByteStreamClient,
};
use serde::{
    de::{self, MapAccess, Visitor},
    Deserialize, Deserializer, Serialize,
};
use tokio::{fs::File, io::AsyncReadExt};
use tonic::transport::{Certificate, Channel, ClientTlsConfig, Endpoint, Identity};
use void::Void;

pub mod actor;
pub mod cas_fs;
pub mod fs_io;
pub mod grpc_io;
pub mod remote;
pub mod virtual_fs;

pub type Crapshoot = anyhow::Result<()>;

#[derive(Clone, Debug)]
pub struct RemoteExecution {
    scheduler: String,
    log_stream: String,
    platform: BTreeMap<String, String>,
}

// https://serde.rs/string-or-struct.html
fn string_or_struct<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: Deserialize<'de> + FromStr<Err = Void>,
    D: Deserializer<'de>,
{
    // This is a Visitor that forwards string types to T's `FromStr` impl and
    // forwards map types to T's `Deserialize` impl. The `PhantomData` is to
    // keep the compiler from complaining about T being an unused generic type
    // parameter. We need T in order to know the Value type for the Visitor
    // impl.
    struct StringOrStruct<T>(PhantomData<fn() -> T>);

    impl<'de, T> Visitor<'de> for StringOrStruct<T>
    where
        T: Deserialize<'de> + FromStr<Err = Void>,
    {
        type Value = T;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("string or map")
        }

        fn visit_str<E>(self, value: &str) -> Result<T, E>
        where
            E: de::Error,
        {
            Ok(FromStr::from_str(value).unwrap())
        }

        fn visit_map<M>(self, map: M) -> Result<T, M::Error>
        where
            M: MapAccess<'de>,
        {
            // `MapAccessDeserializer` is a wrapper that turns a `MapAccess`
            // into a `Deserializer`, allowing it to be used as the input to T's
            // `Deserialize` implementation. T then deserializes itself using
            // the entries from the map visitor.
            Deserialize::deserialize(de::value::MapAccessDeserializer::new(map))
        }
    }

    deserializer.deserialize_any(StringOrStruct(PhantomData))
}

/// Represents methods of specifying PEM-encoded X.509 Certificate data
#[derive(Debug, Eq, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
#[serde(untagged)]
pub enum PemData {
    /// Raw text in the configuration file. Not recommended for private keys.
    Str { text: String },

    /// Path to a file containing PEM-encoded data
    File { path: PathBuf },

    /// An environment variable containing the PEM-encoded data
    Env { env: String },
}

impl PemData {
    /// Get the PEM-encoded data from the configuration.
    pub fn get_pem(&self) -> anyhow::Result<String> {
        match self {
            PemData::Str { text } => Ok(text.to_owned()),
            PemData::File { path } => {
                let mut fl = std::fs::File::open(path)?;
                let mut data = vec![];
                fl.read_to_end(&mut data)?;
                Ok(String::from_utf8(data)?)
            }
            PemData::Env { env } => Ok(env::var(env)?),
        }
    }
}

impl FromStr for PemData {
    // This implementation of `from_str` can never fail, so use the impossible
    // `Void` type as the error type.
    type Err = Void;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(PemData::Str {
            text: s.to_string(),
        })
    }
}

/// Configuration for a TLS keypair, used for client mTLS authentication
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct CacheCert {
    #[serde(deserialize_with = "string_or_struct")]
    key: PemData,
    #[serde(deserialize_with = "string_or_struct")]
    cert: PemData,
}

/// Configuration for access to a Remote Asset API server implementation
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct RemoteAssetConfig {
    /// URL Endpoint of a Remote Asset API server
    url: String,
}

impl RemoteAssetConfig {
    /// Get the url or endpoint of a remote asset cache
    pub fn get_url(&self) -> &String {
        &self.url
    }
}

/// Configuration for access to a REAPI server
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct RemoteCacheConfig {
    /// URL Endpoint of a remote cache (CAS and Action Cache)
    url: String,

    /// REAPI Instance name to use with this endpoint
    instance: String,

    /// Whether to push built artifacts to this endpoint.  `true` to push, `false` to read only.
    push: Option<bool>,

    /// Configuration of a REAPI Execution service endpoint to use
    execution: Option<RemoteExecutionConfig>,

    /// Configuration of a Remote Asset API endpoint to use
    assets: Option<RemoteAssetConfig>,

    /// Whether to stop executing if an element fails to push to this endpoint
    stop_on_failed_push: Option<bool>,

    /// TLS keypair used for mTLS authentication with the endpoint
    client_cert: Option<CacheCert>,

    /// CA certificate used to verify the server
    server_cert: Option<PemData>,
}

impl RemoteCacheConfig {
    /// Get the url or endpoint of a remote cache
    pub fn get_url(&self) -> &String {
        &self.url
    }
    /// Get the instance name of a remote cache
    pub fn get_instance(&self) -> &String {
        &self.instance
    }
    /// Get if newly build elements should be pushed to a remote cache
    pub fn get_push(&self) -> bool {
        self.push.unwrap_or_default()
    }
    /// Get if girderstream should consider a failed push a reason to stop
    pub fn get_stop_on_failed_push(&self) -> bool {
        self.stop_on_failed_push.unwrap_or_default()
    }

    /// Get if girderstream should use this as a remote execution target
    pub fn get_execution(&self) -> Option<&RemoteExecutionConfig> {
        self.execution.as_ref()
    }

    /// Get if girderstream should use this as a remote asset target
    pub fn get_assets(&self) -> Option<&RemoteAssetConfig> {
        self.assets.as_ref()
    }

    /// Get the X.509 keypair girderstream should use for mTLS authentication with the target
    pub fn get_client_keypair(&self) -> Option<&CacheCert> {
        self.client_cert.as_ref()
    }

    /// Get the CA certificate girderstream should use to verify the target server
    pub fn get_server_cert(&self) -> Option<&PemData> {
        self.server_cert.as_ref()
    }
}

/// The configuration information needed to connect to the remote cas' of a project
///
/// This holds all the remote caches for a given project.
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct RemotesConfig {
    /// List of configured remotes
    remotes: Vec<RemoteCacheConfig>,
}

impl RemotesConfig {
    /// Create a remote cas' from a remote cas configuration file
    pub async fn open_from_file(config_path: &Path) -> anyhow::Result<Self> {
        let mut buff = vec![];

        File::open(&config_path)
            .await?
            .read_to_end(&mut buff)
            .await?;
        let s = std::str::from_utf8(&buff).unwrap();

        let new: RemotesConfig = serde_yaml::from_str(s).expect("Could parse yaml");
        Ok(new)
    }
    /// Get all the remote cache configs for the project
    pub fn get_remotes(&self) -> &Vec<RemoteCacheConfig> {
        &self.remotes
    }
}

/// Configuration of a REAPI Execution service
#[derive(Debug, Eq, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct RemoteExecutionConfig {
    /// URL endpoint for the Execution service
    scheduler: String,

    /// Name of this service's LogStream
    log_stream: Option<String>,

    /// Key/Value pairs used as Platform Properties
    platform: BTreeMap<String, String>,
}

impl RemoteExecutionConfig {
    /// Get the url or endpoint of a remote cache
    pub fn get_scheduler(&self) -> &String {
        &self.scheduler
    }
    /// Get the log stream name of a remote cache
    pub fn get_log_stream(&self) -> Option<&str> {
        self.log_stream.as_deref()
    }

    /// Get the mapping of Platform properties for this endpoint
    pub fn get_platform(&self) -> &BTreeMap<String, String> {
        &self.platform
    }
}

impl From<RemoteExecutionConfig> for RemoteExecution {
    fn from(remote_ex: RemoteExecutionConfig) -> Self {
        RemoteExecution {
            scheduler: remote_ex.get_scheduler().clone(),
            log_stream: remote_ex
                .get_log_stream()
                .unwrap_or_else(|| remote_ex.get_scheduler())
                .to_string(),
            platform: remote_ex.get_platform().clone(),
        }
    }
}

impl RemoteExecution {
    /// Get the URL endpoint of the REAPI Execution service
    pub fn get_scheduler(&self) -> &String {
        &self.scheduler
    }

    /// Get the Log Stream name of the service
    pub fn get_log_stream(&self) -> &String {
        &self.log_stream
    }

    /// Get the mapping of Platform properties for this endpoint
    pub fn get_platform(&self) -> &BTreeMap<String, String> {
        &self.platform
    }
}

/// Actual TLS keypair held in memory
#[derive(Clone, Debug)]
pub struct CacheCertConfig {
    /// The PEM-encoded private key of a TLS keypair
    key: String,

    /// The PEM-encoded public X.509 certificate of a TLS keypair
    cert: String,
}

impl TryFrom<CacheCert> for CacheCertConfig {
    type Error = anyhow::Error;

    fn try_from(client: CacheCert) -> Result<Self, Self::Error> {
        Ok(CacheCertConfig {
            key: client.key.get_pem()?,
            cert: client.cert.get_pem()?,
        })
    }
}

/// Hold the configuration information for a remote reapi cas
#[derive(Clone, Debug)]
pub struct RemoteConfig {
    /// URL Endpoint of the remote cache (CAS/AC)
    endpoint: String,

    /// URL Endpoint of a Remote Asset API server
    asset_endpoint: Option<String>,

    /// REAPI Instance name to use for this remote
    instance_name: String,

    /// Whether to push objects into this cache
    push: bool,

    /// Configuration of a REAPI Execution service
    execution: Option<RemoteExecution>,

    _previous_issues: bool,

    /// Whether to halt if a build fails
    stop_on_failed_build: bool,

    /// TLS Keypair used for client mTLS authentication with the endpoint
    client_key: Option<CacheCertConfig>,

    /// CA certificate used to verify the server
    server_cert: Option<String>,
}

impl TryFrom<RemoteCacheConfig> for RemoteConfig {
    type Error = anyhow::Error;

    fn try_from(remote: RemoteCacheConfig) -> Result<Self, Self::Error> {
        Ok(RemoteConfig {
            endpoint: remote.url,
            instance_name: remote.instance,
            push: remote.push.unwrap_or(false),
            execution: remote.execution.map(|ex| ex.into()),
            _previous_issues: false,
            stop_on_failed_build: remote.stop_on_failed_push.unwrap_or(false),
            client_key: if let Some(client_key) = remote.client_cert {
                Some(client_key.try_into()?)
            } else {
                None
            },
            server_cert: if let Some(server_cert) = remote.server_cert {
                Some(server_cert.get_pem()?)
            } else {
                None
            },
            asset_endpoint: remote.assets.map(|asset| asset.url),
        })
    }
}

impl RemoteConfig {
    /// Create a `Config` from configuration information
    pub fn new(
        endpoint: String,
        instance_name: String,
        push: bool,
        execution: Option<RemoteExecution>,
        stop_on_failed_build: bool,
    ) -> Self {
        RemoteConfig {
            endpoint,
            instance_name,
            push,
            execution,
            _previous_issues: false,
            stop_on_failed_build,
            client_key: None,
            server_cert: None,
            asset_endpoint: None,
        }
    }

    /// Indicate if Girderstream should try to push to this remote
    pub fn can_push(&self) -> bool {
        self.push
    }

    /// Get the instance name or url
    pub fn get_endpoint(&self) -> String {
        self.endpoint.clone()
    }

    /// Get if configured to stop on push
    pub fn stop_on_failed_push(&self) -> bool {
        self.stop_on_failed_build
    }

    /// Get the configuration of the Execution service
    pub fn get_execution(&self) -> Option<&RemoteExecution> {
        self.execution.as_ref()
    }

    /// Get the instance name to operate with
    pub fn get_instance_name(&self) -> &String {
        &self.instance_name
    }

    /// Get the Remote Asset API endpoint
    pub fn get_asset_endpoint(&self) -> Option<&String> {
        self.asset_endpoint.as_ref()
    }
}

#[derive(Clone, Debug)]
pub enum Executor {
    Buildbox(String),
    Yaba(PathBuf),
}

/// Hold the configuration information for a local reapi cas
#[derive(Clone)]
pub struct LocalConfig {
    endpoint: String,
    instance_name: String,
    executor: Option<Executor>,
}

impl LocalConfig {
    /// Create a `Config` from configuration information
    pub fn new(endpoint: String, instance_name: String, executor: String) -> Self {
        LocalConfig {
            endpoint,
            instance_name,
            executor: Some(Executor::Buildbox(executor)),
        }
    }
    pub fn new_default(endpoint: String, instance_name: String) -> Self {
        LocalConfig {
            endpoint,
            instance_name,
            executor: Some(Executor::Buildbox("buildbox-run".to_string())),
        }
    }
    pub fn new_yaba(endpoint: String, instance_name: String, root: PathBuf) -> Self {
        LocalConfig {
            endpoint,
            instance_name,
            executor: Some(Executor::Yaba(root)),
        }
    }
}

/// Enum used by ReapiConnection to indicate if the connection is remote or local
#[derive(Clone)]
enum ConnectionType {
    /// Indicates that this Config is for a local buildbox-casd
    Local(LocalConfig),
    /// Indicates that this Config is for a remote reapi cas server
    Remote(RemoteConfig),
    /// Indicates that this Config is for a remote reapi execution server
    Execution(RemoteConfig),
}

impl ConnectionType {
    /// Get the endpoint of the contained config
    fn get_endpoint(&self) -> &String {
        match self {
            Self::Local(conf) => &conf.endpoint,
            Self::Remote(conf) => &conf.endpoint,
            Self::Execution(conf) => &conf.endpoint,
        }
    }

    fn get_local_exec(&self) -> Option<&Executor> {
        match &self {
            ConnectionType::Local(local) => local.executor.as_ref(),
            ConnectionType::Remote(_) => None,
            ConnectionType::Execution(_) => None,
        }
    }

    /// Get the instance of the contained config
    fn get_instance_name(&self) -> &String {
        match self {
            Self::Local(conf) => &conf.instance_name,
            Self::Remote(conf) => &conf.instance_name,
            Self::Execution(conf) => &conf.instance_name,
        }
    }

    /// Get the instance of the contained config
    fn get_client_cert(&self) -> Option<&CacheCertConfig> {
        match self {
            Self::Local(_) => None,
            Self::Remote(conf) => conf.client_key.as_ref(),
            Self::Execution(conf) => conf.client_key.as_ref(),
        }
    }

    /// Get the instance of the contained config
    fn get_server_cert(&self) -> Option<&String> {
        match self {
            Self::Local(_) => None,
            Self::Remote(conf) => conf.server_cert.as_ref(),
            Self::Execution(conf) => conf.server_cert.as_ref(),
        }
    }

    /// Get the instance of the contained config
    fn get_asset_endpoint(&self) -> &String {
        match self {
            Self::Local(conf) => &conf.endpoint,
            Self::Remote(conf) => {
                if let Some(custom) = conf.asset_endpoint.as_ref() {
                    custom
                } else {
                    &conf.endpoint
                }
            }
            Self::Execution(conf) => {
                if let Some(custom) = conf.asset_endpoint.as_ref() {
                    custom
                } else {
                    &conf.endpoint
                }
            }
        }
    }
}

/// A full set of local and a number of remote configs
///
/// Girderstream needs a local connection but will then also connect
/// to remote cas to reuse builds by others or to allow sharing of
/// our builds with others.
#[derive(Clone)]
pub struct FullConfig {
    local_config: LocalConfig,
    remote_config: Vec<RemoteConfig>,
}

impl FullConfig {
    /// Create a Full config from a local config and a number of remotes
    pub fn new(local: LocalConfig, remote: Vec<RemoteConfig>) -> Self {
        FullConfig {
            local_config: local,
            remote_config: remote,
        }
    }

    /// Get the local config
    pub fn get_local(&self) -> &LocalConfig {
        &self.local_config
    }

    /// Get the remote configs
    pub fn get_remote(&self) -> &Vec<RemoteConfig> {
        &self.remote_config
    }
}

/// A connection to a cas server
///
/// When created this just knows about the remotes but then
/// as the clients are requested they will be created and then
/// cache and re-used.
pub struct ReapiConnection {
    config: ConnectionType,
    bytestream: Option<ByteStreamClient<Channel>>,
    cas: Option<ContentAddressableStorageClient<Channel>>,
    asset_fetch: Option<FetchClient<Channel>>,
    asset_push: Option<PushClient<Channel>>,
    executor: Option<ExecutionClient<Channel>>,
    log_stream: Option<ByteStreamClient<Channel>>,
    capabilities_client: Option<CapabilitiesClient<Channel>>,
    blob_size: Option<i64>,
}

impl Clone for ReapiConnection {
    fn clone(&self) -> Self {
        ReapiConnection {
            config: self.config.clone(),
            bytestream: None,
            cas: None,
            asset_fetch: None,
            asset_push: None,
            executor: None,
            log_stream: None,
            capabilities_client: None,
            blob_size: None,
        }
    }
}

impl From<LocalConfig> for ReapiConnection {
    /// Create a ReapiConnection from a local config
    ///
    /// The created connection will create when needed and then cache
    /// and then re-use its grpc clients
    fn from(local: LocalConfig) -> Self {
        ReapiConnection {
            config: ConnectionType::Local(local),
            bytestream: None,
            cas: None,
            asset_fetch: None,
            asset_push: None,
            executor: None,
            log_stream: None,
            capabilities_client: None,
            blob_size: None,
        }
    }
}

impl From<RemoteConfig> for ReapiConnection {
    /// Create a ReapiConnection from a remote config
    ///
    /// The created connection will create when needed and then cache
    /// and then re-use its grpc clients
    fn from(remote: RemoteConfig) -> Self {
        if remote.execution.is_some() {
            ReapiConnection {
                config: ConnectionType::Execution(remote),
                bytestream: None,
                cas: None,
                asset_fetch: None,
                asset_push: None,
                executor: None,
                log_stream: None,
                capabilities_client: None,
                blob_size: None,
            }
        } else {
            ReapiConnection {
                config: ConnectionType::Remote(remote),
                bytestream: None,
                cas: None,
                asset_fetch: None,
                asset_push: None,
                executor: None,
                log_stream: None,
                capabilities_client: None,
                blob_size: None,
            }
        }
    }
}

impl ReapiConnection {
    /// Get the string of the cas's endpoint
    pub fn get_endpoint(&self) -> &String {
        self.config.get_endpoint()
    }
    pub fn get_local_exec(&self) -> Option<&Executor> {
        self.config.get_local_exec()
    }

    fn tonic_endpoint(&self, url: String) -> anyhow::Result<Endpoint> {
        let tls_config = ClientTlsConfig::new();
        let tls_config = if let Some(cert) = self.config.get_client_cert() {
            let identity = Identity::from_pem(&cert.cert, &cert.key);
            tls_config.identity(identity)
        } else {
            tls_config
        };
        let tls_config = if let Some(cert) = self.config.get_server_cert() {
            let ca_certificate = Certificate::from_pem(cert);
            tls_config.ca_certificate(ca_certificate)
        } else {
            tls_config
        };
        let config = Endpoint::new(url)?;
        Ok(config.tls_config(tls_config)?)
    }
    /// Get the tonic endpoint of the cas
    pub fn get_tonic_endpoint(&self) -> anyhow::Result<Endpoint> {
        self.tonic_endpoint(self.get_endpoint().to_owned())
    }

    pub fn get_asset_endpoint(&self) -> &String {
        self.config.get_asset_endpoint()
    }

    pub fn get_tonic_asset_endpoint(&self) -> anyhow::Result<Endpoint> {
        self.tonic_endpoint(self.get_asset_endpoint().to_owned())
    }

    /// Get the maximum allowed request size
    ///
    /// The hole request mush be less than this size including any meta data
    /// along with the payload.
    pub async fn get_blob_size(&mut self) -> anyhow::Result<i64> {
        if let Some(size) = self.blob_size {
            return Ok(size);
        }
        let request = GetCapabilitiesRequest {
            instance_name: self.get_instance_name().to_string(),
        };
        let cas = self.get_capabilities_client().await?;
        let capabilities = cas.get_capabilities(request).await?.into_inner();
        let cas_caps = capabilities
            .cache_capabilities
            .ok_or(anyhow::anyhow!("missing directory blob"))?;
        let size = cas_caps.max_batch_total_size_bytes;
        let size = if size == 0 { 4 * 1024 * 1024 } else { size };
        self.blob_size = Some(size);
        Ok(size)
    }

    /// Get teh string of the cas's instance
    pub fn get_instance_name(&self) -> &String {
        self.config.get_instance_name()
    }

    /// Create or return a cached bytestream cleint
    pub async fn get_bytestream_client(
        &mut self,
    ) -> anyhow::Result<&mut ByteStreamClient<Channel>> {
        if self.bytestream.is_none() {
            self.bytestream = Some(ByteStreamClient::connect(self.get_tonic_endpoint()?).await?);
        }
        Ok(self.bytestream.as_mut().unwrap())
    }

    /// Create or return a cached asset fetch client
    pub async fn get_asset_fetch_client(&mut self) -> anyhow::Result<&mut FetchClient<Channel>> {
        if self.asset_fetch.is_none() {
            self.asset_fetch = Some(FetchClient::connect(self.get_tonic_asset_endpoint()?).await?);
        }
        Ok(self.asset_fetch.as_mut().unwrap())
    }

    /// Create or return a cached asset fetch client
    pub async fn get_cas_client(
        &mut self,
    ) -> anyhow::Result<&mut ContentAddressableStorageClient<Channel>> {
        if self.cas.is_none() {
            self.cas =
                Some(ContentAddressableStorageClient::connect(self.get_tonic_endpoint()?).await?);
        }
        Ok(self.cas.as_mut().unwrap())
    }
    /// Create or return a cached asset push client
    pub async fn get_asset_push_client(&mut self) -> anyhow::Result<&mut PushClient<Channel>> {
        if self.asset_push.is_none() {
            self.asset_push = Some(PushClient::connect(self.get_tonic_asset_endpoint()?).await?);
        }
        Ok(self.asset_push.as_mut().unwrap())
    }

    /// Create or return a cached capabilities client
    pub async fn get_capabilities_client(
        &mut self,
    ) -> anyhow::Result<&mut CapabilitiesClient<Channel>> {
        if self.capabilities_client.is_none() {
            self.capabilities_client =
                Some(CapabilitiesClient::connect(self.get_tonic_endpoint()?).await?);
        }
        Ok(self.capabilities_client.as_mut().unwrap())
    }

    /// Indicate if the connected cache should be pushed to
    ///
    /// If negative then we should not push to this remote cache.
    ///
    /// Will panic if called on a local rather than remote connection.
    pub fn can_push(&self) -> Option<bool> {
        match &self.config {
            ConnectionType::Local(_) => None,
            ConnectionType::Remote(conf) => Some(conf.push),
            ConnectionType::Execution(_) => None,
        }
    }

    /// Create or return a cached execution client
    pub async fn get_execution_client(
        &mut self,
    ) -> anyhow::Result<Option<&mut ExecutionClient<Channel>>> {
        match &self.config {
            ConnectionType::Execution(remote) => {
                if self.executor.is_none() {
                    self.executor = Some(
                        ExecutionClient::connect(
                            remote
                                .execution
                                .as_ref()
                                .ok_or(anyhow::anyhow!("remote must be executor"))?
                                .get_scheduler()
                                .to_owned(),
                        )
                        .await?,
                    );
                }
                Ok(Some(self.executor.as_mut().unwrap()))
            }
            ConnectionType::Remote(_) => Ok(None),
            ConnectionType::Local(_) => Ok(None),
        }
    }

    /// Create or return a cached logstream client
    pub async fn get_logstream_client(
        &mut self,
    ) -> anyhow::Result<Option<&mut ByteStreamClient<Channel>>> {
        match &self.config {
            ConnectionType::Execution(remote) => {
                if self.log_stream.is_none() {
                    self.log_stream = Some(
                        ByteStreamClient::connect(
                            remote
                                .execution
                                .as_ref()
                                .ok_or(anyhow::anyhow!("remote must be executor"))?
                                .get_log_stream()
                                .to_owned(),
                        )
                        .await?,
                    );
                }
                Ok(Some(self.log_stream.as_mut().unwrap()))
            }
            ConnectionType::Remote(_) => Ok(None),
            ConnectionType::Local(_) => Ok(None),
        }
    }
}

#[cfg(feature = "test_infra")]
pub mod test_infra {
    use std::net::SocketAddr;
    use std::path::Path;
    use tempfile::{tempdir, TempDir};
    use tokio::net::TcpListener;
    use tokio::task::JoinHandle;
    use yaba::server::serve_listener;

    async fn bind() -> (TcpListener, String) {
        let addr = SocketAddr::from(([127, 0, 0, 1], 0));
        let lis = TcpListener::bind(addr).await.expect("listener");
        let url = format!("http://{}", lis.local_addr().unwrap());

        (lis, url)
    }

    pub struct YabaTestSetup {
        tmp_dir: TempDir,
    }

    impl YabaTestSetup {
        pub async fn setup_server() -> anyhow::Result<Self> {
            let tmp_dir_test = tempdir()?;
            Ok(Self {
                tmp_dir: tmp_dir_test,
            })
        }

        pub async fn run_cas_thread(&self) -> YabaTestServer {
            let tmp_dir_str = format!("{}", self.tmp_dir.path().display());
            let (listener, host_name) = bind().await;
            println!("yaba port: {}", host_name);
            // https://github.com/hyperium/tonic/blob/fc940ce158513d5d989772c571a34ee5f6f43292/tests/web/tests/grpc.rs
            let server = tokio::spawn(async move {
                serve_listener(listener, Some(tmp_dir_str.into()), true).await;
            });
            YabaTestServer {
                tmp_dir: &self.tmp_dir,
                _server: server,
                host_name,
            }
        }
        pub fn get_temp_root(&self) -> &Path {
            self.tmp_dir.path()
        }
    }

    pub struct YabaTestServer<'a> {
        tmp_dir: &'a TempDir,
        _server: JoinHandle<()>,
        host_name: String,
    }

    impl YabaTestServer<'_> {
        pub fn get_temp_root(&self) -> &Path {
            self.tmp_dir.path()
        }

        pub fn get_address(&self) -> &String {
            &self.host_name
        }
    }
}

#[cfg(test)]
mod tests {
    use std::os::unix::ffi::OsStrExt;

    use anyhow::Ok;
    use tempfile::tempdir;
    use tokio::io::AsyncWriteExt;

    use crate::{Crapshoot, RemotesConfig};

    #[tokio::test]
    async fn test_loading_remotes() -> Crapshoot {
        let raw_input = "remotes: []";
        let new: RemotesConfig = serde_yaml::from_str(raw_input).expect("Could parse yaml");
        assert_eq!(new.get_remotes().len(), 0);

        let raw_input = "remotes:\n- url: bob\n  instance: \"\"";
        let new: RemotesConfig = serde_yaml::from_str(raw_input).expect("Could parse yaml");
        assert_eq!(new.get_remotes().len(), 1);

        let raw_input="remotes:\n- url: bob\n  instance: \"\"\n  client_cert:\n    key: mycert\n    cert: mycert";
        let new: RemotesConfig = serde_yaml::from_str(raw_input).expect("Could parse yaml");
        assert_eq!(new.get_remotes().len(), 1);
        let remote = new
            .get_remotes()
            .first()
            .unwrap()
            .client_cert
            .clone()
            .unwrap();
        assert_eq!(remote.key.get_pem().unwrap(), "mycert");

        let raw_input="remotes:\n- url: bob\n  instance: \"\"\n  client_cert:\n    key:\n      text: mycert\n    cert:\n      text: mycert";
        let new: RemotesConfig = serde_yaml::from_str(raw_input).expect("Could parse yaml");
        assert_eq!(new.get_remotes().len(), 1);
        let remote = new
            .get_remotes()
            .first()
            .unwrap()
            .client_cert
            .clone()
            .unwrap();
        assert_eq!(remote.key.get_pem().unwrap(), "mycert");

        temp_env::with_var("MY_KEY", Some("mycert"), || {
            let raw_input="remotes:\n- url: bob\n  instance: \"\"\n  client_cert:\n    key:\n      env: MY_KEY\n    cert:\n      env: MY_CERT";
            let new: RemotesConfig = serde_yaml::from_str(raw_input).expect("Could parse yaml");
            assert_eq!(new.get_remotes().len(), 1);
            let remote = new
                .get_remotes()
                .first()
                .unwrap()
                .client_cert
                .clone()
                .unwrap();
            assert_eq!(remote.key.get_pem().unwrap(), "mycert");
            assert!(remote.cert.get_pem().is_err())
        });

        let tmp_dir = tempdir().unwrap();
        let temp_file = tmp_dir.path().join("temp.txt");
        let mut temp_fl = tokio::fs::File::create(&temp_file).await.unwrap();

        let buff = "my secret".as_bytes();
        temp_fl.write_all(buff).await.unwrap();
        temp_fl.flush().await.unwrap();

        let raw_input=format!("remotes:\n- url: bob\n  instance: \"\"\n  client_cert:\n    key:\n      path: {}\n    cert:\n      path: other", String::from_utf8(temp_file.as_os_str().as_bytes().to_owned()).unwrap() );
        let new: RemotesConfig = serde_yaml::from_str(&raw_input).expect("Could parse yaml");
        assert_eq!(new.get_remotes().len(), 1);
        let remote = new
            .get_remotes()
            .first()
            .unwrap()
            .client_cert
            .clone()
            .unwrap();
        assert_eq!(remote.key.get_pem().unwrap(), "my secret");
        assert!(remote.cert.get_pem().is_err());

        drop(tmp_dir);

        Ok(())
    }
}
