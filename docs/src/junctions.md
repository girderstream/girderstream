# Junctions

The way that girderstream extents projects is with junctions.

Junctions are special elements, they mostly behave like any other element but there are a few special cases were they
do different things.

## Sources

Junctions sources are handled exactly like any other elements sources.

## Project with in a project

The magic of junctions is that they load a sub project into your main project.

For Buildstream uses its worth noting that girderstream handles junctions differently to Buildstream. Buildstream's
junctions load in a fully formed junction and then let the parent project refure to elments within that sub project.

Girderstream imports the elements from the sub project into the main project, merging the two projects to form a single
project.

The girderstream approach means that you can load in a fully formed project, then just provide one or two elements that
you with to override and then treat the new project like the original project with very little extra todo.

## Managing providers

Girdersteam projects enforce that once a project is fully configures there can be only one provider of each component.

Girderstream provides the following junction configuration variables:
* `over-ride-source-*`
* `skip-element-prefix`
* `pass-though-options`
* `over-ride-build-*`
* `load-sub-junctions`

### `over-ride-source-*` and `over-ride-build-*`

The over variables take the patten `over-ride-build-ELEMENT_NAME` eg `over-ride-elements-demo_app` and
`over-ride-source-SOURCE_PLUGIN` eg `over-ride-source-cargo_source_bootstrap`.

This would look like:

```yaml
variables:
  over-ride-elements-demo_app: skip
  over-ride-source-cargo_source_bootstrap: skip
```

These variables can be used to skip the loading of a source or element. These are very useful if you already have these
element declared.

### `skip-element-prefix`

The `skip-element-prefix` variable can be used to skip all the elments that start with a set patten eg:

```yaml
variables:
  skip-element-prefix: "bootstrap_"
```

This might be used ful if both your project and the project brought in by a junction both have a lot of elements with the
same prefix.

### `pass-though-options`

When loading elements via a junction then the junctions existing `provides_rules` can be used to know which element should
be used for [provider/requirement resolution](./provides_requires.md). To use the `provides_rules` then the junction-ed
project needs to have its configuration set rather than always using its own defaults. Girderstream will use the default
configuration for junction-ed project even if the parent has the same configuration options by default. The junction-ed
project can be set to use the parents project options by explicitly setting which options to pass through with
`pass-though-options`.

```yaml
variables:
  pass-though-options: OPTION_TO_PASS_THOUGH
```

Once the option is set to pass through then setting the option on the girderstream cli will then effect both the elements
selection in your project and the junction-ed elements.

Note that you must have the project configuration option declared in both the parent project and the junction-ed project
for the options specified by `pass-though-options`.

### `load-sub-junctions`

By default junctions load in their own junctions and so on and so forth until all the junctions have been included. However
that may not always be the desired behavior, this option allows you to skip loading the junctions junctions.

If you want to use a different sdk for both your elements and the elements that come from this junction then this
option is very helpful. But be aware you will need the other version of the sdk you are junction-ing in a different element
or providing with your own elements. Will be required by girderstream to have the same element names and for the software to build
at all or correctly will require the compilers to be fairly similar.

## Example junction

A full junction element might look like:

```
kind: junction

sources:
- kind: bootstrap_git
  sha: 6b2fa5799e64e16da2b43fe4af74f40b4233cd35
  url: https://my.greate.upstream/project/to/junction.git

plugin_variables:
  join_strategy: Join
  variables:
    load-sub-junctions: false
    skip-element-prefix: "bootstrap_"
    pass-though-options: "bsp"
    join-contexts-strategy: skip-all
    join-source-strategy: keep
    join-build-strategy: keep
    over-ride-source-tar_automake_example: skip
    over-ride-build-bootstrap_manual: skip
    over-ride-build-bootstrap_manual_min: skip
    over-ride-elements-demo_app: skip
```

This element has at least one `source` like any other, its kind is set to junction to use the build in kind and trigger
girderstream to treat it appropriately.

Then the junction has a set of `plugin_variables` using the special variable meanings as set above to ensure that the right
elements are loaded from the junction.

When starting a new project some consideration should be given to [bootstrapping](./bootstrap.md) and this book has a
section on this. Junctions can end up interacting with the bootstrap options because they need a source plugin but if the
junction is to the sdk that provides most of the source plugins for the project then the bootstrap element needs to provide
the source plugin for the sdk's junction-ing element and the project.conf needs to provide config for how to use it. Even if all other
plugins come in from this junction.
