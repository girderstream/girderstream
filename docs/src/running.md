# Running girderstream

Before you start girderstream you will commonly need todo a few things
  * Have a local buildbox-casd running on port 50040
  * Have a buildbox-run implementation and its deps on your path,
    eg buildbox-run-bubblewrap and bubblewrap
  * Have your bootstrap uploaded to your local or remote cache

Some of these things might be handled by girderstream in future versions
but for now you need to provide them.

Please note that these examples use docker to run girderstream and buildbox
as this is a very convenient way to get all the required tools but should work
however you have installed girderstream and buildbox so the examples within the
docker images should work without docker as well.

From the [installing page](./install.md), if you are using docker then please, remember to pull the latest docker image with:

```bash
docker pull registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
```

## Upload your bootstrap

As mentioned in the [Intro](./using_intro.md) your base elements that everything
else including your bootstrap build and source plugins depend on or transitively
depend use `cas` sources, these are the only build in sources.

There for you must upload your bootstrap elements bootstrap-chroot into ether the local
or remote caches. If you are using someone elses project then they might have
a remote cache that you can use and they might include that in a `.remotes` file
in there projects source. In that cas you wont need todo any more.

If you are creating your own project or the project you are using does not have
a cache you can access then you will need to uplaod your projects bootstrap to your
local cache or a remote cache you setup your self.

There are several ways to upload a booststrap to a REAPI cache but if you already
have buildbox installed then the easiest way to to use casupload from buildbox.

For instance using the integration tests in the girderstream project with the
girderstream docker images you could do 

``` bash
$ cd girderstream/tests/build_tests
$ docker run -it -v `pwd`:/project:z -v ~/.cache/girderstream/docker:/tmp/cas:z --privileged --workdir /project/ registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
# buildbox-casd  --bind localhost:50040 /tmp/cas &
# cd files
# tar -xf  busybox.tar.xz
# cd ..
# casupload --cas-server=http://localhost:50040 files/busybox2
2023-05-11T20:37:24.192+0000 [82:139880848902208] [buildboxcommon_casclient.cpp:95] [INFO] Setting d_maxBatchTotalSizeBytes = 4128768 bytes by default
2023-05-11T20:37:25.210+0000 [82:139880848902208] [casupload.m.cpp:120] [INFO] Uploaded "busybox2": 9c69051b0df71f11f946369cff347ae670ff91e5f82861a5f8bfd1a8e07491ab/321
```

You can then verify that the uploaded directory matches that in the element

eg elements/stage0.grd

``` yaml
kind: bootstrap_import

provides: stage0

sources:
- kind: cas
  digest: "9c69051b0df71f11f946369cff347ae670ff91e5f82861a5f8bfd1a8e07491ab/321"
```

## Building elements with girderstream

Now that our cas contains everything needed to build our stage0 element we can build it by running girderstream with the
build verb and the element to build specified.

```bash
# girderstream -v build --name stage0
unbuilt junctions: []
built junctions
GirderStream Project InfoBlock

Project configuration:
	Configuration Option: set
		Configuration Selected: set1
		From:
		- set1
		- set2

Elements:
- Element: stage0              	Kind: bootstrap_import	Key w: d0ac37c25cdfc4e814c2c9e618b5777657ec2914cd3dad9dc6b94e65a292206d Key s: fc8090968ec9ebffffdc1117a6c2a79a588abc95b4985e7dca50b3d91bd3b9fd cached: Success Result Digest: 9c69051b0df71f11f946369cff347ae670ff91e5f82861a5f8bfd1a8e07491ab/321
Waiting for task
Waiting for task
Waiting for task
Waiting for task
Waiting for task
build_requester Sender { .. }
final_result: Ok(())
Build succeeded
- Element: stage0              	Key: fc8090968ec9ebffffdc1117a6c2a79a588abc95b4985e7dca50b3d91bd3b9fd cached: Success
```

With your bootstrap loaded into the stage0 element subsistent elements can then run commands inside the sandbox
instead of just copying directly from the sources.

```bash
$ cd girderstream
$ docker run -it -v `pwd`:/project:z -v ~/.cache/girderstream/docker:/tmp/cas:z --privileged --workdir /project/ registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
# buildbox-casd  --bind localhost:50040 /tmp/cas &
# cd tests/build_tests/
# girderstream build --name basic_build
unbuilt junctions: []
built junctions
GirderStream Project InfoBlock

Project configuration:
	Configuration Option: set
		Configuration Selected: set1
		From:
		- set1
		- set2

Elements:
- Element: stage0              	Key: fc8090968ec9ebffffdc1117a6c2a79a588abc95b4985e7dca50b3d91bd3b9fd cached: Success
- Element: basic_build         	Key: 46a82bf697159a742ea979f5390fb7980806b5569b5204c91a812de8f937d6aa cached: Not Built
Waiting for task
Waiting for task
Waiting for task
Waiting for task
Waiting for task
enables {}
requires {}
building ["basic_build"]
Got build: basic_build bootstrap_manual
Building "basic_build"
starting build of basic_build
Finished building basic_build, Success: true
Done build: "basic_build"
'basic_build' has finished BuildResult { build_output: Digest { hash: "56e0338cf30c636a1fcea0bc0aba6af24a8781fbfd1791b3954117825ad64a3c", size_bytes: 189 }, stdout: Digest { hash: "0f3d656aed10729c0bd064408847d70a4076142a96fb289f22710da226e3b7d8", size_bytes: 18 }, stderr: Digest { hash: "09030f0ee2b870c062e312a534ca16140c9285b3d56b161f313d80cb0800d81b", size_bytes: 18 }, success: true, source_keys: [] }
build_requester Sender { .. }
final_result: Ok(())
Build succeeded
- Element: stage0              	Key: fc8090968ec9ebffffdc1117a6c2a79a588abc95b4985e7dca50b3d91bd3b9fd cached: Success
- Element: basic_build         	Key: 46a82bf697159a742ea979f5390fb7980806b5569b5204c91a812de8f937d6aa cached: Success
```

If you run girderstream again then the cached job will persist and so will produce much less output as it does not need
to repeat the build.

```bash
# girderstream build --name basic_build
GirderStream Project InfoBlock

Project configuration:
	Configuration Option: set
		Configuration Selected: set1
		From:
		- set1
		- set2

Elements:
- Element: stage0              	Key: f59af9eec1ed088811dfffe5c8b7b44891abb9704323cb29adc7b2a8f87b320d cached: true
- Element: basic_build         	Key: cd25526a8266bbf4c76158977b8ff00d5c2995f1c8e669980cb7586a750a2e15 cached: true
Waiting for task
Waiting for task
Waiting for task
Waiting for task
Waiting for task
[src/scheduler.rs:425] &locked_building = {}
Builder finished
Builder finished
Builder finished
Builder finished
Builder finished
build_requester Sender { .. }
- Element: stage0              	Key: f59af9eec1ed088811dfffe5c8b7b44891abb9704323cb29adc7b2a8f87b320d cached: true
- Element: basic_build         	Key: cd25526a8266bbf4c76158977b8ff00d5c2995f1c8e669980cb7586a750a2e15 cached: true

```

As we are using a volume to persist the cache between involcations of docker then these cached builds will not need to be
rebuilt every time you start docker but you will need to restart buildbox-casd each time you start docker.

## Exporting built artifacts from girderstream

Once you have built some elements with girderstream you will want to check out the elements you have built.

Girderstream provides the checkout verb for this tasks, it requires you to specify the element to checkout
with the `--name` option and a folder to check the element out into with the `--location` option.

```bash
# girderstream checkout --name basic_build --location output
unbuilt junctions: []
built junctions
# ls output/
test_source.output  test_source_second.output
```
## Further verbs

The show, build and checkout verbs alow you to understand build and use the software integrated in your project. However
there are move verbs that will be described in the documentation are are described using the `--help` option.

``` bash
$ girderstream --help
girderstream 0.1.0

USAGE:
    girderstream [OPTIONS] <SUBCOMMAND>

OPTIONS:
        --endpoint <ENDPOINT>
            Specify the url of the local cas endpoint

    -h, --help
            Print help information

        --instance <INSTANCE>
            Specify the instance of the local cas

        --options <OPTIONS>
            Allow for specifying options to use
            
            Both the option name and value must be options given in the project.conf eg:
            --options=opt1=value

        --project-root <PROJECT_ROOT>
            Specify the project root to load from, defaults to pwd if non given

    -v, --verbose
            Flag to turn on verbose output

    -V, --version
            Print version information

SUBCOMMANDS:
    build
            Trigger a Girderstream build of a given element
    checkout
            Checkout a given elements output to the local file system
    export-logs
            Export logs
    help
            Print this message or the help of the given subcommand(s)
    push
            Push a given element to the remote caches
    show
            Show a given Girderstream element and all of the information about it
```
