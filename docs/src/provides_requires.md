# Provides Requires Patten

In order to generate cache keys that change when ever any aspect of the build that would effect it. Girderstream
use the dependencies to calculate the cache key.

But some times we want to reuse a element for multiple different dependencies. In the
[element reuse](./project_structure_details.html#element-re-use) section we talk about why you might want to do this for a
number of reasons. But for this doc we will consider the case of having multiple providers of a API that a element can
use.

Girderstream resolves these two seemingly contradictory requirements with the `Provides Requires Patten` with the
[principles](./principles.md) that for any given girderstream configuration the exact dependency is explicitly set
and that when the configuration changes so that the dependency changes then the cache key also changes.

## The Provides Patten

Elements can advertise what they provide ether through there file name or through the `provides` key there yaml.

Many elements can provide the same thing and which the project uses is not set in the element file. see the
[resolving provides section](#resolving-provides)

## The Requires Patten

Every time a element declares a dependency that dependency is a `requirement` of the element and must be `provided` by
the project. The `name` key in the dependencies list in the element is used to set the `requirement`.

## Resolving Provides

It is the projects responsibility to make sure that there is only ever one element providing each requirement at a time.
The two main ways todo this are the `project.conf`'s `provides_rules:` section and each [junction](./junctions.md)'s
`variables:` using the `over-ride-source-*`, `skip-element-prefix`, `pass-though-options`, `over-ride-build` keys.
