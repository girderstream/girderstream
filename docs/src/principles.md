# Principles of Girderstream

This document is under construction.

GirderStream is currently in the design/early implementation phase. But can
be used to build fairly complex projects.

## Who is this document for

This document is aimed at people trying to decide if girderstream's architecture meets the
requirements of a tool they would like to use. And is for people trying to work out if a
change would be suitable for the project.


## Motivating factors

I want to build the same thing as my college so we can work together on the same project.
This sounds trivial but the reality is that using a package based OS were you want to be bit for
bit repeatable for gigabits of software and keep getting exactly the same bits this has proven
to many instances to be very hard.

Its fairly easy to compile ths same source code to make roughly the same binary but its
very hard to make exactly the same binary and have confidence that it will always be the same
going forward.

This is why `we` the software community created Buildroot and Yocto. They do a much better
job than make on a packaged based OS. but they are not satisfactory, ie. they still have a
lot of host tools, to the point that nether have distributed caches etc. Bazel also tries
really hard and works ok if you just want a binary and you have extra tools to control its
dependencies.

On recent projects with Yocto I spent more time with it failing to build due to issues with
my host than i had it building successful projects.


## Key Principles

We need to outline some principles so we can tell when we are not doing enough to keep
girderstream sensible and when we are over engineering.

Inputs must be clear and complete:
  * Input files must be purely declarative and elements must not interfere with each other.
    Elements that depend on each other should effect each other clearly, ie in the file system
    and this must be reflected in the cache key. Elements ABSOLUTELY must not effect each other
    in a way that is not reflected in the cache key.
  * All inputs to a element must be obvious, discoverable and auditable.

Must be decoupled from the host as much as possible:
  * In scope:
    * ABI
    * Sources
  * Out of scope:
    * Arch


## Implementation

The current very early versions of girderstream do not yet fully meet the principles although
they are close we should not merge any code to the default branch that moves us further or makes
it harder to meet the principles without a plan to meet them. We should work towards showing that
we do meet them and how to check that we meet them and then not deviate from them.
